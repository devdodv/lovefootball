package app.himaproduction.lifefootball.viewmodel

import androidx.lifecycle.MutableLiveData
import app.himaproduction.lifefootball.data.entity.League
import app.himaproduction.lifefootball.data.entity.MediaComment
import app.himaproduction.lifefootball.data.repository.Repository
import app.himaproduction.lifefootball.viewmodel.BaseViewModel
import app.himaproduction.lifefootball.ui.state.DataState
import com.google.firebase.firestore.DocumentSnapshot

class MainViewModel(val repository: Repository) : BaseViewModel() {
    private val countriesLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val leaguesFromCountryLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val someNextFixturesFromLeagueLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val standingFromLeagueLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val topScorerFromLeagueLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val eventsFromFixtureLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val statisticFromFixtureLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val mediasLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val pushCommentLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val commentsLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val uploadMediaLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val teamsLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val playersLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val teamStatisticLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val settingLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val predictionLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val postFeedbackLiveData: MutableLiveData<DataState> = MutableLiveData()
    private val predictionFixturesLiveData: MutableLiveData<DataState> = MutableLiveData()

    fun getCountriesLiveData() = countriesLiveData
    fun getLeaguesFromCountryLiveData() = leaguesFromCountryLiveData
    fun getSomeNextFixturesFromLeagueLiveData() = someNextFixturesFromLeagueLiveData
    fun getStandingFromLeagueLiveData() = standingFromLeagueLiveData
    fun getTopScorerFromLeagueLiveData() = topScorerFromLeagueLiveData
    fun getEventsFromFixtureLiveData() = eventsFromFixtureLiveData
    fun getStatisticFromFixtureLiveData() = statisticFromFixtureLiveData
    fun getMediasLiveData() = mediasLiveData
    fun getPushMediaLiveData() = pushCommentLiveData
    fun getCommentsLiveData() = commentsLiveData
    fun getUploadMediaLiveData() = uploadMediaLiveData
    fun getTeamsLiveData() = teamsLiveData
    fun getPlayersLiveData() = playersLiveData
    fun getTeamStatisticLiveData() = teamStatisticLiveData
    fun settingLiveData() = settingLiveData
    fun getPredictionLiveData() = predictionLiveData
    fun getPostFeedback() = postFeedbackLiveData
    fun getPredictionFixturesLiveData() = predictionFixturesLiveData


    fun fetchCountries() {
        executeFlowable(repository.getCountries(), countriesLiveData)
    }

    fun fetchLeaguesFromCountry(country: String, season: String) {
        executeFlowable(
            repository.getLeaguesFromCountry(country, season),
            leaguesFromCountryLiveData
        )
    }

    fun fetchSomeNextFixtureFromLeague(leagueId: Int, numberOfMatches: Int) {
        executeFlowable(
            repository.getSomeNextFixturesFromLeague(leagueId, numberOfMatches),
            someNextFixturesFromLeagueLiveData
        )
    }

    fun fetchStandingFromLeague(leagueId: Int) {
        executeFlowable(repository.getStandingFromLeague(leagueId), standingFromLeagueLiveData)
    }

    fun fetchTopScorerFromLeague(leagueId: Int) {
        executeFlowable(repository.getTopScorerFromLeague(leagueId), topScorerFromLeagueLiveData)
    }

    fun fetchEventsFromFixtre(fixtureId: Int) {
        executeFlowable(repository.getEvent(fixtureId), eventsFromFixtureLiveData)
    }

    fun fetchStatisticFromFixture(fixtureId: Int) {
        executeFlowable(repository.getStatisticFromFixture(fixtureId), statisticFromFixtureLiveData)
    }

    fun fetchMedias(lastSnapshot: DocumentSnapshot?) {
        executeFlowable(repository.getMedias(lastSnapshot), mediasLiveData)
    }

    fun pushComment(msg: MediaComment, id: String?) {
        executeFlowable(repository.pushComment(msg, id), pushCommentLiveData)
    }

    fun fetchComments(id: String?) {
        executeFlowable(repository.fetchComments(id), commentsLiveData)
    }

    fun uploadMedia() {
        executeCompletable(repository.uploadMedia(), uploadMediaLiveData)
    }

    fun fetchTeamsFromLeague(leagueId: Int) {
        executeFlowable(repository.fetchTeams(leagueId), teamsLiveData)
    }

    fun fetchPlayersFromTeam(teamId: Int) {
        executeFlowable(repository.fetchPlayersFromTeam(teamId), playersLiveData)
    }

    fun getTeamStatistic(teamId: Int, leagueId: Int) {
        executeFlowable(repository.getTeamStatistic(teamId, leagueId), teamStatisticLiveData)
    }

    fun getSetting() {
        executeFlowable(repository.getSetting(), settingLiveData)
    }

    fun fetchPrediction(fixtureId: Int){
        executeFlowable(repository.fetchPrediction(fixtureId), predictionLiveData)
    }

    fun postFeedback(feedbackMsg: String, username: String?) {
        executeCompletable(repository.postFeedback(feedbackMsg,username), postFeedbackLiveData)
    }

    fun getPredictionFixtures(url:String){
        executeFlowable(repository.getPredictionFixtures(url),predictionFixturesLiveData)
    }
}