package app.himaproduction.lifefootball.viewmodel

import androidx.lifecycle.MutableLiveData
import app.himaproduction.lifefootball.data.entity.League
import app.himaproduction.lifefootball.data.repository.Repository
import app.himaproduction.lifefootball.viewmodel.BaseViewModel
import app.himaproduction.lifefootball.ui.state.DataState

class LeagueFollowViewModel(val repository: Repository) : BaseViewModel() {
    private val addFollowingLeagueLiveData:MutableLiveData<DataState> = MutableLiveData()
    private val unFollowLeagueLiveData:MutableLiveData<DataState> = MutableLiveData()
    private val followingLeaguesLiveData:MutableLiveData<DataState> = MutableLiveData()
    private val isFollowedLeagueLiveData: MutableLiveData<DataState> = MutableLiveData()

    fun addFollowingLeagueLiveData() = addFollowingLeagueLiveData
    fun getFollowingLeaguesLiveData() = followingLeaguesLiveData
    fun unFollowLeagueLiveData() = unFollowLeagueLiveData
    fun isFollowedLeagueLiveData() = isFollowedLeagueLiveData

    fun addFollowingLeague(league: League) {
        executeFlowable(repository.saveFollowingLeague(league),addFollowingLeagueLiveData)
    }

    fun fetchFollowingLeagues() {
        executeFlowable(repository.getFollowingLeagues(),followingLeaguesLiveData)
    }

    fun unFollowLeague(league: League) {
        executeFlowable(repository.unFollowLeague(league),unFollowLeagueLiveData)
    }

    fun isFollowedLeague(leagueId: Int) {
        executeFlowable(repository.isFollowedLeague(leagueId), isFollowedLeagueLiveData)
    }

}