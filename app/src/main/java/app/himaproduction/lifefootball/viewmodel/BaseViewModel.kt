package app.himaproduction.lifefootball.viewmodel

import android.content.Context
import android.net.ConnectivityManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.himaproduction.lifefootball.App
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.BaseDataApi
import app.himaproduction.lifefootball.ui.state.DataState
import com.google.gson.JsonSyntaxException
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers


abstract class BaseViewModel : ViewModel() {
    var disposable: Disposable = CompositeDisposable()

    override fun onCleared() {
        disposable?.dispose()
        super.onCleared()
    }

    fun <T> executeFlowable(
        followable: Flowable<T>,
        resultLiveData: MutableLiveData<DataState>
    ) {

        disposable = followable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                resultLiveData.postValue(DataState.Loading)
            }
            .subscribe({
                when (it) {
                    is BaseDataApi -> {
                        if (it.result == 0) {
                            resultLiveData.postValue(DataState.Error(Utils.noDataMsg))
                        } else {
                            resultLiveData.postValue(DataState.Success(it.data!!))
                        }
                    }
                    is Collection<*> -> {
                        if (it.size == 0) {
                            resultLiveData.postValue(DataState.Error(Utils.noDataMsg))
                        } else {
                            resultLiveData.postValue(DataState.Success(it!!))
                        }
                    }
                    else -> {
                        if (it == null) {
                            resultLiveData.postValue(DataState.Error(Utils.noDataMsg))
                        } else {
                            resultLiveData.postValue(DataState.Success(it!!))
                        }
                    }
                }

            }, {
                if (it is JsonSyntaxException)
                    resultLiveData.postValue(DataState.Error(Utils.noDataMsg))
                else
                    resultLiveData.postValue(DataState.Error(Utils.serverErrorMsg))
            })
    }

    fun executeCompletable(
        completable: Completable,
        liveData: MutableLiveData<DataState>?
    ) {
        disposable = completable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableCompletableObserver() {
                override fun onStart() {
                    liveData?.postValue(DataState.Loading)
                }

                override fun onError(error: Throwable) {
                    liveData?.postValue(DataState.Error(error.localizedMessage))
                }

                override fun onComplete() {
                    liveData?.postValue(DataState.Success("Success"))
                }
            })
    }

    fun executeSingle(
        single: Single<Boolean>,
        liveData: MutableLiveData<Boolean>
    ) {
        disposable = single
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { t: Boolean? ->
                liveData.postValue(t)
            }
    }
}

fun hasInternet(): Boolean {
    val cm =
        App.self().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    val netInfo = cm!!.activeNetworkInfo
    return netInfo != null && netInfo.isConnectedOrConnecting
}