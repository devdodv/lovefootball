package app.himaproduction.lifefootball

object Const {
    val TAG_GET_FOLLOWING_LEAGUES: String = "TAG_GET_FOLLOWING_LEAGUES"
    val TAG_UNFOLLOW_LEAGUE: String = "TAG_UNFOLLOW_LEAGUE"
    val TAG_FOLLOW_LEAGUE: String = "TAG_FOLLOW_LEAGUE"
    const val TABLE_NAME_COUNTRIES = "countries"
    const val TABLE_NAME_FAVORITE_LEAGUES = "favorite_leagues_table"
    const val TABLE_NAME_SETTING = "setting_table"

    const val SELECT_ALL_COUNTRIES = "SELECT * FROM" + " " + TABLE_NAME_COUNTRIES
    const val SELECT_SETTING = "SELECT * FROM" + " " + TABLE_NAME_SETTING
    const val DELETE_ALL_COUNTRIES = "DELETE FROM" + " " + TABLE_NAME_COUNTRIES
    const val SELECT_FOLLOWING_LEAGUE = "SELECT * FROM" + " " + TABLE_NAME_FAVORITE_LEAGUES

    const val SUBST = "subst"
    const val GOAL = "Goal"
    const val CARD = "Card"
}