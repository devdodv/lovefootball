package app.himaproduction.lifefootball.ui.league

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.Standing
import com.bumptech.glide.Glide
import kotlin.collections.ArrayList

/**
 * Created by vando on 3/16/18.
 */
class StandingAdapter(private val mContext: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val standings: ArrayList<Any>
    private var tab = 0
    private var callback: OnItemClick? =
        null

    override fun getItemViewType(position: Int): Int {
        return if (standings[position] is String) GROUP_TYPE else ITEM_TYPE
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return if (viewType == ITEM_TYPE) {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_team_standing, parent, false)
            CustomViewHolder(itemView)
        } else {
            val groupView = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_group_standing, parent, false)
            GroupViewHolder(groupView)
        }
    }

    /**
     * Populate the views with appropriate Text and Images
     *
     * @param holderType
     * @param position
     */
    override fun onBindViewHolder(
        holderType: RecyclerView.ViewHolder,
        position: Int
    ) {
        val type = getItemViewType(position)
        when (type) {
            ITEM_TYPE -> {
                val holder = holderType as CustomViewHolder
                val standing = standings[position] as Standing
                Glide.with(mContext).load(standing.logo).placeholder(R.drawable.icon_football)
                    .into(holder.ivClub!!)
                holder.tvClub!!.text = standing.teamName
                holder.tvClubNo!!.text = standing.rank.toString() + ""
                holder.tvPoints!!.text = standing.points.toString() + ""
                when (tab) {
                    0 -> {
                        holder.tvTotalGame!!.text = standing.all!!.matchsPlayed.toString() + ""
                        holder.tvWin!!.text = standing.all!!.win.toString() + ""
                        holder.tvDraw!!.text = standing.all!!.draw.toString() + ""
                        holder.tvLose!!.text = standing.all!!.lose.toString() + ""
                        holder.tvGoal!!.text =
                            standing.all!!.goalsFor.toString() + "/" + standing.all!!.goalsAgainst
                    }
                    1 -> {
                        holder.tvTotalGame!!.text = standing.home!!.matchsPlayed.toString() + ""
                        holder.tvWin!!.text = standing.home!!.win.toString() + ""
                        holder.tvDraw!!.text = standing.home!!.draw.toString() + ""
                        holder.tvLose!!.text = standing.home!!.lose.toString() + ""
                        holder.tvGoal!!.text =
                            standing.all!!.goalsFor.toString() + "/" + standing.all!!.goalsAgainst
                    }
                    2 -> {
                        holder.tvTotalGame!!.text = standing.away!!.matchsPlayed.toString() + ""
                        holder.tvWin!!.text = standing.away!!.win.toString() + ""
                        holder.tvDraw!!.text = standing.away!!.draw.toString() + ""
                        holder.tvLose!!.text = standing.away!!.lose.toString() + ""
                        holder.tvGoal!!.text =
                            standing.all!!.goalsFor.toString() + "/" + standing.all!!.goalsAgainst
                    }
                    else -> {
                        holder.tvTotalGame!!.text = standing.all!!.matchsPlayed.toString() + ""
                        holder.tvWin!!.text = standing.all!!.win.toString() + ""
                        holder.tvDraw!!.text = standing.all!!.draw.toString() + ""
                        holder.tvLose!!.text = standing.all!!.lose.toString() + ""
                        holder.tvGoal!!.text =
                            standing.all!!.goalsFor.toString() + "/" + standing.all!!.goalsAgainst
                    }
                }
                holder.layoutClubContent!!.setOnClickListener {
                    if (callback != null) {
                        callback!!.onItemClick(standing.teamId!!)
                    }
                }
            }
            GROUP_TYPE -> {
                val groupHolder = holderType as GroupViewHolder
                val groupName = standings[position] as String
                groupHolder.tvGroupName!!.text = groupName
            }
            else -> {
            }
        }
    }

    override fun getItemCount(): Int {
        return standings.size
    }

    fun setTab(i: Int) {
        tab = i
        notifyDataSetChanged()
    }

    inner class CustomViewHolder(itemView: View?) :
        RecyclerView.ViewHolder(itemView!!) {
        var tvClubNo: TextView? = null
        var tvClub: TextView? = null
        var tvTotalGame: TextView? = null
        var tvWin: TextView? = null
        var tvDraw: TextView? = null
        var tvLose: TextView? = null
        var tvGoal: TextView? = null
        var tvPoints: TextView? = null
        var ivClub: ImageView? = null
        var layoutClubContent: ConstraintLayout? = null

        /**
         * Constructor to initialize the Views
         *
         * @param itemView
         */
        init {
            tvClubNo = itemView!!.findViewById(R.id.tvNo)
            tvClub = itemView!!.findViewById(R.id.tvTeamName)
            tvTotalGame = itemView!!.findViewById(R.id.tvTotalMatches)
            tvWin = itemView!!.findViewById(R.id.tvWin)
            tvDraw = itemView!!.findViewById(R.id.tvDraw)
            tvLose = itemView!!.findViewById(R.id.tvLost)
            tvGoal = itemView!!.findViewById(R.id.tvFA)
            tvPoints = itemView!!.findViewById(R.id.tvPoints)
            ivClub = itemView!!.findViewById(R.id.imvLogo)
            layoutClubContent = itemView!!.findViewById(R.id.layouTeam)

        }
    }

    interface OnItemClick {
        fun onItemClick(teamId: Int)
    }

    fun setCallback(callback: OnItemClick?) {
        this.callback = callback
    }

    fun setData(standings: ArrayList<Any>) {
        tab = 0
        this.standings.clear()
        this.standings.addAll(standings)
        notifyDataSetChanged()
    }

    inner class GroupViewHolder(itemView: View?) :
        RecyclerView.ViewHolder(itemView!!) {
        var tvGroupName: TextView? = null

        /**
         * Constructor to initialize the Views
         *
         * @param itemView
         */
        init {
            tvGroupName = itemView?.findViewById(R.id.tvGroupName)
        }
    }

    companion object {
        private const val ITEM_TYPE = 0
        private const val GROUP_TYPE = 1
    }

    init {
        standings = ArrayList()
    }
}