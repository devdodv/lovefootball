package app.himaproduction.lifefootball.ui.competition

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.Country
import app.himaproduction.lifefootball.data.entity.League
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.himaproduction.lifefootball.ui.bottomsheet.BottomSheetFragment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_countries.*


class CountriesFragment : BaseFragment(), CountriesAdapter.OnCountryItemClick {
    var countries: ArrayList<Country> = arrayListOf()
    var countriesAdapter: CountriesAdapter? = null;
    var country: Country? = null

    override fun setupViewModel() {
        setupObserverLive(mainViewModel.getCountriesLiveData(), Utils.GET_COUNTRIES_TAG)
        setupObserverLive(mainViewModel.getLeaguesFromCountryLiveData(),Utils.GET_LEAGUES_FROM_COUNTRIES_TAG)
        setupObserverLive(followViewModel.getFollowingLeaguesLiveData(),Utils.GET_FOLLOWING_LEAGUES_TAG)
    }

    override fun setupEventListener() {
        countriesAdapter?.setListener(this)
    }

    override fun setupView() {
        countriesAdapter = activity?.let {
            CountriesAdapter(
                it,
                arrayListOf()
            )
        }
        recycler_view_countries.apply {
            adapter = countriesAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(keyword: String?): Boolean {
                countriesAdapter?.search(keyword)
                return false
            }
        })

    }

    override fun setupExposeData(data: Any, tag: String) {
        val obj = when {
            tag.contentEquals(Utils.GET_COUNTRIES_TAG) -> {
                val result = data!! as ArrayList<Country>
                countriesAdapter?.updateData(
                    data!! as ArrayList<Country>,
                    false
                )
            }
            tag.contentEquals(Utils.GET_LEAGUES_FROM_COUNTRIES_TAG) -> {
                var result = data as ArrayList<League>
                DataHelper.leaguesByCountry = result
                if (country != null) {
                    val bottomSheetFragment = BottomSheetFragment.newInstance(country!!)
                    bottomSheetFragment.show(
                        activity?.supportFragmentManager,
                        bottomSheetFragment.tag
                    )
                } else {
                    return
                }
            }
            tag.contentEquals(Utils.GET_FOLLOWING_LEAGUES_TAG) ->{
                DataHelper.followingLeagues = data as ArrayList<League>
            }
            else -> ""
        }
    }

    @SuppressLint("ShowToast")
    override fun setupExposeError(errorMessage: String, tag: String) {
        when {
            tag.contentEquals(Utils.GET_LEAGUES_FROM_COUNTRIES_TAG) ->
                Toast.makeText(
                    activity!!,
                    resources.getText(R.string.no_internet_error),
                    Toast.LENGTH_LONG
                ).show()
            else -> {
                showError(errorMessage, object : OnRetry {
                    override fun onRetry() {
                        onLoadData()
                    }
                })
            }
        }

    }

    override fun setupExposeLoading(tag: String) {
        when(tag){
            Utils.GET_LEAGUES_FROM_COUNTRIES_TAG->showProgressDialog(Utils.loadingMsg)
        }

    }

    override fun onLoadData() {
        /*
        * Fetch following leagues same time to show status when show leagues choose from country
        */
        mainViewModel.fetchCountries()
    }

    override fun onCreateView() {
        rootView = R.layout.fragment_countries
    }

    override fun onViewCreated() {
        setupView()
        setupEventListener()
        setupViewModel()
    }

    override fun onCountryClick(country: Country) {
        this.country = country
        val let = country.country?.let { mainViewModel.fetchLeaguesFromCountry(it, "2019") }
        followViewModel.fetchFollowingLeagues()
    }
}