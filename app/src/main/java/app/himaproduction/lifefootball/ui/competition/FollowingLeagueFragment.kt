package app.himaproduction.lifefootball.ui.competition

import android.content.Intent
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import app.himaproduction.lifefootball.Const
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.League
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import kotlinx.android.synthetic.main.fragment_countries.*
import kotlinx.android.synthetic.main.fragment_following_league.*

class FollowingLeagueFragment :
    BaseFragment(), FollowingLeagueAdapter.OnItemClick {
    private lateinit var followingLeagueAdapter: FollowingLeagueAdapter
    private var positionOfUnFollowLeague: Int = -1
    private var unFollowLeague:League? = null
    override fun onCreateView() {
        rootView = R.layout.fragment_following_league
    }

    override fun setupViewModel() {
        setupObserverLive(followViewModel.getFollowingLeaguesLiveData(), Const.TAG_GET_FOLLOWING_LEAGUES)
        setupObserverLive(followViewModel.unFollowLeagueLiveData(), Const.TAG_UNFOLLOW_LEAGUE)
        setupObserverLive(followViewModel.addFollowingLeagueLiveData(),Const.TAG_FOLLOW_LEAGUE)
    }

    override fun setupEventListener() {
        followingLeagueAdapter.setListener(this)
    }

    override fun setupView() {
        followingLeagueAdapter = context?.let { FollowingLeagueAdapter(it, arrayListOf()) }!!
        recycler_view_following_leagues.apply {
            adapter = followingLeagueAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        }
    }

    override fun setupExposeData(data: Any, tag: String) {
        when(tag){
            Const.TAG_GET_FOLLOWING_LEAGUES->followingLeagueAdapter.updateData(data as List<League>)
            Const.TAG_FOLLOW_LEAGUE->followingLeagueAdapter.newFollow(data as League)
            Const.TAG_UNFOLLOW_LEAGUE ->followingLeagueAdapter.unFollow(data as League)
        }
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
        showError(errorMessage, null)
    }

    override fun setupExposeLoading(tag: String) {
        showLoadingDialog(Utils.loadingMsg)
    }

    override fun onLoadData() {
        setupViewModel()
        followViewModel.fetchFollowingLeagues()
    }

    override fun onViewCreated() {
        setupView()
        setupEventListener()
    }

    override fun onLeagueClick(league: League) {
        DataHelper.selectedLeague = league
        val intent = Intent(context, CompetitionDetailsActivity::class.java)
        activity?.startActivity(intent)
    }

    override fun onUnfollow(
        pos: Int,
        league: League
    ) {
        unFollowLeague = league
        positionOfUnFollowLeague = pos
        followViewModel.unFollowLeague(league)
    }

    override fun onUnfollowAll() {
        prepareShowNodata()
    }
}