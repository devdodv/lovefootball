package app.himaproduction.lifefootball.ui.user

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.SharedPref
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.himaproduction.lifefootball.ui.state.DataState
import kotlinx.android.synthetic.main.fragment_profile.*

class UserFragment : BaseFragment() {

    override fun onCreateView() {
        rootView = R.layout.fragment_profile
    }

    override fun setupViewModel() {
    }

    override fun setupEventListener() {
    }

    override fun setupView() {
        if (context?.let { SharedPref(it).getUsername?.isEmpty() }!!) {
            btnEditNickname.visibility = View.INVISIBLE
            tvSave.visibility = View.VISIBLE
        } else {
            edtNickname.text =
                Editable.Factory.getInstance().newEditable(SharedPref(context!!).getUsername)
            edtNickname.isEnabled = false
            btnEditNickname.visibility = View.VISIBLE
            tvSave.visibility = View.INVISIBLE
        }
        tvSave.setOnClickListener {
            if (edtNickname.text.toString().isNullOrEmpty()) {
                return@setOnClickListener
            }
            SharedPref(context!!).setUsername(edtNickname.text.toString().trim())
            edtNickname.isEnabled = false
            btnEditNickname.visibility = View.VISIBLE
            tvSave.visibility = View.INVISIBLE
        }

        btnEditNickname.setOnClickListener {
            tvSave.visibility = View.VISIBLE
            btnEditNickname.visibility = View.INVISIBLE
            edtNickname.isEnabled = true
        }
        btnSendFeedback.setOnClickListener {
            btnSendFeedback.text = "Send"
            var feedbackMsg = edtFeedback.text.toString().trim()
            if (feedbackMsg.isNullOrEmpty())
                return@setOnClickListener
            mainViewModel.postFeedback(feedbackMsg, SharedPref(context!!).getUsername)
            mainViewModel.getPostFeedback().observe(this, Observer {
                when(it){
                    is DataState.Loading->{
                        showProgressDialog("Sending...")
                    }
                    is DataState.Success-> {
                        hideProgressDialog()
                        Toast.makeText(context, "Thank you in advance", Toast.LENGTH_SHORT).show()
                        edtFeedback.text = Editable.Factory.getInstance().newEditable("")
                    }
                    is DataState.Error->{
                        hideProgressDialog()
                        Toast.makeText(context, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
                        btnSendFeedback.text = "Resend"
                    }
                }
            })
        }
    }

    override fun setupExposeData(data: Any, tag: String) {
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
    }

    override fun setupExposeLoading(tag: String) {
    }

    override fun onLoadData() {
    }

    override fun onViewCreated() {
        setupView()
    }
}