package app.himaproduction.lifefootball.ui.league

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.Country
import app.himaproduction.lifefootball.data.entity.Topscorer


class TopscorerAdapter(
    private val context: Context,
    private val topscorers: ArrayList<Topscorer> = arrayListOf()
) :
    RecyclerView.Adapter<TopscorerAdapter.VH>() {
    private var mContext: Context? = null
    private var listener: OnCountryItemClick? = null

    init {
        mContext = context
    }

    class VH(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_top_scorer, parent, false)
        ) {
        private var tvNo:TextView
        private var tvPlayer:TextView
        private var tvTeam:TextView
        private var tvGoal:TextView
        private var tvGame:TextView
        private var layoutItem = null
        init {
            tvNo = itemView.findViewById(R.id.tvNo)
            tvPlayer = itemView.findViewById(R.id.tvPlayer)
            tvTeam = itemView.findViewById(R.id.tvTeam)
            tvGoal = itemView.findViewById(R.id.tvGoal)
            tvGame = itemView.findViewById(R.id.tvGame)
        }

        fun bind(
            scorer: Topscorer,
            context: Context,
            position: Int
        ) {
            tvNo.text = (position+1).toString()
            tvPlayer.text = scorer.playerName
            tvTeam.text = scorer.teamName
            tvGoal.text = scorer.goals?.total.toString()
            tvGame.text = scorer.games?.appearences.toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val inflater = LayoutInflater.from(parent.context)
        return VH(
            inflater,
            parent
        )
    }

    override fun getItemCount(): Int {
        return topscorers.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(topscorers[position], context, position)
    }

    fun updateData(list: List<Topscorer>) {
        topscorers.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(callback: OnCountryItemClick) {
        this.listener = callback
    }

    interface OnCountryItemClick {
        fun onCountryClick(country: Country)
    }
}
