package app.himaproduction.lifefootball.ui.league

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.Const
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.BaseHomeAway
import app.himaproduction.lifefootball.data.entity.Event
import app.himaproduction.lifefootball.data.entity.Fixture
import app.himaproduction.lifefootball.data.entity.Statistic
import app.himaproduction.lifefootball.ui.custom.RoundedView
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.bumptech.glide.Glide


class StatisticAdapter(
    private val context: Context,
    private val items: ArrayList<BaseHomeAway> = arrayListOf()
) :
    RecyclerView.Adapter<StatisticAdapter.VH>() {
    private var mContext: Context? = null
    private var listener: OnFixtureClick? = null

    init {
        mContext = context
    }

    class VH(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_statistic, parent, false)
        ) {
        private var tvLabel: TextView
        private var tvValueA: TextView
        private var tvValueB: TextView


        init {
            tvLabel = itemView.findViewById(R.id.tvLabel)
            tvValueA = itemView.findViewById(R.id.tvValueA)
            tvValueB = itemView.findViewById(R.id.tvValueB)

        }

        fun bind(
            item: BaseHomeAway,
            context: Context,
            listener: OnFixtureClick?
        ) {

            when (item) {
                is Statistic.BallPossession -> tvLabel.text = "Ball possession"
                is Statistic.BlockedShots -> tvLabel.text = "Blocked shots"
                is Statistic.CornerKicks -> tvLabel.text = "Corner kicks"
                is Statistic.Fouls -> tvLabel.text = "Fouls"
                is Statistic.GoalkeeperSaves -> tvLabel.text = "Goalkeeper saves"
                is Statistic.Offsides -> tvLabel.text = "Offside"
                is Statistic.Passes -> tvLabel.text = "% pass accurate"
                is Statistic.PassesAccurate -> tvLabel.text = "Passes accurate"
                is Statistic.RedCards -> tvLabel.text = "Red cards"
                is Statistic.ShotsInsidebox -> tvLabel.text = "Shots inside box"
                is Statistic.ShotsOffGoal -> tvLabel.text = "Shots off goal"
                is Statistic.ShotsOnGoal -> tvLabel.text = "Shots on goal"
                is Statistic.ShotsOutsidebox -> tvLabel.text = "Shots outside box"
                is Statistic.TotalPasses -> tvLabel.text = "Total passes"
                is Statistic.TotalShots -> tvLabel.text = "Total shots"
                is Statistic.YellowCards -> tvLabel.text = "Yellow cards"
            }
            tvValueA.text = item.home
            tvValueB.text = item.away
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatisticAdapter.VH {
        val inflater = LayoutInflater.from(parent.context)
        return StatisticAdapter.VH(
            inflater,
            parent
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: StatisticAdapter.VH, position: Int) {
        holder.bind(items[position], context, listener)
    }

    fun updateData(list: List<BaseHomeAway>, clearOldData: Boolean = true) {
        if (clearOldData)
            items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(callback: OnFixtureClick) {
        this.listener = callback
    }

    interface OnFixtureClick {
        fun onFixtureClick(fixture: Fixture)
        fun onTeamClick(team: Int)
    }
}
