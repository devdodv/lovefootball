package app.himaproduction.lifefootball.ui.player

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.Player
import app.himaproduction.lifefootball.data.entity.Team
import com.bumptech.glide.Glide


class PlayerAdapter(
    private val context: Context,
    private val players: ArrayList<Player> = arrayListOf()
) :
    RecyclerView.Adapter<PlayerAdapter.CountriesVH>() {
    private var mContext: Context? = null
    private var listener: OnTeamClick? = null

    init {
        mContext = context
    }

    class CountriesVH(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_player, parent, false)
        ) {
        private var tvPlayerName: TextView
        private var tvPlayerPos: TextView

        init {
            tvPlayerName = itemView.findViewById(R.id.tvPlayerName)
            tvPlayerPos = itemView.findViewById(R.id.tvPosition)
        }

        fun bind(
            player: Player,
            context: Context,
            listener: OnTeamClick?
        ) {
            tvPlayerName.text = player.playerName
            tvPlayerPos.text = player.position
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountriesVH {
        val inflater = LayoutInflater.from(parent.context)
        return CountriesVH(
            inflater,
            parent
        )
    }

    override fun getItemCount(): Int {
        return players.size
    }

    override fun onBindViewHolder(holder: CountriesVH, position: Int) {
        holder.bind(players[position], context, listener)
    }

    fun updateData(list: ArrayList<Player>, clearOldData: Boolean = true) {
        if (clearOldData)
            players.clear()
        players.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(callback: OnTeamClick) {
        this.listener = callback
    }

    interface OnTeamClick {
        fun onTeamClick(team: Team)
    }
}
