package app.himaproduction.lifefootball.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import app.himaproduction.lifefootball.App
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.ui.base.BaseActivity
import app.himaproduction.lifefootball.ui.competition.CompetitionFragment
import app.himaproduction.lifefootball.ui.league.PredictionFragment
import app.himaproduction.lifefootball.ui.livescore.LiveScoreFragment
import app.himaproduction.lifefootball.ui.media.MediaFragment
import app.himaproduction.lifefootball.ui.tip.TipFragment
import app.himaproduction.lifefootball.ui.user.UserFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_toolbar.*


class MainActivity : BaseActivity() {
    private val mediaFragment = MediaFragment()
    private val competitionFragment = CompetitionFragment()
    private val tipFragment = PredictionFragment()
    private val userFragment = UserFragment()
    private var activeFragment: Fragment? = null

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_media -> {
                    addFragment(mediaFragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.nav_competition -> {
                    toolbar!!.title = "Competitions"
                    addFragment(competitionFragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.nav_live_score -> {
                    toolbar!!.title = "Tip"
                    addFragment(tipFragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.nav_news -> {
                    toolbar!!.title = "User"
                    addFragment(userFragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    private fun addFragment(fragment: Fragment) {
        if (fragment != null) {
            if (fragment == activeFragment)
                return
            if (!fragment.isAdded) {
                supportFragmentManager.beginTransaction()
                    .add(frame_container.id, fragment)
                    .hide(activeFragment!!)
                    .commit()
            } else {
                supportFragmentManager.beginTransaction()
                    .show(fragment)
                    .hide(activeFragment!!)
                    .commit()
            }
            activeFragment = fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnBackToolbar.visibility = View.GONE
        setupToolbar("",getString(R.string.app_name))
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(false);
        supportFragmentManager.beginTransaction().add(frame_container.id, tipFragment).commit()
        activeFragment = tipFragment
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        App.self().loadBannerSmall()
        App.self().loadNativeAds()
    }

    override fun setupView() {
        TODO("Not yet implemented")
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onReloadData() {
        TODO("Not yet implemented")
    }
}
