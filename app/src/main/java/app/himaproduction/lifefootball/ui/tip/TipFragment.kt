package app.himaproduction.lifefootball.ui.tip

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.Country
import app.himaproduction.lifefootball.data.entity.PredictionFixture
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.himaproduction.lifefootball.ui.bottomsheet.BottomSheetFragment
import kotlinx.android.synthetic.main.fragment_prediction_fixtures.*

class TipFragment : BaseFragment(), TipFixturesAdapter.OnFixtureClick {
    private var fixturesAdapter:TipFixturesAdapter? = null
    private var url:String? = null
    companion object{
        fun newInstance(url: String) = TipFragment().apply {
            arguments = Bundle().apply {
                putString("url", url)
            }
        }
    }

    override fun onCreateView() {
        rootView = R.layout.fragment_prediction_fixtures
    }

    override fun setupViewModel() {
        setupObserverLive(mainViewModel.getPredictionFixturesLiveData(), "")
    }

    override fun setupEventListener() {
    }

    override fun setupView() {
        fixturesAdapter = activity?.let {
            TipFixturesAdapter(
                it
            )
        }
        fixturesAdapter?.setListener(this)
        recycler_view_prediction_fixtures.apply {
            adapter = fixturesAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        }
    }

    override fun setupExposeData(data: Any, tag: String) {
        fixturesAdapter?.updateData(data as ArrayList<PredictionFixture>)
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
        showError(errorMessage,null)
    }

    override fun setupExposeLoading(tag: String) {
        showLoadingDialog(Utils.loadingMsg)
    }

    override fun onLoadData() {
        if (arguments != null) {
            url = arguments?.getString("url")!!
        }
        url?.let { mainViewModel.getPredictionFixtures(it) }
    }

    override fun onViewCreated() {
        setupView()
        setupViewModel()
    }

    override fun onFixtureClick(fixture: PredictionFixture) {
        TODO("Not yet implemented")
    }
}