package app.himaproduction.lifefootball.ui.livescore

import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import app.himaproduction.lifefootball.App
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.ui.ViewPagerAdapter
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.himaproduction.lifefootball.ui.competition.CountriesFragment
import app.himaproduction.lifefootball.ui.competition.FollowingLeagueFragment
import kotlinx.android.synthetic.main.fragment_competitions.*
import kotlinx.android.synthetic.main.tab_layout.*

class LiveScoreFragment : BaseFragment() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView() {
        rootView = R.layout.fragment_live_score
    }

    override fun setupViewModel() {

    }

    override fun setupEventListener() {

    }

    override fun setupExposeData(data: Any, tag: String) {

    }

    override fun setupExposeError(errorMessage: String, tag: String) {
        TODO("Not yet implemented")
    }

    override fun setupExposeLoading(tag: String) {
        TODO("Not yet implemented")
    }

    override fun onLoadData() {
    }

    override fun onViewCreated() {
        setupView()
        setupViewModel()
    }

    override fun setupView() {
        setupViewPager(viewPager)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter =
            ViewPagerAdapter(
                childFragmentManager
            )
        adapter.addFragment(
            CountriesFragment(),
            App.self().resources.getString(R.string.lb_score_live)
        )
        adapter.addFragment(
            FollowingLeagueFragment(),
            App.self().resources.getString(R.string.lb_score_today)
        )
        viewPager.offscreenPageLimit = 2
        viewPager.adapter = adapter
        tab_layout.setupWithViewPager(this.viewPager)
    }
}