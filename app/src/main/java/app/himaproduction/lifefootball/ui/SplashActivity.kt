package app.himaproduction.lifefootball.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.Setting
import app.himaproduction.lifefootball.ui.base.BaseActivity
import app.himaproduction.lifefootball.ui.competition.CompetitionFragment
import app.himaproduction.lifefootball.ui.livescore.LiveScoreFragment
import app.himaproduction.lifefootball.ui.media.MediaFragment
import app.himaproduction.lifefootball.ui.state.DataState
import app.himaproduction.lifefootball.ui.user.UserFragment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.custom_toolbar.*


class SplashActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mainViewModel.getSetting()
        mainViewModel.settingLiveData().observe(this, Observer {
            when (it) {
                is DataState.Loading -> {
                    progressBar.visibility = View.VISIBLE
                    tvMessageError.visibility = View.INVISIBLE
                    btnRetry.visibility = View.INVISIBLE
                }
                is DataState.Success -> {
                    DataHelper.setting = it.data as Setting
                    progressBar.visibility = View.INVISIBLE
                    startActivity(Intent(this,MainActivity::class.java))
                    mainViewModel.settingLiveData()
                    finish()
                }
                is DataState.Error -> {
                    progressBar.visibility = View.GONE
                    tvMessageError.visibility = View.VISIBLE
                    btnRetry.visibility = View.VISIBLE
                    btnRetry.setOnClickListener{
                        mainViewModel.getSetting()
                    }
                }
            }
        })
    }

    override fun setupView() {

    }

    override fun onReloadData() {
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
    }
}
