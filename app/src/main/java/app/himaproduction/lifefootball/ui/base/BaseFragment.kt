package app.himaproduction.lifefootball.ui.base

import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.media.ThumbnailUtils
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.viewmodel.MainViewModel
import app.himaproduction.lifefootball.ui.custom.CustomProgressbar
import app.himaproduction.lifefootball.ui.state.DataState
import app.himaproduction.lifefootball.viewmodel.LeagueFollowViewModel
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.layout_error.*
import org.koin.android.viewmodel.ext.android.viewModel

abstract class BaseFragment : Fragment(){
    var isLoaded: Boolean = false
    val mainViewModel: MainViewModel by viewModel()
    val followViewModel: LeagueFollowViewModel by viewModel()
    var errorView: View? = null
    var rootView: Int = 0
    private val progress = CustomProgressbar()
    abstract fun onCreateView()
    abstract fun onViewCreated()
    abstract fun setupView()
    abstract fun setupViewModel()
    abstract fun onLoadData()
    abstract fun setupEventListener()
    abstract fun setupExposeLoading(tag: String)
    abstract fun setupExposeData(data: Any, tag: String)
    abstract fun setupExposeError(errorMessage: String, tag: String)


    interface OnRetry {
        fun onRetry()
    }

    fun showProgressDialog(msg: String) {
        context?.let { progress.show(it, msg) }
    }

    fun hideProgressDialog(){
        progress.hide()
    }

    fun showLoadingDialog(msg: String) {
//        context?.let { progress.show(it, msg) }
    }

    fun hideLoading() {
//        progress.hide()
        loading_indicator?.visibility = View.INVISIBLE
    }

    fun showError(errorMsg: String, callback: OnRetry?) {
        tvMessageError.text = errorMsg
        if (callback == null || errorMsg.contentEquals(Utils.noDataMsg))
            btnRetry.visibility = View.INVISIBLE
        else
            btnRetry.visibility = View.VISIBLE
        btnRetry.setOnClickListener { callback!!.onRetry() }
    }

    fun prepareShowData() {
        layout_error?.visibility = View.GONE
    }

    fun prepareShowError() {
        layout_error?.visibility = View.VISIBLE
//        btnRetry?.visibility = View.VISIBLE
        tvMessageError?.visibility = View.VISIBLE
        loading_indicator?.visibility = View.INVISIBLE
    }

    fun prepareShowNodata() {
        layout_error?.visibility = View.VISIBLE
        btnRetry?.visibility = View.VISIBLE
        tvMessageError?.visibility = View.VISIBLE
        tvMessageError.text = Utils.noDataMsg
    }

    private fun showProgressIndicator() {
        loading_indicator?.visibility = View.VISIBLE
        btnRetry?.visibility = View.INVISIBLE
        tvMessageError?.visibility = View.INVISIBLE
    }

    fun setupObserverLive(liveData: MutableLiveData<DataState>, tag: String) {
        liveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                is DataState.Loading -> {
                    showProgressIndicator()
                    setupExposeLoading(tag)
                }
                is DataState.Success -> {
                    isLoaded = true
                    hideLoading()
                    hideProgressDialog()
                    setupExposeData(it.data!!, tag)
                    prepareShowData()
                }
                is DataState.Error -> {
                    isLoaded = true
                    hideLoading()
                    hideProgressDialog()
                    setupExposeError(it.errorMessage!!, tag)
                    if (!tag.contentEquals(Utils.GET_LEAGUES_FROM_COUNTRIES_TAG))
                        prepareShowError()
                }
            }
        })
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && isResumed && !isLoaded) {
            onLoadData()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (userVisibleHint)
            onLoadData()
        onCreateView()
        return inflater.inflate(rootView, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onViewCreated()
    }
}