package app.himaproduction.lifefootball.ui.league

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.Fixture
import com.bumptech.glide.Glide


class FixtureAdapter(
    private val context: Context,
    private val fixtures: ArrayList<Fixture> = arrayListOf()
) :
    RecyclerView.Adapter<FixtureAdapter.CountriesVH>() {
    private var mContext: Context? = null
    private var listener: OnFixtureClick? = null

    init {
        mContext = context
    }

    class CountriesVH(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_fixture, parent, false)
        ) {
        private var tvFixtureDate: TextView? = null
        private var tvTeamA: TextView? = null
        private var tvScoreTeamA: TextView? = null
        private var tvTeamB: TextView? = null
        private var tvScoreTeamB: TextView? = null
        private var tvStatus: TextView? = null
        private var imvTeamALogo: ImageView? = null
        private var imvTeamBLogo: ImageView? = null
        private var layoutItem: ConstraintLayout? = null

        init {
            layoutItem = itemView.findViewById(R.id.layout_item)
            tvTeamA = itemView.findViewById(R.id.tvTeamA)
            tvScoreTeamA = itemView.findViewById(R.id.tvScoreTeamA)
            tvScoreTeamB = itemView.findViewById(R.id.tvScoreTeamB)
            tvTeamB = itemView.findViewById(R.id.tvTeamB)
            imvTeamALogo = itemView.findViewById(R.id.imvTeamA)
            imvTeamBLogo = itemView.findViewById(R.id.imvTeamB)
            tvFixtureDate = itemView.findViewById(R.id.tvFixtureDate)
            tvStatus = itemView.findViewById(R.id.tvStatus)
        }

        fun bind(
            fixture: Fixture,
            context: Context,
            listener: OnFixtureClick?
        ) {
            tvFixtureDate?.text = Utils.convertTimestampToDate(fixture?.eventTimestamp!!)
            tvTeamA?.text = fixture?.homeTeam?.team_name
            tvTeamB?.text = fixture?.awayTeam?.team_name
            if (fixture.goalsHomeTeam.toString().equals("null")) {
                tvScoreTeamA?.text = ""
                tvScoreTeamB?.text = ""
                tvStatus?.text = " vs "
            } else {
                tvScoreTeamA?.text = fixture.goalsHomeTeam.toString()
                tvScoreTeamB?.text = fixture.goalsAwayTeam.toString()
                tvStatus?.text = ":"
            }
            Glide.with(context).load(fixture.homeTeam?.logo).placeholder(R.drawable.icon_football)
                .error(R.drawable.icon_football).into(imvTeamALogo!!)
            Glide.with(context).load(fixture.awayTeam?.logo).placeholder(R.drawable.icon_football)
                .error(R.drawable.icon_football).into(imvTeamBLogo!!)
            layoutItem?.setOnClickListener{
                listener?.onFixtureClick(fixture)
            }
            tvTeamA?.setOnClickListener{
                listener?.onTeamClick(fixture?.homeTeam?.team_id!!)
            }
            tvTeamB?.setOnClickListener{
                listener?.onTeamClick(fixture?.awayTeam?.team_id!!)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountriesVH {
        val inflater = LayoutInflater.from(parent.context)
        return CountriesVH(
            inflater,
            parent
        )
    }

    override fun getItemCount(): Int {
        return fixtures.size
    }

    override fun onBindViewHolder(holder: CountriesVH, position: Int) {
        holder.bind(fixtures[position], context, listener)
    }

    fun updateData(list: ArrayList<Fixture>, clearOldData: Boolean = true) {
        if (clearOldData)
            fixtures.clear()
        fixtures.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(callback: OnFixtureClick) {
        this.listener = callback
    }

    interface OnFixtureClick{
        fun onFixtureClick(fixture: Fixture)
        fun onTeamClick(team: Int)
    }
}
