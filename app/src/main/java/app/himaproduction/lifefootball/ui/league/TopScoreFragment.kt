package app.himaproduction.lifefootball.ui.league

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.Topscorer
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import kotlinx.android.synthetic.main.fragment_topscorers.*

class TopScoreFragment : BaseFragment() {
    private var topscorerAdapter: TopscorerAdapter? = null
    override fun onCreateView() {
        rootView = R.layout.fragment_topscorers
    }
    override fun onViewCreated() {
        setupViewModel()
        setupView()
    }

    override fun setupViewModel() {
        setupObserverLive(mainViewModel.getTopScorerFromLeagueLiveData(),"")
    }

    override fun setupView() {
        topscorerAdapter = context?.let { TopscorerAdapter(it, arrayListOf()) }
        recyclerview_top_scorer.apply {
            adapter = topscorerAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        }
    }
    override fun setupEventListener() {
        TODO("Not yet implemented")
    }

    override fun setupExposeData(data: Any, tag: String) {
        topscorerAdapter?.updateData(data as List<Topscorer>)
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
       showError(errorMessage,object : OnRetry{
           override fun onRetry() {
               onLoadData()
           }
       })
    }

    override fun setupExposeLoading(tag: String) {
        showLoadingDialog(Utils.loadingMsg)
    }

    override fun onLoadData() {
       mainViewModel.fetchTopScorerFromLeague(DataHelper.selectedLeague!!.league_id)
    }
}