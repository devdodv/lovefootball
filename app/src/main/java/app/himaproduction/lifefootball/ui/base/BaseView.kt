package app.himaproduction.lifefootball.ui.base

import android.widget.ImageView

interface BaseView {
    fun onReloadData()
    fun setupReloadButton(btnReload: ImageView) {
        btnReload.setOnClickListener {
            onReloadData()
        }
    }
}