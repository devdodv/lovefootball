package app.himaproduction.lifefootball.ui.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import app.himaproduction.lifefootball.ConnectivityReceiver
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.League
import app.himaproduction.lifefootball.data.entity.Team
import app.himaproduction.lifefootball.data.repository.Repository
import app.himaproduction.lifefootball.ui.competition.CompetitionDetailsActivity
import app.himaproduction.lifefootball.ui.custom.CustomProgressbar
import app.himaproduction.lifefootball.ui.state.DataState
import app.himaproduction.lifefootball.viewmodel.LeagueFollowViewModel
import app.himaproduction.lifefootball.viewmodel.MainViewModel
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.bumptech.glide.Glide
import com.google.android.gms.ads.MobileAds
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.custom_toolbar.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel


abstract class BaseActivity : AppCompatActivity(), BaseView,
    ConnectivityReceiver.ConnectivityReceiverListener {
    private val progress = CustomProgressbar()
    lateinit var toolbar: Toolbar
    val mainViewModel: MainViewModel by viewModel()
    val leagueFollowViewModel: LeagueFollowViewModel by viewModel()
    var registerReceiver: BroadcastReceiver? = null
    private var selectedLeague: League? = null
    private var selectedTeam: Team? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbar = layoutInflater.inflate(R.layout.custom_toolbar, null) as Toolbar
        selectedLeague = DataHelper.selectedLeague
        selectedTeam = DataHelper.selectedTeam
        MobileAds.initialize(this)
        registerReceiver = ConnectivityReceiver()
        registerReceiver(
            registerReceiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(registerReceiver)
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showNetworkMessage(isConnected)
    }

    private fun showNetworkMessage(isConnected: Boolean) {
        if (!isConnected) {
            layout_internet_status?.visibility = View.VISIBLE
        } else {
            layout_internet_status?.visibility = View.GONE
        }
    }

    fun setupToolbar(flag: String?, name: String?) {
        title_toolbar?.text = name
        when (this) {
            is CompetitionDetailsActivity -> {
//                mainViewModel.isFollowedLeagueLiveData().observe(this, Observer {
//                    if (it) {
//                        displayUnfollowBtn()
//                        btn_add_to_league_favorite.setOnClickListener {
//                            leagueFollowViewModel.unFollowLeague(selectedLeague!!)
//                            mainViewModel.isFollowedLeague(DataHelper.selectedLeague?.league_id!!)
//                        }
//                    } else {
//                        displayFollowBtn()
//                        btn_add_to_league_favorite.setOnClickListener {
//                            leagueFollowViewModel.addFollowingLeague(selectedLeague!!)
//                            mainViewModel.isFollowedLeague(DataHelper.selectedLeague?.league_id!!)
//                        }
//                    }
//                })
//                mainViewModel.isFollowedLeague(DataHelper.selectedLeague?.league_id!!)
            }
            else -> btn_add_to_league_favorite?.visibility = View.INVISIBLE
        }
        if (!flag.isNullOrEmpty())
            Glide.with(this).load(Uri.parse(flag)).into(logo)
        else
            logo.visibility = View.GONE
        btnBackToolbar.setOnClickListener { finish() }
    }

    fun displayUnfollowBtn() {
        btn_add_to_league_favorite?.visibility = View.VISIBLE
        btn_add_to_league_favorite.text = getString(R.string.lb_unfollow)
        btn_add_to_league_favorite.setTextColor(resources.getColor(R.color.windowBackground))
        btn_add_to_league_favorite.setBackgroundResource(R.drawable.background_following_in_toolbar)
    }

    fun displayFollowBtn() {
        btn_add_to_league_favorite?.visibility = View.VISIBLE
        btn_add_to_league_favorite.text = getString(R.string.lb_follow)
        btn_add_to_league_favorite.setTextColor(resources.getColor(R.color.blue))
        btn_add_to_league_favorite.setBackgroundResource(R.drawable.background_follow)
    }

    abstract fun setupView()
    fun showLoadingDialog(msg: String) {
        progress.show(this, msg)
    }

    fun hideLoading() {
        progress.hide()
    }

    open fun hideKeyboard() { // Check if no view has focus:
        val view = this.currentFocus
        if (view != null) {
            val inputManager =
                this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                view.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }
}