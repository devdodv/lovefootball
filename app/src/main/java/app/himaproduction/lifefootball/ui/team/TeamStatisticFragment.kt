package app.himaproduction.lifefootball.ui.team

import androidx.recyclerview.widget.LinearLayoutManager
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import kotlinx.android.synthetic.main.fragment_team_statistic.*

class TeamStatisticFragment : BaseFragment() {
    lateinit var teamStatisticAdapter:TeamStatisticAdapter
    override fun onCreateView() {
        rootView = R.layout.fragment_team_statistic
    }

    override fun setupViewModel() {
        setupObserverLive(mainViewModel.getTeamStatisticLiveData(),"")
    }

    override fun setupEventListener() {

    }

    override fun setupView() {
        tv_league_name.text = DataHelper.selectedLeague?.name
        teamStatisticAdapter = TeamStatisticAdapter(context!!, arrayListOf())
        recycler_view_team_statistic.apply {
            adapter = teamStatisticAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun setupExposeData(data: Any, tag: String) {
        teamStatisticAdapter.updateData(data as ArrayList<Any>)
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
        showError(errorMessage!!,object :OnRetry{
            override fun onRetry() {
                onLoadData()
            }
        })
    }

    override fun setupExposeLoading(tag: String) {

    }

    override fun onLoadData() {
        mainViewModel.getTeamStatistic(DataHelper.selectedTeam?.teamId!!,DataHelper.selectedLeague?.league_id!!)
    }

    override fun onViewCreated() {
        setupView()
        setupViewModel()
    }
}