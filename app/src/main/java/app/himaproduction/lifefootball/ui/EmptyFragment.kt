package app.himaproduction.lifefootball.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.ui.base.BaseFragment

class EmptyFragment : BaseFragment() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_countries, container, false)
    }

    override fun setupViewModel() {
        TODO("Not yet implemented")
    }

    override fun setupEventListener() {
        TODO("Not yet implemented")
    }

    override fun setupView() {
        TODO("Not yet implemented")
    }

    override fun setupExposeData(data: Any, tag: String) {
        TODO("Not yet implemented")
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
        TODO("Not yet implemented")
    }

    override fun setupExposeLoading(tag: String) {
        TODO("Not yet implemented")
    }

    override fun onLoadData() {
//        TODO("Not yet implemented")
    }

    override fun onViewCreated() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}