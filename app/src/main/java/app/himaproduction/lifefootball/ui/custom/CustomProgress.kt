package app.himaproduction.lifefootball.ui.custom

import android.R
import android.content.Context
import android.graphics.PorterDuff
import android.util.AttributeSet
import android.widget.ProgressBar


class CustomProgress : ProgressBar {
    constructor(context: Context?) : super(context) {
        this.isIndeterminate = true
        this.indeterminateDrawable
            .setColorFilter(resources.getColor(R.color.holo_blue_light), PorterDuff.Mode.MULTIPLY)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        this.indeterminateDrawable
            .setColorFilter(resources.getColor(R.color.holo_blue_light), PorterDuff.Mode.MULTIPLY)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
    }
}