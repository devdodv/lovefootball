package app.himaproduction.lifefootball.ui.competition

import android.content.Context
import android.graphics.drawable.PictureDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.Country
import app.himaproduction.lifefootball.ui.custom.SvgSoftwareLayerSetter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade


class CountriesAdapter(
    private val context: Context,
    private val countries: ArrayList<Country> = arrayListOf()
) :
    RecyclerView.Adapter<CountriesAdapter.CountriesVH>() {
    private var mContext: Context? = null
    private var listener: OnCountryItemClick? = null
    var tempCountries:ArrayList<Country> = arrayListOf()
    init {
        mContext = context
    }

    class CountriesVH(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_country, parent, false)
        ) {
        private var tvCountryName: TextView? = null
        private var imvCountryFlag: ImageView? = null
        private var layoutItem: ConstraintLayout? = null

        init {
            layoutItem = itemView.findViewById(R.id.layout_item)
            tvCountryName = itemView.findViewById(R.id.tv_country_name)
            imvCountryFlag = itemView.findViewById(R.id.imv_country_flag)
        }

        fun bind(
            country: Country,
            context: Context,
            listener: OnCountryItemClick?
        ) {
            tvCountryName?.text = country.country
            if (country.flag != null) {
//                val requestBuilder: RequestBuilder<PictureDrawable> = Glide.with(context)
//                    .`as`(PictureDrawable::class.java)
//                    .transition(DrawableTransitionOptions.withCrossFade())
//                    .error(R.drawable.icon_football)
//                    .listener(SvgSoftwareLayerSetter())
//                val uri: Uri = Uri.parse(country.flag)
//                requestBuilder.load(uri)
//                    .into(imvCountryFlag!!)
                Glide.with(context!!)
                    .`as`(PictureDrawable::class.java)
                    .transition(withCrossFade())
                    .load(country.flag)
                    .listener(SvgSoftwareLayerSetter())
                    .into(imvCountryFlag!!)
            }
            layoutItem?.setOnClickListener { listener?.onCountryClick(country) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountriesVH {
        val inflater = LayoutInflater.from(parent.context)
        return CountriesVH(
            inflater,
            parent
        )
    }

    override fun getItemCount(): Int {
        return countries.size
    }

    override fun onBindViewHolder(holder: CountriesVH, position: Int) {
        holder.bind(countries[position], context, listener)
    }

    fun updateData(list: ArrayList<Country>, clearOldData: Boolean = true) {
        if (clearOldData)
            countries.clear()
        countries.addAll(list)
        tempCountries.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(callback: OnCountryItemClick) {
        this.listener = callback
    }

    fun search(keyword: String?) {
        countries.clear()
        if(keyword?.isEmpty()!!){
            countries.addAll(tempCountries)
            notifyDataSetChanged()
        }else{
            for(country in tempCountries){
                if(country.country.toLowerCase().contains(keyword.toLowerCase())){
                    countries.add(country)
                }
            }
            notifyDataSetChanged()
        }
    }

    interface OnCountryItemClick {
        fun onCountryClick(country: Country)
    }
}
