package app.himaproduction.lifefootball.ui.team

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import app.himaproduction.lifefootball.App
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.Team
import app.himaproduction.lifefootball.ui.ViewPagerAdapter
import app.himaproduction.lifefootball.ui.base.BaseActivity
import app.himaproduction.lifefootball.ui.bottomsheet.BottomSheetFragment
import app.himaproduction.lifefootball.ui.player.PlayersFragment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_competition_details.view_pager
import kotlinx.android.synthetic.main.activity_team_detail.*
import kotlinx.android.synthetic.main.tab_layout.*


class TeamDetailActivity : BaseActivity() {
    companion object {
        var competitionId: Long? = 0L
        var name: String? = ""
    }

    private var bottomSheetFragment: BottomSheetFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_detail)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(false)
        setupView()

    }

    override fun setupView() {
        var team:Team = DataHelper.selectedTeam!!
        tvCountry.text = team.country
        tvCity.text = team.venueCity
        tvAddress.text = team.venueAddress
        tvFounded.text = team.founded.toString()
        tvStadium.text = team.venueName
        tvCapacity.text = Utils.parseNumberFromString(team.venueCapacity.toString())
        Glide.with(this).load(team.logo).into(imv_team_name)
        setupViewPager(view_pager)
        setupToolbar("",team.name)
    }

    override fun onReloadData() {
        TODO("Not yet implemented")
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter =
            ViewPagerAdapter(
                supportFragmentManager
            )
        adapter.addFragment(PlayersFragment(), App.self().resources.getString(R.string.lb_player))
        adapter.addFragment(TeamStatisticFragment(), App.self().resources.getString(R.string.lb_statistic))
//        adapter.addFragment(TopScoreFragment(), App.self().resources.getString(R.string.lb_top_score))
//        adapter.addFragment(TeamFragment(), App.self().resources.getString(R.string.lb_team))
        viewPager.offscreenPageLimit = 4
        viewPager.adapter = adapter
        tab_layout.setupWithViewPager(view_pager)
    }

}
