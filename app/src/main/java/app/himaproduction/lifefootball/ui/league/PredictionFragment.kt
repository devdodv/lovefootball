package app.himaproduction.lifefootball.ui.league

import androidx.viewpager.widget.ViewPager
import app.himaproduction.lifefootball.App
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.ui.ViewPagerAdapter
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.himaproduction.lifefootball.ui.competition.FollowingLeagueFragment
import app.himaproduction.lifefootball.ui.tip.TipFragment
import kotlinx.android.synthetic.main.fragment_prediction.viewPager
import kotlinx.android.synthetic.main.tab_layout.*

class PredictionFragment : BaseFragment() {
    override fun onCreateView() {
        rootView = R.layout.fragment_prediction
    }

    override fun setupViewModel() {
    }

    override fun setupEventListener() {
    }

    override fun setupView() {
        setupViewPager(viewPager)
    }

    override fun setupExposeData(data: Any, tag: String) {
       
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
    }

    override fun setupExposeLoading(tag: String) {
    }

    override fun onLoadData() {
    }

    override fun onViewCreated() {
        setupView()
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter =
            ViewPagerAdapter(
                childFragmentManager
            )
        adapter.addFragment(
            TipFragment.newInstance("https://www.predictz.com/predictions/"),
            App.self().resources.getString(R.string.lb_prediction_today)
        )
        adapter.addFragment(
            TipFragment.newInstance("https://www.predictz.com/predictions/tomorow/"),
            App.self().resources.getString(R.string.lb_prediction_tomorow)
        )
        viewPager.offscreenPageLimit = 2
        viewPager.adapter = adapter
        tab_layout.setupWithViewPager(this.viewPager)
    }
}