package app.himaproduction.lifefootball.ui.bottomsheet

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.Media
import app.himaproduction.lifefootball.data.repository.Repository
import app.himaproduction.lifefootball.ui.base.BaseActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.layout_add_media.*
import org.koin.android.ext.android.inject
import java.io.ByteArrayOutputStream
import java.io.File
import javax.inject.Inject


class AddnewActivity : BaseActivity() {
    //Variables
    private var mediaUrl:String? = null
    private var mediaThumbnail:String? = null
    private var videoFile: File? = null
    private var thumbnailBitmap: Bitmap? = null
    private val GALLERY = 1
    private val PERMISSION_REQUEST_CODE: Int = 101


    //Firebase
    var storageReference: StorageReference? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_add_media)
        storageReference = FirebaseStorage.getInstance().getReference()

        //Initialize Views
        btnChooseFile!!.setOnClickListener { chooseImage() }
        btnSubmit!!.setOnClickListener {
            if(checkPersmission()){
            uploadImage() }
            else requestPermission()
        }
    }

    override fun setupView() {
        TODO("Not yet implemented")
    }

    private fun chooseImage() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        )

        startActivityForResult(galleryIntent, GALLERY)
    }


    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data!!.data
                val selectedVideoPath = getPath(contentURI)
                videoFile = File(selectedVideoPath)
                thumbnailBitmap=ThumbnailUtils.createVideoThumbnail(videoFile?.path, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND)
                tvFileName.text = videoFile?.name
                Log.d("path", selectedVideoPath)
            }
        }
    }

    fun getPath(uri: Uri?): String? {
        val projection = arrayOf(MediaStore.Video.Media.DATA)
        val cursor = contentResolver.query(uri!!, projection, null, null, null)
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            val column_index = cursor!!
                .getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
            cursor!!.moveToFirst()
            return cursor!!.getString(column_index)
        } else
            return null
    }

    private fun uploadImage() {
        if (videoFile != null) {
            val progressDialog = ProgressDialog(this)
            progressDialog.setTitle("Uploading...")
            progressDialog.show()

            val mediaRef =
                storageReference!!.child("medias/" + Utils.getCurrentTime("yyyy/MM/dd/ hh:mm:ss") + " ${videoFile?.name}")
            mediaRef.putFile(Uri.fromFile(videoFile))
                .addOnSuccessListener {
                    progressDialog.dismiss()
                    mediaRef.downloadUrl.addOnSuccessListener {
                        mediaUrl = it.toString()
                        if(mediaThumbnail!=null){
                            uploadMedia()
                        }
                    }
                    Toast.makeText(this, "Uploaded", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener { e ->
                    progressDialog.dismiss()
                    Toast.makeText(this, "Failed " + e.message, Toast.LENGTH_SHORT)
                        .show()
                }
                .addOnProgressListener { taskSnapshot ->
                    val progress =
                        100.0 * taskSnapshot.bytesTransferred / taskSnapshot
                            .totalByteCount
                    progressDialog.setMessage("Uploaded " + progress.toInt() + "%")
                }

            val baos = ByteArrayOutputStream()
            thumbnailBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, baos)

            val data = baos.toByteArray()

            var uploadTask =
            storageReference!!.child("thumbnails/" + Utils.getCurrentTime("yyyy/MM/dd/ hh:mm:ss") + " ${videoFile?.nameWithoutExtension}.jpg")
            uploadTask?.putBytes(data).addOnFailureListener {
                // Handle unsuccessful uploads
            }?.addOnSuccessListener {
                uploadTask.downloadUrl.addOnSuccessListener {
                    mediaThumbnail = it.toString()

                    if(mediaUrl!=null){
                        uploadMedia()
                    }
                }
            }
        }
    }

    private fun uploadMedia() {
        var media = Media("","",0,mediaUrl,edtDescription.text.toString(),true,Utils.getCurrentTime("yyyy/MM/dd hh:mm:ss"),"Football Lover",mediaThumbnail)
        var firestore = FirebaseFirestore.getInstance()
        firestore.collection("media")
            .document()
            .set(media)
            .addOnCompleteListener{
                if(it.isComplete || it.isSuccessful){
                    Toast.makeText(this,"Done!",Toast.LENGTH_SHORT).show()
                }
            }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                ) {
                    uploadImage()

                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
                return
            }
            else -> {
            }
        }
    }

    override fun onReloadData() {
        TODO("Not yet implemented")
    }

    private fun checkPersmission(): Boolean {
        return (ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this, arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ), PERMISSION_REQUEST_CODE
        )
    }
}