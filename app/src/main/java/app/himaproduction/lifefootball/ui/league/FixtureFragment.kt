package app.himaproduction.lifefootball.ui.league

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.Country
import app.himaproduction.lifefootball.data.entity.Fixture
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import kotlinx.android.synthetic.main.fragment_fixtures.*

class FixtureFragment : BaseFragment(), FixtureAdapter.OnFixtureClick {
    var fixtureAdapter: FixtureAdapter? = null

    override fun onCreateView() {
        rootView = R.layout.fragment_fixtures
    }

    override fun onViewCreated() {
        setupView()
        setupViewModel()
    }

    override fun setupView() {
        fixtureAdapter = activity?.let {
            FixtureAdapter(
                it,
                arrayListOf()
            )
        }
        fixtureAdapter?.setListener(this)
        recyclerview_fixtures.apply {
            adapter = fixtureAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        }
    }

    override fun setupViewModel() {
        setupObserverLive(mainViewModel.getSomeNextFixturesFromLeagueLiveData(), "")
    }

    override fun setupEventListener() {
        TODO("Not yet implemented")
    }

    override fun onLoadData() {
        fetchNextFixturesByLeague()
    }

    private fun fetchNextFixturesByLeague() {
        mainViewModel.fetchSomeNextFixtureFromLeague(DataHelper.selectedLeague!!.league_id, 20)
    }

    override fun setupExposeData(data: Any, tag: String) {
        hideLoading()
        fixtureAdapter?.updateData(data as ArrayList<Fixture>)
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
        hideLoading()
        showError(errorMessage!!, object : OnRetry {
            override fun onRetry() {
                onLoadData()
            }
        })
    }

    override fun setupExposeLoading(tag: String) {
        showLoadingDialog(Utils.loadingMsg)
    }

    override fun onFixtureClick(fixture: Fixture) {
        DataHelper.fixtureForDetail = fixture
        startActivity(Intent(activity,FixtureDetailActivity::class.java))
    }

    override fun onTeamClick(team: Int) {

    }
}