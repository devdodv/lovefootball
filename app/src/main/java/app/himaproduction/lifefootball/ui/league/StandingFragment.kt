package app.himaproduction.lifefootball.ui.league

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.Standing
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import kotlinx.android.synthetic.main.fragment_league_standing.*

class StandingFragment : BaseFragment(), StandingAdapter.OnItemClick {
    var standingAdapter: StandingAdapter? = null;

    override fun onCreateView() {
        rootView = R.layout.fragment_league_standing
    }

    override fun onViewCreated() {
        setupView()
        setupViewModel()
        setupEventListener()
    }

    override fun setupView() {
        standingAdapter = activity?.let {
            StandingAdapter(
                it
            )
        }
        recycler_view_standing.apply {
            adapter = standingAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        }
    }

    override fun setupViewModel() {
        setupObserverLive(mainViewModel.getStandingFromLeagueLiveData(), "")
    }

    override fun setupEventListener() {
        standingAdapter?.setCallback(this)
    }

    override fun onLoadData() {
        fetchStanding()
    }

    private fun fetchStanding() {
        mainViewModel.fetchStandingFromLeague(DataHelper.selectedLeague?.league_id!!)
    }

    override fun setupExposeData(data: Any, tag: String) {
        standingAdapter!!.setData(data as ArrayList<Any>)
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
        errorMessage?.let {
            showError(it, object : OnRetry {
                override fun onRetry() {
                    onLoadData()
                }
            })
        }
    }

    override fun setupExposeLoading(tag: String) {
        showLoadingDialog(Utils.loadingMsg)
    }

    override fun onItemClick(teamId: Int) {

    }
}