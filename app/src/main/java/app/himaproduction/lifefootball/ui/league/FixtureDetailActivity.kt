package app.himaproduction.lifefootball.ui.league

import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import app.himaproduction.lifefootball.App
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.ui.ViewPagerAdapter
import app.himaproduction.lifefootball.ui.base.BaseActivity
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_fixture_detail.*
import kotlinx.android.synthetic.main.activity_fixture_detail.tvStatus
import kotlinx.android.synthetic.main.activity_fixture_detail.tvTeamA
import kotlinx.android.synthetic.main.activity_fixture_detail.tvTeamB
import kotlinx.android.synthetic.main.tab_layout.*

class FixtureDetailActivity: BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fixture_detail)
        setupView()
        setupViewPager(view_pager)
    }

    override fun setupView() {
        val fixture= DataHelper.fixtureForDetail
        val league= DataHelper.selectedLeague
        setupToolbar(league?.logo,league?.name)
        tvTeamA.text = fixture?.homeTeam?.team_name
        tvTeamB.text = fixture?.awayTeam?.team_name
        tvDatatime.text = Utils.convertTimestampToDate(fixture?.eventTimestamp!!).replace("\n"," ")
        Glide.with(this).load(fixture.homeTeam?.logo).into(imvLogoTeamA)
        Glide.with(this).load(fixture.awayTeam?.logo).into(imvLogoTeamB)
        if(fixture?.goalsHomeTeam == null){
            tvVs.text = "vs"
            tvStatus.text = Utils.parseFullStatus(fixture?.statusShort!!)
            tvStatus.visibility = View.VISIBLE
        }else {
            tvTeamAScore.text = fixture?.goalsHomeTeam.toString()
            tvTeamBScore.text = fixture?.goalsAwayTeam.toString()
        }
    }

    override fun onReloadData() {
        TODO("Not yet implemented")
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter =
            ViewPagerAdapter(
                supportFragmentManager
            )
        adapter.addFragment(EventFragment(), App.self().resources.getString(R.string.lb_event))
        adapter.addFragment(StatisticFragment(), App.self().resources.getString(R.string.lb_statistic))
        viewPager.offscreenPageLimit = 2
        viewPager.adapter = adapter
        tab_layout.setupWithViewPager(view_pager)
    }
}