package app.himaproduction.lifefootball.ui.bottomsheet

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.League
import android.content.Context
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.MediaComment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.bumptech.glide.Glide


class CommentAdapter(
    private val context: Context,
    private var comments: ArrayList<MediaComment>
) :
    RecyclerView.Adapter<CommentAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var listener: OnItemClick? = null

    init {
        mContext = context
    }

    class ViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_comment, parent, false)
        ) {
        private var tvUser: TextView
        private var tvCreateTime: TextView
        private var tvComment: TextView

        init {
            tvUser = itemView.findViewById(R.id.tvUser)
            tvCreateTime = itemView.findViewById(R.id.tvTimeComment)
            tvComment = itemView.findViewById(R.id.tvComment)
        }

        fun bind(comment:MediaComment) {
            tvUser.text = comment.username
            tvCreateTime.text = comment.createTime
            tvComment.text = comment.comment
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            inflater,
            parent
        )
    }

    override fun getItemCount(): Int {
        return comments.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(comments[position])
    }

    fun setListener(callback: OnItemClick) {
        this.listener = callback
    }

    fun update(mediaComment: MediaComment) {
        this.comments.add(0,mediaComment)
        notifyItemInserted(0)
    }

    fun setData(comments: List<MediaComment>) {
        this.comments.addAll(comments)
        notifyDataSetChanged()
    }

    interface OnItemClick {
        fun onCountryClick(league: League)
        fun onFollow(league: League)
        fun onUnfollow(league: League)
    }
}