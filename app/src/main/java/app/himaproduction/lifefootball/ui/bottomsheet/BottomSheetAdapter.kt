package app.himaproduction.lifefootball.ui.bottomsheet

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.League
import android.content.Context
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import app.himaproduction.lifefootball.viewmodel.LeagueFollowViewModel
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.bumptech.glide.Glide
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class BottomSheetAdapter(
    private val context: Context,
    private val leagues: ArrayList<League> = arrayListOf()
) :
    RecyclerView.Adapter<BottomSheetAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var listener: OnItemClick? = null
    private var followingViewModel: LeagueFollowViewModel? = null

    init {
        mContext = context
    }

    class ViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_league, parent, false)
        ) {
        private var tvLeague: TextView? = null
        private var layoutItem: ConstraintLayout? = null
        private var imvLeagueLogo: ImageView? = null
        private var btnFollow: TextView? = null

        init {
            imvLeagueLogo = itemView.findViewById(R.id.imv_league_logo)
            tvLeague = itemView.findViewById(R.id.tv_league_name)
            layoutItem = itemView.findViewById(R.id.layout_item)
            btnFollow = itemView.findViewById(R.id.btn_add_to_league_favorite)
        }

        fun bind(
            league: League,
            context: Context,
            listener: OnItemClick?
        ) {
            tvLeague?.text = league.name
            Glide.with(context)
                .load(league.logo)
                .into(imvLeagueLogo!!)
            layoutItem?.setOnClickListener { listener?.onCountryClick(league) }
            btnFollow!!.post {
                checkFollowedLeague(league)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .doOnSuccess {
                        if (it) {
                            btnFollow?.setBackground(context.resources.getDrawable(R.drawable.background_following))
                            btnFollow?.setTextColor(context.resources.getColor(R.color.windowBackground))
                            btnFollow?.text = context.resources.getString(R.string.lb_unfollow)
                            btnFollow?.setOnClickListener { listener?.onUnfollow(league) }
                        } else {
                            btnFollow?.setBackground(context.resources.getDrawable(R.drawable.background_follow))
                            btnFollow?.setTextColor(context.resources.getColor(R.color.blue))
                            btnFollow?.text = context.resources.getString(R.string.lb_follow)
                            btnFollow?.setOnClickListener { listener?.onFollow(league) }
                        }
                    }
                    .subscribe()
            }

        }

        private fun checkFollowedLeague(league: League): Single<Boolean> {
            return Single.create {
                if (DataHelper.followingLeagues == null) {
                    it.onSuccess(false)
                } else {
                    for (league1 in DataHelper.followingLeagues!!) {
                        if (league.league_id == league1.league_id) {
                            it.onSuccess(true)
                            break
                        }
                    }
                    it.onSuccess(false)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            inflater,
            parent
        )
    }

    override fun getItemCount(): Int {
        return leagues.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(leagues[position], context, listener)
    }

    fun updateData(list: ArrayList<League>, clearOldData: Boolean = true) {
        if (clearOldData)
            leagues.clear()
        leagues.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(callback: OnItemClick) {
        this.listener = callback
    }

    fun addNewFollow(league: League) {
        notifyDataSetChanged()
    }

    interface OnItemClick {
        fun onCountryClick(league: League)
        fun onFollow(league: League)
        fun onUnfollow(league: League)
    }
}