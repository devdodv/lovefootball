package app.himaproduction.lifefootball.ui.bottomsheet

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import app.himaproduction.lifefootball.data.entity.Country
import app.himaproduction.lifefootball.data.entity.League
import app.himaproduction.lifefootball.ui.competition.CompetitionDetailsActivity
import app.himaproduction.lifefootball.ui.state.DataState
import app.himaproduction.lifefootball.viewmodel.LeagueFollowViewModel
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_bottom_sheet.*
import org.koin.android.viewmodel.ext.android.viewModel

class BottomSheetFragment : BottomSheetDialogFragment(), BottomSheetAdapter.OnItemClick {
    private lateinit var country: String
    private lateinit var madapter: BottomSheetAdapter
    val followingViewModel: LeagueFollowViewModel by viewModel()
    lateinit var leagueFollow: League
    lateinit var unLeagueFollow: League
    var bottomSheetBehavior: BottomSheetBehavior<*>? = null

    companion object {
        fun newInstance(country: Country) = BottomSheetFragment().apply {
            arguments = Bundle().apply {
                putString("name", country.country)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null) {
            country = arguments?.getString("name")!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? { // Inflate
        return inflater.inflate(
            app.himaproduction.lifefootball.R.layout.fragment_bottom_sheet,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        btnClose.setOnClickListener { dismiss() }

        followingViewModel.addFollowingLeagueLiveData().observe(viewLifecycleOwner, Observer {
            when(it){
                is DataState.Success -> {
                    madapter.addNewFollow(it.data as League)
                }
            }
        })
        followingViewModel.unFollowLeagueLiveData().observe(viewLifecycleOwner, Observer {
//            if (::unLeagueFollow.isInitialized)
//                madapter.notifyItemChanged(madapter.positonOfFollowLeague(unLeagueFollow))
//            else
//                madapter.notifyDataSetChanged()
        })
    }

    private fun initRecyclerView() {
        tv_country_name.text = country
        madapter = BottomSheetAdapter(activity!!, arrayListOf())
        madapter?.setListener(this)
        recycler_view_league_options.apply {
            adapter = madapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        }
        madapter?.updateData(DataHelper.leaguesByCountry!!)
    }

    override fun onCountryClick(league: League) {
        DataHelper.selectedLeague = league
        val intent = Intent(context, CompetitionDetailsActivity::class.java)
        activity?.startActivity(intent)
    }

    override fun onFollow(league: League) {
        leagueFollow = league
        followingViewModel.addFollowingLeague(league)
    }

    override fun onUnfollow(league: League) {
        unLeagueFollow = league
        followingViewModel.unFollowLeague(league)
    }

}