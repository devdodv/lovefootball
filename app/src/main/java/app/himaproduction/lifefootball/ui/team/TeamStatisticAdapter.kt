package app.himaproduction.lifefootball.ui.team

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.Team
import app.himaproduction.lifefootball.data.entity.TeamStatistic


class TeamStatisticAdapter(
    private val context: Context,
    private val statistics:ArrayList<Any>
) :
    RecyclerView.Adapter<TeamStatisticAdapter.CountriesVH>() {
    private var mContext: Context? = null
    private var listener: OnTeamClick? = null

    init {
        mContext = context
    }

    class CountriesVH(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_team_statistic, parent, false)
        ) {
        private var tvTeamTitle: TextView
        private var tvTotal: TextView
        private var tvHome: TextView
        private var tvAway: TextView

        init {
            tvTeamTitle = itemView.findViewById(R.id.tvTitle)
            tvTotal = itemView.findViewById(R.id.tvTotal)
            tvHome = itemView.findViewById(R.id.tvHome)
            tvAway = itemView.findViewById(R.id.tvAway)
        }

        fun bind(
            stat: Any,
            context: Context,
            listener: OnTeamClick?
        ) {
            when(stat){
                is String-> {
                    tvTeamTitle.text = ""
                    tvTotal.text = context.resources.getString(R.string.lb_total)
                    tvHome.text = context.resources.getString(R.string.lb_home)
                    tvAway.text = context.resources.getString(R.string.lb_away)
                }
                is TeamStatistic.MatchsPlayed->{
                    tvTeamTitle.text = "Matchs Played"
                    tvTotal.text = stat.total.toString()
                    tvHome.text = stat.home.toString()
                    tvAway.text = stat.away.toString()
                }
                is TeamStatistic.Wins->{
                    tvTeamTitle.text = "Win"
                    tvTotal.text = stat.total.toString()
                    tvHome.text = stat.home.toString()
                    tvAway.text = stat.away.toString()
                }
                is TeamStatistic.Draws->{
                    tvTeamTitle.text = "Draw"
                    tvTotal.text = stat.total.toString()
                    tvHome.text = stat.home.toString()
                    tvAway.text = stat.away.toString()
                }
                is TeamStatistic.Loses->{
                    tvTeamTitle.text = "Lose"
                    tvTotal.text = stat.total.toString()
                    tvHome.text = stat.home.toString()
                    tvAway.text = stat.away.toString()
                }
                is TeamStatistic.GoalsFor->{
                    tvTeamTitle.text = "Goal For"
                    tvTotal.text = stat.total.toString()
                    tvHome.text = stat.home.toString()
                    tvAway.text = stat.away.toString()
                }
                is TeamStatistic.GoalsAgainst->{
                    tvTeamTitle.text = "Goal Against"
                    tvTotal.text = stat.total.toString()
                    tvHome.text = stat.home.toString()
                    tvAway.text = stat.away.toString()
                }
                is TeamStatistic.GoalsForAvg->{
                    tvTeamTitle.text = "Goal for average"
                    tvTotal.text = stat?.total.toString()
                    tvHome.text = stat?.home.toString()
                    tvAway.text = stat?.away.toString()
                }
                is TeamStatistic.GoalsAgainstAvg->{
                    tvTeamTitle.text = "Goal against average"
                    tvTotal.text = stat?.total.toString()
                    tvHome.text = stat?.home.toString()
                    tvAway.text = stat?.away.toString()
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountriesVH {
        val inflater = LayoutInflater.from(parent.context)
        return CountriesVH(
            inflater,
            parent
        )
    }

    override fun getItemCount(): Int {
        return statistics.size
    }

    override fun onBindViewHolder(holder: CountriesVH, position: Int) {
        holder.bind(statistics[position], context, listener)
    }

    fun updateData(list: ArrayList<Any>, clearOldData: Boolean = true) {
        if (clearOldData)
            statistics.clear()
        list.add(0,"")
        statistics.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(callback: OnTeamClick) {
        this.listener = callback
    }

    interface OnTeamClick {
        fun onTeamClick(team: Team)
    }
}
