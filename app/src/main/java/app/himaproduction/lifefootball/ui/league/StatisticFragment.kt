package app.himaproduction.lifefootball.ui.league

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.BaseHomeAway
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import kotlinx.android.synthetic.main.fragment_statistic_from_fixture.*

class StatisticFragment : BaseFragment() {
    private lateinit var mAdapter: StatisticAdapter

    override fun onCreateView() {
        rootView = R.layout.fragment_statistic_from_fixture
    }

    override fun setupViewModel() {
        setupObserverLive(mainViewModel.getStatisticFromFixtureLiveData(), "")
    }

    override fun setupEventListener() {
        TODO("Not yet implemented")
    }

    override fun setupView() {
        mAdapter = context?.let { StatisticAdapter(it, arrayListOf()) }!!
        recycler_view_statistic.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(context)
//            addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        }
    }

    override fun setupExposeData(data: Any, tag: String) {
        mAdapter.updateData(data as ArrayList<BaseHomeAway>)
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
        showError(errorMessage,object :OnRetry{
            override fun onRetry() {
                onLoadData()
            }
        })
    }

    override fun setupExposeLoading(tag: String) {
        showLoadingDialog(Utils.loadingMsg)
    }

    override fun onLoadData() {
        mainViewModel.fetchStatisticFromFixture(DataHelper.fixtureForDetail?.fixtureId!!)
    }

    override fun onViewCreated() {
        setupView()
        setupViewModel()
    }
}