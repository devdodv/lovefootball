package app.himaproduction.lifefootball.ui.competition

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.League
import android.content.Context
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.bumptech.glide.Glide
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.logging.Handler


class FollowingLeagueAdapter(
    private val context: Context,
    private val leagues: ArrayList<League> = arrayListOf()
) :
    RecyclerView.Adapter<FollowingLeagueAdapter.ViewHolder>() {
    private var mContext: Context? = null
    private var listener: OnItemClick? = null

    init {
        mContext = context
    }

    class ViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_following_league, parent, false)
        ) {
        private var tvLeague: TextView? = null
        private var tvCountry: TextView? = null
        private var layoutItem: ConstraintLayout? = null
        private var imvLeagueLogo: ImageView? = null
        private var btnFollow: TextView? = null

        init {
            imvLeagueLogo = itemView.findViewById(R.id.imv_league_logo)
            tvLeague = itemView.findViewById(R.id.tv_league_name)
            tvCountry = itemView.findViewById(R.id.tvCountry)
            layoutItem = itemView.findViewById(R.id.layout_item)
            btnFollow = itemView.findViewById(R.id.btn_add_to_league_favorite)
        }

        fun bind(
            position: Int,
            league: League,
            context: Context,
            listener: OnItemClick?
        ) {
            tvCountry?.text = league.country
            tvLeague?.text = league.name
            Glide.with(context)
                .load(league.logo)
                .placeholder(R.drawable.icon_football)
                .error(R.drawable.icon_football)
                .into(imvLeagueLogo!!)
            layoutItem?.setOnClickListener { listener?.onLeagueClick(league) }
            btnFollow?.setOnClickListener { listener?.onUnfollow(position, league) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            inflater,
            parent
        )
    }

    override fun getItemCount(): Int {
        return leagues.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position, leagues[position], context, listener)
    }

    fun updateData(list: List<League>, clearOldData: Boolean = true) {
        if (clearOldData)
            leagues.clear()
        leagues.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(callback: OnItemClick) {
        this.listener = callback
    }

    fun newFollow(league: League) {
        leagues.add(league)
        notifyItemInserted(leagues.size - 1)
    }

    fun unFollow(league: League) {
        refreshListFlowable(league).observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnNext {
                leagues.removeAt(it)
                if (leagues.size > 0)
                    notifyItemRangeRemoved(it, leagues.size)
                else
                    notifyItemRemoved(it)
            }
            .subscribe()

    }

    fun refreshListFlowable(league: League): Flowable<Int> {
        return Flowable.create({
            if (leagues.size == 0)
                it.onComplete()
            else {
                for (i in 0..leagues.size - 1) {
                    if (leagues[i].league_id == league.league_id) {
                        it.onNext(i)
                        break
                    }
                }
            }
        }, BackpressureStrategy.LATEST)
    }

    interface OnItemClick {
        fun onLeagueClick(league: League)
        fun onUnfollow(pos: Int, league: League)
        fun onUnfollowAll()
    }
}