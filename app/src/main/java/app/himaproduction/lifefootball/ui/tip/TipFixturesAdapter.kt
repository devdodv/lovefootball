package app.himaproduction.lifefootball.ui.tip

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.Fixture
import app.himaproduction.lifefootball.data.entity.PredictionFixture
import com.bumptech.glide.Glide


class TipFixturesAdapter(
    private val context: Context,
    private val fixtures: ArrayList<PredictionFixture> = arrayListOf()
) :
    RecyclerView.Adapter<TipFixturesAdapter.CountriesVH>() {
    private var mContext: Context? = null
    private var listener: OnFixtureClick? = null

    init {
        mContext = context
    }

    class CountriesVH(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_prediction_fixture, parent, false)
        ) {
        private var tvTeamA: TextView
        private var tvTeamB: TextView
        private var tvLast5MatchesTeamA: TextView
        private var tvLast5MatchesTeamB: TextView
        private var layoutItem:ConstraintLayout

        init {
            tvTeamA = itemView.findViewById(R.id.tvTeamHome)
            tvTeamB = itemView.findViewById(R.id.tvTeamAway)
            tvLast5MatchesTeamA = itemView.findViewById(R.id.tvLast5MatchesHome)
            tvLast5MatchesTeamB = itemView.findViewById(R.id.tvLast5MatchesAway)
            layoutItem = itemView.findViewById(R.id.layout_item)
        }

        fun bind(
            fixture: PredictionFixture,
            context: Context,
            listener: OnFixtureClick?
        ) {
            tvTeamA.text = fixture.teamHome
            tvTeamB.text = fixture.teamAway
            var last5HomeMatches = ""
            var last5AwayMatches = ""
            fixture.lastFiveMatchesHome.forEach {
                last5HomeMatches = last5HomeMatches+"$it  "
            }
            fixture.lastFiveMatchesAway.forEach {
                last5AwayMatches = last5AwayMatches+"$it "
            }
            tvTeamA.text = fixture.teamHome
            tvTeamA.text = fixture.teamHome
            tvLast5MatchesTeamA.text = last5HomeMatches
            tvLast5MatchesTeamB.text = last5AwayMatches
            layoutItem.setOnClickListener {
               listener?.onFixtureClick(fixture)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountriesVH {
        val inflater = LayoutInflater.from(parent.context)
        return CountriesVH(
            inflater,
            parent
        )
    }

    override fun getItemCount(): Int {
        return fixtures.size
    }

    override fun onBindViewHolder(holder: CountriesVH, position: Int) {
        holder.bind(fixtures[position], context, listener)
    }

    fun updateData(list: ArrayList<PredictionFixture>, clearOldData: Boolean = true) {
        if (clearOldData)
            fixtures.clear()
        fixtures.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(callback: OnFixtureClick) {
        this.listener = callback
    }

    interface OnFixtureClick{
        fun onFixtureClick(fixture: PredictionFixture)
    }
}
