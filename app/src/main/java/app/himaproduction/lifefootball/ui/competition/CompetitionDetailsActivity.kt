package app.himaproduction.lifefootball.ui.competition

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import app.himaproduction.lifefootball.App
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.ui.ViewPagerAdapter
import app.himaproduction.lifefootball.ui.base.BaseActivity
import app.himaproduction.lifefootball.ui.bottomsheet.BottomSheetFragment
import app.himaproduction.lifefootball.ui.league.FixtureFragment
import app.himaproduction.lifefootball.ui.league.StandingFragment
import app.himaproduction.lifefootball.ui.league.TopScoreFragment
import app.himaproduction.lifefootball.ui.team.TeamFragment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import kotlinx.android.synthetic.main.activity_competition_details.*
import kotlinx.android.synthetic.main.tab_layout.*


class CompetitionDetailsActivity : BaseActivity() {
    companion object {
        var competitionId: Long? = 0L
        var name: String? = ""
    }

    private var bottomSheetFragment: BottomSheetFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_competition_details)
        var league = DataHelper.selectedLeague
        setupToolbar(league?.logo,league?.name!!)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(false)
        setupViewPager(view_pager)
    }

    override fun setupView() {
        TODO("Not yet implemented")
    }

    override fun onReloadData() {
        TODO("Not yet implemented")
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter =
            ViewPagerAdapter(
                supportFragmentManager
            )
        adapter.addFragment(FixtureFragment(), App.self().resources.getString(R.string.lb_schedule))
        adapter.addFragment(StandingFragment(), App.self().resources.getString(R.string.lb_standing))
        adapter.addFragment(TopScoreFragment(), App.self().resources.getString(R.string.lb_top_score))
        adapter.addFragment(TeamFragment(), App.self().resources.getString(R.string.lb_team))
        viewPager.offscreenPageLimit = 4
        viewPager.adapter = adapter
        tab_layout.setupWithViewPager(view_pager)
    }

}
