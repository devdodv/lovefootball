package app.himaproduction.lifefootball.ui.player

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.Player
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import kotlinx.android.synthetic.main.fragment_players.*

class PlayersFragment : BaseFragment() {
    lateinit var playerAdapter: PlayerAdapter

    override fun onCreateView() {
        rootView = R.layout.fragment_players
    }

    override fun setupViewModel() {
        setupObserverLive(mainViewModel.getPlayersLiveData(),"")
    }

    override fun setupEventListener() {
        TODO("Not yet implemented")
    }

    override fun setupView() {
        playerAdapter = PlayerAdapter(context!!, arrayListOf())
        recyclerview_players.apply {
            adapter = playerAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        }
    }

    override fun setupExposeData(data: Any, tag: String) {
        playerAdapter.updateData(data as ArrayList<Player>)
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
        showError(errorMessage,object :OnRetry{
            override fun onRetry() {
                onLoadData()
            }
        })
    }

    override fun setupExposeLoading(tag: String) {
    }

    override fun onLoadData() {
        mainViewModel.fetchPlayersFromTeam(DataHelper.selectedTeam?.teamId!!)
    }

    override fun onViewCreated() {
        setupView()
        setupViewModel()
    }
}