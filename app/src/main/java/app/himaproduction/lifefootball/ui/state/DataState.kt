package app.himaproduction.lifefootball.ui.state

import org.buffer.android.boilerplate.ui.model.ResourceState


sealed class DataState(val resourceState: ResourceState,
                       val data: Any? = null,
                       val errorMessage: String? = null) {

    data class Success(private val result: Any): DataState(ResourceState.SUCCESS,
        result)

    data class Error(private val message: String? = null): DataState(ResourceState.SUCCESS,
            errorMessage = message)

    object Loading: DataState(ResourceState.LOADING)
}