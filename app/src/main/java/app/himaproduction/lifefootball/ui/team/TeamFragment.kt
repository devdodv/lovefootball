package app.himaproduction.lifefootball.ui.team

import android.content.Intent
import androidx.recyclerview.widget.GridLayoutManager
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.Team
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.himaproduction.lifefootball.ui.custom.Space
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import kotlinx.android.synthetic.main.fragment_team.*

class TeamFragment : BaseFragment(),
    TeamAdapter.OnTeamClick {
    lateinit var teamAdapter: TeamAdapter

    override fun onCreateView() {
        rootView = R.layout.fragment_team
    }

    override fun setupViewModel() {
        setupObserverLive(mainViewModel.getTeamsLiveData(), "")
    }

    override fun setupEventListener() {
    }

    override fun setupView() {
        teamAdapter = TeamAdapter(
            context!!,
            arrayListOf()
        )
        teamAdapter.setListener(this)
        recyclerview_teams.apply {
            adapter = teamAdapter
            layoutManager = GridLayoutManager(context, 3)
            addItemDecoration(Space(3, 20, true, 0))
        }

    }

    override fun setupExposeData(data: Any, tag: String) {
        teamAdapter.updateData(data as ArrayList<Team>)
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
        showError(errorMessage, object : OnRetry {
            override fun onRetry() {
                onLoadData()
            }
        })
    }

    override fun setupExposeLoading(tag: String) {
    }

    override fun onLoadData() {
        if (mainViewModel != null && DataHelper.selectedLeague != null)
            mainViewModel.fetchTeamsFromLeague(DataHelper.selectedLeague?.league_id!!)
    }

    override fun onViewCreated() {
        setupView()
        setupViewModel()
        setupEventListener()
    }

    override fun onTeamClick(team: Team) {
        DataHelper.selectedTeam = team
        startActivity(
            Intent(
                activity,
                TeamDetailActivity::class.java
            )
        )
    }
}