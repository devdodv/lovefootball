package app.himaproduction.lifefootball.ui.media

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.api.response.MediaResponse
import app.himaproduction.lifefootball.data.entity.Media
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.himaproduction.lifefootball.ui.bottomsheet.AddnewActivity
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.firebase.firestore.DocumentSnapshot
import kotlinx.android.synthetic.main.fragment_medias.*


class MediaFragment : BaseFragment(), MediaAdapter.OnMediaClick {
    var mediaAdapter: MediaAdapter? = null
    var isLoading = false
    var lastSnapshot: DocumentSnapshot? = null
    private lateinit var simpleExoPlayer: SimpleExoPlayer
    private lateinit var mediaDataSourceFactory: DataSource.Factory


    override fun setupViewModel() {
        setupObserverLive(mainViewModel.getMediasLiveData(), "")
    }

    override fun setupEventListener() {
    }

    override fun setupView() {
        mediaAdapter = activity?.let {
            MediaAdapter(
                it,
                arrayListOf()
            )
        }
        mediaAdapter?.setListener(this)
        recycler_view_medias.apply {
            adapter = mediaAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
            setHasFixedSize(true)
            setItemViewCacheSize(20)
        }
        floatingAddMedia.setOnClickListener {
            startActivity(Intent(activity, AddnewActivity::class.java))
        }
        initScrollListener(object : MediaAdapter.OnLoadMoreClick {
            override fun onLoadmore() {
                onLoadData()
            }
        })
    }

    override fun setupExposeData(data: Any, tag: String) {
        isLoading = false
        val mediaResponse: MediaResponse = data as MediaResponse

        if (lastSnapshot == null)
            mediaAdapter?.setData(mediaResponse.medias, false)
        else
            mediaAdapter?.updateDataAfterLoadMore(mediaResponse.medias)
        lastSnapshot = mediaResponse.lastSapshot
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
        isLoading = false
        if (lastSnapshot == null)
            showError(errorMessage, object : OnRetry {
                override fun onRetry() {
                    onLoadData()
                }
            }) else {
            Toast.makeText(activity,"Opps something is wrong!",Toast.LENGTH_SHORT).show()
            mediaAdapter?.loadMoreFail()
        }
    }

    override fun setupExposeLoading(tag: String) {
        isLoading = true
        if (lastSnapshot == null)
            showLoadingDialog(Utils.loadingMsg)
    }

    override fun onLoadData() {
        mainViewModel.fetchMedias(lastSnapshot)
    }

    override fun onViewCreated() {
        setupView()
        setupViewModel()
    }

    override fun onCreateView() {
        rootView = R.layout.fragment_medias
    }


    override fun onMediaClick(media: Media) {
        DataHelper.watchMediaObj = media
        startActivity(Intent(activity, MediaDetailActivity::class.java))
    }

    override fun onPlayClick(playerView: PlayerView,media: Media,progressBar: ProgressBar) {
        playVideo(playerView,media.mediaUrl!!,progressBar)
    }

    private fun initScrollListener(param: MediaAdapter.OnLoadMoreClick) {
        recycler_view_medias.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(
                @NonNull recyclerView: RecyclerView,
                newState: Int
            ) {
                super.onScrollStateChanged(recyclerView, newState)
                val linearLayoutManager =
                    recyclerView.layoutManager as LinearLayoutManager?
                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    mediaAdapter?.setLiveData(true)
                }else{
                    mediaAdapter?.setLiveData(false)
                }
            }

            override fun onScrolled(
                @NonNull recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager =
                    recyclerView.layoutManager as LinearLayoutManager?
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == mediaAdapter!!.itemCount - 2) {
                        //bottom of list!
                        loadMore(param)
                        isLoading = true
                    }
                }
            }
        })
    }

    private fun loadMore(param: MediaAdapter.OnLoadMoreClick) {
        recycler_view_medias.post(Runnable {
            mediaAdapter?.addLoadmore(param)
        })
    }
    fun playVideo(playerView: PlayerView,url:String,progressBar: ProgressBar){

        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(context)
        mediaDataSourceFactory =
            DefaultDataSourceFactory(context, Util.getUserAgent(context, "mediaPlayerSample"))

        val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory).createMediaSource(
            Uri.parse(url)
        )

        simpleExoPlayer.prepare(mediaSource, false, false)
        simpleExoPlayer.playWhenReady = true

        playerView.setShutterBackgroundColor(Color.TRANSPARENT)
        playerView.player = simpleExoPlayer
        playerView.requestFocus()

        simpleExoPlayer.addListener(object : Player.EventListener {
            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {

            }

            override fun onTracksChanged(
                trackGroups: TrackGroupArray?,
                trackSelections: TrackSelectionArray?
            ) {

            }

            override fun onPlayerError(error: ExoPlaybackException?) {

            }

            /** 4 playbackState exists */
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    Player.STATE_BUFFERING -> {
                        progressBar.visibility = View.VISIBLE
                    }
                    Player.STATE_READY -> {
                        progressBar.visibility = View.INVISIBLE
                    }
                    Player.STATE_IDLE -> {
                    }
                    Player.STATE_ENDED -> {
                        progressBar.visibility = View.INVISIBLE
                    }
                }
            }

            override fun onLoadingChanged(isLoading: Boolean) {

            }

            override fun onPositionDiscontinuity(reason: Int) {

            }

            override fun onRepeatModeChanged(repeatMode: Int) {
            }

            override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {
            }
        })

    }

    private fun releasePlayer() {
//        if(::simpleExoPlayer.isInitialized)
//            simpleExoPlayer.release()
        if(::simpleExoPlayer.isInitialized){
            simpleExoPlayer.playWhenReady = false
        }
    }
    private fun resumePlayer() {
        if(::simpleExoPlayer.isInitialized){
            simpleExoPlayer.playWhenReady = false
        }
    }

    private fun pausePlayer() {
        if(::simpleExoPlayer.isInitialized) {
            simpleExoPlayer.setPlayWhenReady(false)

            simpleExoPlayer.getPlaybackState()
        }
    }

    private fun startPlayer() {
        if(::simpleExoPlayer.isInitialized) {
            simpleExoPlayer.setPlayWhenReady(true)
            simpleExoPlayer.seekTo(5)
            simpleExoPlayer.getPlaybackState()
        }
    }

    public override fun onPause() {
        super.onPause()

        if (Util.SDK_INT <= 23) pausePlayer()
    }

    public override fun onStop() {
        super.onStop()

        if (Util.SDK_INT > 23) pausePlayer()
    }
//
//    public override fun onStart() {
//        super.onStart()
//
//        if (Util.SDK_INT > 23) initializePlayer()
//    }
//
    public override fun onResume() {
        super.onResume()

        if (Util.SDK_INT <= 23) startPlayer()
    }
}