package app.himaproduction.lifefootball.ui.league

import androidx.recyclerview.widget.LinearLayoutManager
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.entity.Event
import app.himaproduction.lifefootball.ui.base.BaseFragment
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import kotlinx.android.synthetic.main.fragment_event.*

class EventFragment : BaseFragment() {
    lateinit var eventAdapter: EventAdapter

    override fun onCreateView() {
        rootView = R.layout.fragment_event
    }

    override fun setupViewModel() {
        setupObserverLive(mainViewModel.getEventsFromFixtureLiveData(),"")
    }

    override fun setupEventListener() {
    }

    override fun setupView() {
        eventAdapter = context?.let { EventAdapter(it, arrayListOf()) }!!
        recycler_view_events.apply {
            adapter = eventAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun setupExposeData(data: Any, tag: String) {
        eventAdapter.updateData(data as List<Event>)
    }

    override fun setupExposeError(errorMessage: String, tag: String) {
        showError(errorMessage,null)
    }

    override fun setupExposeLoading(tag: String) {
        showLoadingDialog(Utils.loadingMsg)
    }

    override fun onLoadData() {
        mainViewModel.fetchEventsFromFixtre(DataHelper.fixtureForDetail?.fixtureId!!)
    }

    override fun onViewCreated() {
        setupView()
        setupViewModel()
    }
}