package app.himaproduction.lifefootball.ui.league

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.Const
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.Event
import app.himaproduction.lifefootball.data.entity.Fixture
import app.himaproduction.lifefootball.ui.custom.RoundedView
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.bumptech.glide.Glide


class EventAdapter(
    private val context: Context,
    private val events: ArrayList<Event> = arrayListOf()
) :
    RecyclerView.Adapter<EventAdapter.VH>() {
    private var mContext: Context? = null
    private var listener: OnFixtureClick? = null

    init {
        mContext = context
    }

    class VH(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_event, parent, false)
        ) {
        private var tvElaspse: RoundedView? = null
        private var tvPlayerHomeA: TextView? = null
        private var tvPlayerHomeB: TextView? = null
        private var tvPlayerAwayA: TextView? = null
        private var tvPlayerAwayB: TextView? = null
        private var tvTeamB: TextView? = null
        private var tvScoreTeamB: TextView? = null
        private var tvStatus: TextView? = null
        private var imvEventHome: ImageView? = null
        private var imvEventAway: ImageView? = null
        private var layoutItem: ConstraintLayout? = null
        private var teamHome = DataHelper.fixtureForDetail?.homeTeam?.team_name
        private var teamAway = DataHelper.fixtureForDetail?.awayTeam?.team_name

        init {
            tvElaspse = itemView.findViewById(R.id.tvElapse)
            tvPlayerHomeA = itemView.findViewById(R.id.tvPlayerHomeA)
            tvPlayerHomeB = itemView.findViewById(R.id.tvPlayerHomeB)
            tvPlayerAwayA = itemView.findViewById(R.id.tvPlayerAwayA)
            tvPlayerAwayB = itemView.findViewById(R.id.tvPlayerAwayB)
            tvTeamB = itemView.findViewById(R.id.tvTeamB)
            imvEventHome = itemView.findViewById(R.id.imvEventTypeHome)
            imvEventAway = itemView.findViewById(R.id.imvEventTypeAway)
        }

        fun bind(
            event: Event,
            context: Context,
            listener: OnFixtureClick?
        ) {
            tvElaspse?.setText(event.elapsed.toString() + "'")
            when (event.type) {
                Const.SUBST -> {
                    when (event.teamName) {
                        teamHome -> {
                            Glide.with(context).load(R.drawable.icon_subst).into(imvEventHome!!)
                            imvEventHome!!.visibility = View.VISIBLE
                            tvPlayerHomeA?.text = event.player
                            tvPlayerHomeB?.text = event.detail
                        }
                        teamAway -> {
                            Glide.with(context).load(R.drawable.icon_subst).into(imvEventAway!!)
                            imvEventAway!!.visibility = View.VISIBLE
                            tvPlayerAwayA?.text = event.player
                            tvPlayerAwayB?.text = event.detail
                        }
                    }
                }
                Const.GOAL -> {
                    when (event.teamName) {
                        teamHome -> {
                            Glide.with(context).load(R.drawable.icon_football).into(imvEventHome!!)
                            imvEventHome!!.visibility = View.VISIBLE
                            tvPlayerHomeA?.text = event.player
                            if ("Penalty".contentEquals(event.detail.toString())) {
                                tvPlayerHomeB?.text = "(${event.detail})"
                            } else
                                tvPlayerHomeB?.visibility = View.GONE
                        }
                        teamAway -> {
                            Glide.with(context).load(R.drawable.icon_football).into(imvEventAway!!)
                            imvEventAway!!.visibility = View.VISIBLE
                            tvPlayerAwayA?.text = event.player
                            if ("Penalty".contentEquals(event.detail.toString())) {
                                tvPlayerAwayB?.text = "(${event.detail})"
                            } else
                                tvPlayerAwayB?.visibility = View.GONE
                        }
                    }
                }
                Const.CARD -> {
                    when (event.teamName) {
                        teamHome -> {
                            Glide.with(context)
                                .load(if (event.detail?.contentEquals("Yellow Card")!!) R.drawable.icon_yellow_card else R.drawable.icon_red_card)
                                .into(imvEventHome!!)
                            imvEventHome!!.visibility = View.VISIBLE
                            tvPlayerHomeA?.text = event.player
                            tvPlayerHomeB?.visibility = View.GONE
                        }
                        teamAway -> {
                            Glide.with(context)
                                .load(if (event.detail?.contentEquals("Yellow Card")!!) R.drawable.icon_yellow_card else R.drawable.icon_red_card)
                                .into(imvEventAway!!)
                            imvEventAway!!.visibility = View.VISIBLE
                            tvPlayerAwayA?.text = event.player
                            tvPlayerAwayB?.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventAdapter.VH {
        val inflater = LayoutInflater.from(parent.context)
        return EventAdapter.VH(
            inflater,
            parent
        )
    }

    override fun getItemCount(): Int {
        return events.size
    }

    override fun onBindViewHolder(holder: EventAdapter.VH, position: Int) {
        holder.bind(events[position], context, listener)
    }

    fun updateData(list: List<Event>, clearOldData: Boolean = true) {
        if (clearOldData)
            events.clear()
        events.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(callback: OnFixtureClick) {
        this.listener = callback
    }

    interface OnFixtureClick {
        fun onFixtureClick(fixture: Fixture)
        fun onTeamClick(team: Int)
    }
}
