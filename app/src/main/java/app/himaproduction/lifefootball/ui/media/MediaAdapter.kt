package app.himaproduction.lifefootball.ui.media

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.App
import app.himaproduction.lifefootball.data.entity.Media
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.ui.UnifiedNativeAdViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.gms.ads.formats.NativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView


class MediaAdapter(
    private val context: Context,
    private val medias: ArrayList<Any> = arrayListOf()
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mContext: Context? = null
    private var listener: OnMediaClick? = null
    private var loadmoreCallback: OnLoadMoreClick? = null
    val MEDIA_TYPE: Int = 0
    val LOADMORE_TYPE: Int = 1
    val ADS_TYPE = 2
    var isNoMore: Boolean = false
    var isLoadMoreFail: Boolean = false
    var currentPosition:Int = -1
    private var recyclerViewStateLiveData: MutableLiveData<Boolean> = MutableLiveData()

    init {
        mContext = context
    }

    class MediaVH(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_media, parent, false)
        ) {
        private var tvMediaDescription: TextView
        private var btnPlayMedia: ImageView
        private var progressMedia: ProgressBar
        private var mediaView: ImageView
        private var tvCreateTime: TextView
        private var tvUsername: TextView
        private var layoutMedia: ConstraintLayout
        private var imvUser: ImageView
        private var playerView: PlayerView
        private var progressVideo: ProgressBar
        private lateinit var simpleExoPlayer: SimpleExoPlayer
        private lateinit var mediaDataSourceFactory: DataSource.Factory
        private var context:Context? = null
        private var currentSeek:Long = 0
        init {
            tvCreateTime = itemView.findViewById(R.id.tvCreateDate)
            tvUsername = itemView.findViewById(R.id.tvUsername)
            layoutMedia = itemView.findViewById(R.id.layout_media)
            tvMediaDescription = itemView.findViewById(R.id.tvMediaDesciption)
            btnPlayMedia = itemView.findViewById(R.id.btnPlayMedia)
            progressMedia = itemView.findViewById(R.id.progressMedia)
            mediaView = itemView.findViewById(R.id.mediaView)
            imvUser = itemView.findViewById(R.id.imvUser)
            playerView = itemView.findViewById(R.id.playerView)
            progressVideo = itemView.findViewById(R.id.progressVideo)
        }

        fun bind(
            position: Int,
            media: Media,
            context: Context,
            listener: OnMediaClick?
        ) {
            this.context = context
            Glide.with(context).load(R.drawable.icon_football).into(imvUser)
            tvUsername.text = media.username
            tvCreateTime.text = media.createTime
            tvMediaDescription.text = media.mediaDescription
            Glide.with(context).load(media.videoThumbnail)
                .apply(
                    RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                )
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        mediaView.setImageDrawable(context.getDrawable(R.drawable.icon_reload))
                        progressMedia.visibility = View.GONE
                        btnPlayMedia.visibility = View.VISIBLE
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: com.bumptech.glide.load.DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        mediaView.setImageDrawable(resource)
                        progressMedia.visibility = View.INVISIBLE
                        btnPlayMedia.visibility = View.VISIBLE
                        return false
                    }
                })
                .into(mediaView)

            layoutMedia.setOnClickListener {
                listener?.onMediaClick(media)
                pauseVideo()
            }
            btnPlayMedia.setOnClickListener {
                if(::simpleExoPlayer.isInitialized){
                    if(simpleExoPlayer.playbackState == ExoPlayer.STATE_IDLE){
                        resumeVideo()
                    }
                }else{
                    playVideo(media.mediaUrl)
                    btnPlayMedia.visibility = View.INVISIBLE
                    progressMedia.visibility = View.INVISIBLE
                    mediaView.visibility = View.INVISIBLE
                }
//                listener?.onPlayClick(playerView, media, progressVideo)
            }
        }

        private fun pauseVideo() {
            if(::simpleExoPlayer.isInitialized){
                simpleExoPlayer.playWhenReady = false
                currentSeek = simpleExoPlayer.currentPosition
            }
        }

        private fun resumeVideo(){
            if(::simpleExoPlayer.isInitialized){
                simpleExoPlayer.seekTo(currentSeek)

//                simpleExoPlayer.playWhenReady = true
            }
        }

        private fun playVideo(mediaUrl: String?) {
            simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(context)
            mediaDataSourceFactory =
                DefaultDataSourceFactory(context, Util.getUserAgent(context, "mediaPlayerSample"))

            val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory).createMediaSource(
                Uri.parse(mediaUrl)
            )

            simpleExoPlayer.prepare(mediaSource, false, false)
            simpleExoPlayer.playWhenReady = true

            playerView.setShutterBackgroundColor(Color.TRANSPARENT)
            playerView.player = simpleExoPlayer
            playerView.requestFocus()

            simpleExoPlayer.addListener(object : Player.EventListener {
                override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {

                }

                override fun onTracksChanged(
                    trackGroups: TrackGroupArray?,
                    trackSelections: TrackSelectionArray?
                ) {

                }

                override fun onPlayerError(error: ExoPlaybackException?) {

                }

                /** 4 playbackState exists */
                override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                    when (playbackState) {
                        Player.STATE_BUFFERING -> {
                            progressVideo.visibility = View.VISIBLE
                        }
                        Player.STATE_READY -> {
                            progressVideo.visibility = View.INVISIBLE
                        }
                        Player.STATE_IDLE -> {
                        }
                        Player.STATE_ENDED -> {
                            progressVideo.visibility = View.INVISIBLE
                        }
                    }
                }

                override fun onLoadingChanged(isLoading: Boolean) {

                }

                override fun onPositionDiscontinuity(reason: Int) {

                }

                override fun onRepeatModeChanged(repeatMode: Int) {
                }

                override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {
                }
            })

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            MEDIA_TYPE -> MediaVH(inflater, parent)
            ADS_TYPE -> {
                val unifiedNativeLayoutView: View = LayoutInflater.from(
                    parent.context
                ).inflate(
                    R.layout.layout_native_ads_in_media_list,
                    parent, false
                )
                UnifiedNativeAdViewHolder(unifiedNativeLayoutView)
            }
            LOADMORE_TYPE -> {
                val groupView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_load_more, parent, false)
                GroupViewHolder(groupView)
            }
            else -> MediaVH(inflater, parent)
        }
    }

    inner class GroupViewHolder(itemView: View?) :
        RecyclerView.ViewHolder(itemView!!) {
        var tvLoadmore: TextView? = null
        var progressLoadmore: ProgressBar? = null

        /**
         * Constructor to initialize the Views
         *
         * @param itemView
         */
        init {
            tvLoadmore = itemView?.findViewById(R.id.btnLoadmore)
            progressLoadmore = itemView?.findViewById(R.id.progressLoadmore)
        }

        fun bind(loadmoreCallback: OnLoadMoreClick?) {
            progressLoadmore?.visibility = View.INVISIBLE
            tvLoadmore?.visibility = View.VISIBLE
            if (isNoMore) {
                tvLoadmore?.visibility = View.VISIBLE
                tvLoadmore?.text = "No more!!"
                return
            }
            if (isLoadMoreFail) {
                tvLoadmore?.text = "Try again"
            }
            tvLoadmore?.setOnClickListener {
                tvLoadmore?.visibility = View.INVISIBLE
                progressLoadmore?.visibility = View.VISIBLE
                isLoadMoreFail = false
                loadmoreCallback?.onLoadmore()
            }
        }
    }

    override fun getItemCount(): Int {
        return medias.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val type = getItemViewType(position)
        when (type) {
            MEDIA_TYPE -> (holder as MediaVH).bind(
                position,
                medias[position] as Media,
                context,
                listener
            )
            LOADMORE_TYPE -> (holder as GroupViewHolder).bind(loadmoreCallback)
            ADS_TYPE -> {
                val nativeAd: UnifiedNativeAd = medias[position] as UnifiedNativeAd
                recyclerViewStateLiveData.observeForever { t ->
                    run {
                        if (t) {
                            populateNativeAdView(nativeAd,(holder as UnifiedNativeAdViewHolder).adView)
                        }
                    }
                }
            }
        }
    }

    private fun populateNativeAdView(nativeAd: UnifiedNativeAd, adView: UnifiedNativeAdView) {
        adView.post {
            (adView.headlineView as TextView).text = nativeAd.headline
            (adView.callToActionView as Button).text = nativeAd.callToAction
            val icon: NativeAd.Image? = nativeAd.icon
            if (icon == null) {
                adView.iconView.visibility = View.INVISIBLE
            } else {
                Glide.with(context).load(icon.drawable).into(adView.iconView as ImageView)
                adView.iconView.visibility = View.VISIBLE
            }
            if (nativeAd.price == null) {
                adView.priceView.visibility = View.INVISIBLE
            } else {
                adView.priceView.visibility = View.VISIBLE
                (adView.priceView as TextView).text = nativeAd.price
            }
            if (nativeAd.store == null) {
                adView.storeView.visibility = View.INVISIBLE
            } else {
                adView.storeView.visibility = View.VISIBLE
                (adView.storeView as TextView).text = nativeAd.store
            }
            if (nativeAd.starRating == null) {
                adView.starRatingView.visibility = View.INVISIBLE
            } else {
                (adView.starRatingView as RatingBar).rating =
                    nativeAd.starRating.toFloat()
                adView.starRatingView.visibility = View.VISIBLE
            }
            if (nativeAd.advertiser == null) {
                adView.advertiserView.visibility = View.INVISIBLE
            } else {
                (adView.advertiserView as TextView).text = nativeAd.advertiser
                adView.advertiserView.visibility = View.VISIBLE
            }
            // Assign native ad object to the native view.
            adView.setNativeAd(nativeAd)
        }
    }

    fun setData(list: List<Media>, clearOldData: Boolean) {
        Handler().post {
            if (clearOldData)
                medias.clear()
            medias.addAll(list)
            if (App.mNativeAds.size > 0 && list.size > 2) {
                medias.add(medias.size - 1, App.mNativeAds[App.mNativeAds.size - 1])
            }
            notifyDataSetChanged()
        }
    }

    fun updateDataAfterLoadMore(list: List<Media>) {
        Handler().post {
            var startNotifyChange = 0
            if (list.size == 0) {
                isNoMore = true
                notifyItemChanged(medias.size - 1)
                return@post
            }
            startNotifyChange = medias.size - 1
            medias.removeAt(startNotifyChange)

            medias.addAll(list)
            if (App.mNativeAds.size > 0) {
                medias.add(medias.size - 1, App.mNativeAds[App.mNativeAds.size - 1])
            }
            notifyItemRangeChanged(startNotifyChange, medias.size - 1)
        }
    }

    fun setListener(listener: OnMediaClick) {
        this.listener = listener
    }

    fun addLoadmore(param: OnLoadMoreClick) {
        loadmoreCallback = param
        if (!isNoMore) {
            medias.add("")
            notifyItemInserted(medias.size - 1)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (medias[position] is String)
            return LOADMORE_TYPE
        else if (medias[position] is UnifiedNativeAd)
            return ADS_TYPE
        return MEDIA_TYPE
    }

    fun loadMoreFail() {
        isLoadMoreFail = true
        notifyItemChanged(medias.size - 1)
    }

    interface OnMediaClick {
        fun onMediaClick(media: Media)
        fun onPlayClick(
            playerView: PlayerView,
            media: Media,
            progressVideo: ProgressBar
        )
    }

    interface OnLoadMoreClick {
        fun onLoadmore()
    }

    fun setLiveData(idle: Boolean) {
        recyclerViewStateLiveData.postValue(idle)
    }
}