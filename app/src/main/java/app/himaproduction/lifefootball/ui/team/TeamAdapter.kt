package app.himaproduction.lifefootball.ui.team

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.data.entity.Team
import com.bumptech.glide.Glide


class TeamAdapter(
    private val context: Context,
    private val teams: ArrayList<Team> = arrayListOf()
) :
    RecyclerView.Adapter<TeamAdapter.CountriesVH>() {
    private var mContext: Context? = null
    private var listener: OnTeamClick? = null

    init {
        mContext = context
    }

    class CountriesVH(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.item_team, parent, false)
        ) {
        private var tvTeamName: TextView
        private var imvTeamLogo: ImageView
        private var layoutTeam:CardView

        init {
            tvTeamName = itemView.findViewById(R.id.tvTeamName)
            imvTeamLogo = itemView.findViewById(R.id.imv_team)
            layoutTeam = itemView.findViewById(R.id.layout_team)
        }

        fun bind(
            team: Team,
            context: Context,
            listener: OnTeamClick?
        ) {
            tvTeamName?.text = team.name
            Glide.with(context).load(team.logo).placeholder(R.drawable.icon_football)
                .error(R.drawable.icon_football).into(imvTeamLogo!!)
            layoutTeam.setOnClickListener { listener?.onTeamClick(team) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountriesVH {
        val inflater = LayoutInflater.from(parent.context)
        return CountriesVH(
            inflater,
            parent
        )
    }

    override fun getItemCount(): Int {
        return teams.size
    }

    override fun onBindViewHolder(holder: CountriesVH, position: Int) {
        holder.bind(teams[position], context, listener)
    }

    fun updateData(list: ArrayList<Team>, clearOldData: Boolean = true) {
        if (clearOldData)
            teams.clear()
        teams.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(callback: OnTeamClick) {
        this.listener = callback
    }

    interface OnTeamClick {
        fun onTeamClick(team: Team)
    }
}
