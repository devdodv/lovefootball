package app.himaproduction.lifefootball.ui.media

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import app.himaproduction.lifefootball.App
import app.himaproduction.lifefootball.R
import app.himaproduction.lifefootball.Utils
import app.himaproduction.lifefootball.data.SharedPref
import app.himaproduction.lifefootball.data.entity.MediaComment
import app.himaproduction.lifefootball.ui.base.BaseActivity
import app.himaproduction.lifefootball.ui.bottomsheet.BottomSheetFragment
import app.himaproduction.lifefootball.ui.bottomsheet.CommentAdapter
import app.himaproduction.lifefootball.ui.state.DataState
import app.wooribankvietnam.wooribankdemo.data.DataHelper
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.fragment_play_media.*


class MediaDetailActivity : BaseActivity() {
    private var commentAdapter: CommentAdapter? = null
    val media = DataHelper.watchMediaObj
    var username: String? = null
    private lateinit var simpleExoPlayer: SimpleExoPlayer
    private lateinit var mediaDataSourceFactory: DataSource.Factory
    val TAG: String = "==========="

    companion object {
        var competitionId: Long? = 0L
        var name: String? = ""
    }

    private var bottomSheetFragment: BottomSheetFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(app.himaproduction.lifefootball.R.layout.fragment_play_media)
        setupView()
    }

    override fun setupView() {
        if (media != null) {
            tvMediaDesciption.setText(media.mediaDescription, TextView.BufferType.SPANNABLE)
            tvMediaDesciption.setOriginalText(media.mediaDescription)
            commentAdapter = CommentAdapter(this, arrayListOf())
            recycler_view_comments.apply {
                adapter = commentAdapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            }
            initializePlayer()
        }
        if (App.bannerSmall != null) {
            if (App.bannerSmall?.parent != null)
                (App.bannerSmall?.parent as ViewGroup).removeView(App.bannerSmall)
            adView.addView(App.bannerSmall)
        }
        mainViewModel.fetchComments(media?.id)
        btnSend.setOnClickListener { sendComment() }
        mainViewModel.getPushMediaLiveData().observe(this, Observer {
            when (it) {
                is DataState.Loading -> {
                    showLoadingDialog("Posting...")
                }
                is DataState.Success -> {
                    hideLoading()
                    commentAdapter?.update(it.data as MediaComment)
                    edtComment.clearComposingText()
                    edtComment.text = Editable.Factory.getInstance().newEditable("")
                    username = SharedPref(this).getUsername
                }
                is DataState.Error -> {
                    hideLoading()
                }
            }
        })

        mainViewModel.getCommentsLiveData().observe(this, Observer {
            when (it) {
                is DataState.Loading -> {

                }
                is DataState.Success -> {
                    commentAdapter?.setData(it.data as List<MediaComment>)
                }
                is DataState.Error -> {

                }
            }
        })
    }

    private fun sendComment() {
        var msg = edtComment.text.toString().trim()
        if (msg.isNullOrEmpty()) {
            return
        }
        if (msg.length < 2) {
            Toast.makeText(this, "Comment must 2 letters at least!", Toast.LENGTH_SHORT).show()
            return
        }
        if (username.isNullOrEmpty())
            showInputName(msg)
        else {
            var comment = MediaComment(
                media?.id!!,
                msg,
                username!!,
                Utils.getCurrentTime("yyyy/MM/dd hh:mm:ss")
            )
            mainViewModel.pushComment(comment, media?.id)
            hideKeyboard()
        }
    }

    private fun showInputName(msg: String) {
        val dialogBuilder: android.app.AlertDialog = android.app.AlertDialog.Builder(this).create()
        val inflater = this.layoutInflater
        val dialogView: View =
            inflater.inflate(app.himaproduction.lifefootball.R.layout.layout_dialog_input, null)

        val editText =
            dialogView.findViewById<View>(app.himaproduction.lifefootball.R.id.edt_comment) as EditText
        val button1: Button =
            dialogView.findViewById<View>(app.himaproduction.lifefootball.R.id.buttonSubmit) as Button
        button1.setOnClickListener(View.OnClickListener {
            username = editText.text.toString().trim()
            if (username.isNullOrEmpty())
                return@OnClickListener
            var comment = MediaComment(
                media?.id!!,
                msg,
                username!!,
                Utils.getCurrentTime("yyyy/MM/dd hh:mm:ss")
            )
            mainViewModel.pushComment(comment, media?.id)
            SharedPref(this).setUsername(username!!)
            hideKeyboard()
            dialogBuilder.dismiss()
        })

        dialogBuilder.setView(dialogView)
        dialogBuilder.show()
    }

    private fun initializePlayer() {

        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this)

        mediaDataSourceFactory =
            DefaultDataSourceFactory(this, Util.getUserAgent(this, "mediaPlayerSample"))

        val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory).createMediaSource(
            Uri.parse(media?.mediaUrl)
        )

        simpleExoPlayer.prepare(mediaSource, false, false)
        simpleExoPlayer.playWhenReady = true

        playerView.setShutterBackgroundColor(Color.TRANSPARENT)
        playerView.player = simpleExoPlayer
        playerView.requestFocus()

        simpleExoPlayer.addListener(object : Player.EventListener {
            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {
                Log.d(TAG, "onPlaybackParametersChanged: ")
            }

            override fun onTracksChanged(
                trackGroups: TrackGroupArray?,
                trackSelections: TrackSelectionArray?
            ) {
                Log.d(TAG, "onTracksChanged: ")
            }

            override fun onPlayerError(error: ExoPlaybackException?) {
                Log.d(TAG, "onPlayerError: ")
            }

            /** 4 playbackState exists */
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    Player.STATE_BUFFERING -> {
                        progressMedia.visibility = View.VISIBLE
                        Log.d(TAG, "onPlayerStateChanged - STATE_BUFFERING")
                    }
                    Player.STATE_READY -> {
                        progressMedia.visibility = View.INVISIBLE
                        Log.d(TAG, "onPlayerStateChanged - STATE_READY")
                    }
                    Player.STATE_IDLE -> {
                        Log.d(TAG, "onPlayerStateChanged - STATE_IDLE")
                    }
                    Player.STATE_ENDED -> {
                        progressMedia.visibility = View.INVISIBLE
                        Log.d(TAG, "onPlayerStateChanged - STATE_ENDED")
                    }
                }
            }

            override fun onLoadingChanged(isLoading: Boolean) {
                Log.d(TAG, "onLoadingChanged: ")
            }

            override fun onPositionDiscontinuity(reason: Int) {
                Log.d(TAG, "onPositionDiscontinuity: ")
            }

            override fun onRepeatModeChanged(repeatMode: Int) {
                Log.d(TAG, "onRepeatModeChanged: ")
                Toast.makeText(baseContext, "repeat mode changed", Toast.LENGTH_SHORT).show()
            }

            override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {
                Log.d(TAG, "onTimelineChanged: ")
            }
        })

    }

    private fun releasePlayer() {
        simpleExoPlayer.release()
    }

    public override fun onStart() {
        super.onStart()

        if (Util.SDK_INT > 23) initializePlayer()
    }

    public override fun onResume() {
        super.onResume()

        if (Util.SDK_INT <= 23) initializePlayer()
    }

    public override fun onPause() {
        super.onPause()

        if (Util.SDK_INT <= 23) releasePlayer()
    }

    public override fun onStop() {
        super.onStop()

        if (Util.SDK_INT > 23) releasePlayer()
    }

    override fun onReloadData() {
        TODO("Not yet implemented")
    }
}
