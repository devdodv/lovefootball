package app.himaproduction.lifefootball

import android.graphics.Bitmap
import android.net.Uri
import android.text.format.DateFormat
import android.widget.ImageView
import androidx.annotation.Nullable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.ImageViewTarget
import com.bumptech.glide.request.target.Target
import java.text.SimpleDateFormat
import java.util.*


class Utils {
    companion object {
        val GET_FOLLOWING_LEAGUES_TAG = "GET FOLLOWING LEAGUES"
        val GET_COUNTRIES_TAG = "get countries"
        val GET_LEAGUES_FROM_COUNTRIES_TAG = "get leagues from country"
        var loadingMsg = App.self().resources.getString(R.string.loading_msg)
        var noDataMsg = App.self().resources.getString(R.string.no_data_error)
        var serverErrorMsg = App.self().resources.getString(R.string.server_error)
        var noInternetErrorMsg = App.self().resources.getString(R.string.no_internet_error)

        fun getCurrentTime(pattern: String): String {
            val df: java.text.DateFormat = SimpleDateFormat(pattern)
            val today = Calendar.getInstance().time
            val todayAsString: String = df.format(today)
            return todayAsString
        }

        fun parseFullStatus(short: String): String {
            when (short) {
                "NS" -> return "Not started"
                "HT" -> return "Halftime"
                "ET" -> return "Extra time"
                "P" -> return "In penalty"
                "FT" -> return "Match finished"
                "SUSP" -> return "Suspended"
                "INT" -> return "Interupted"
                "PST" -> return "Postponed"
                "CANC" -> return "Cancelled"
                "ABD" -> return "Abandoned"
                else -> return ""
            }
        }

        fun parseNumberFromString(str: String): String {
            if (str.isNullOrEmpty()) return ""
            return StringBuilder(str).insert(str.length - 3, ".").toString()
        }

        interface LoadImageListener {
            fun onLoadSuccess(resource: Any?)
            fun onLoadFail()
        }

        fun convertTimestampToDate(ts: Long): String {
            val cal: Calendar = Calendar.getInstance(Locale.ENGLISH)
            cal.setTimeInMillis(ts * 1000L)
            val date: String = DateFormat.format("dd/MM/yyyy\nhh:mm a", cal).toString()
            return date.toString()
        }

        fun loadBitmapWithNoCache(url: String, callback: LoadImageListener, mediaView: ImageView) {
            Glide.with(App.self())
                .asBitmap()
                .load(Uri.parse(url))
                .apply(
                    RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                )
                .listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Bitmap>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        callback.onLoadFail()
                        return false
                    }

                    override fun onResourceReady(
                        resource: Bitmap?,
                        model: Any?,
                        target: Target<Bitmap>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        callback.onLoadSuccess(resource)
                        return false
                    }
                })
                .into(object : ImageViewTarget<Bitmap>(mediaView) {
                    override fun setResource(resource: Bitmap?) {
                        mediaView?.setImageBitmap(resource)
                    }
                })
        }

        fun loadGif(
            url: String,
            callback: LoadImageListener,
            mediaView: ImageView?
        ) {
            Glide.with(App.self())
                .asGif()
                .apply(
                    RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                        .skipMemoryCache(true)
                )
                .load(Uri.parse(url))
                .listener(object : RequestListener<GifDrawable?> {
                    override fun onLoadFailed(
                        @Nullable e: GlideException?, model: Any?,
                        target: Target<GifDrawable?>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        callback.onLoadFail()
                        return false
                    }

                    override fun onResourceReady(
                        resource: GifDrawable?,
                        model: Any?,
                        target: Target<GifDrawable?>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        callback.onLoadSuccess(resource)
                        return false
                    }
                }).into(mediaView!!)
        }
    }
}