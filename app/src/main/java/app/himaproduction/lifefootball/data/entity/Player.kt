package app.himaproduction.lifefootball.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
class Player {
    @SerializedName("player_id")
    @Expose
    var playerId: Int? = null
    @SerializedName("player_name")
    @Expose
    var playerName: String? = null
    @SerializedName("firstname")
    @Expose
    var firstname: String? = null
    @SerializedName("lastname")
    @Expose
    var lastname: String? = null
    @SerializedName("number")
    @Expose
    var number: Any? = null
    @SerializedName("position")
    @Expose
    var position: String? = null
    @SerializedName("age")
    @Expose
    var age: Int? = null
    @SerializedName("birth_date")
    @Expose
    var birthDate: String? = null
        get() = if (field == null) "N/A" else field
    @SerializedName("birth_place")
    @Expose
    var birthPlace: String? = null
        get() = if (field == null) "N/A" else field
    @SerializedName("birth_country")
    @Expose
    var birthCountry: String? = null
        get() = if (field == null) "N/A" else field
    @SerializedName("nationality")
    @Expose
    var nationality: String? = null
        get() = if (field == null) "N/A" else field
    @SerializedName("height")
    @Expose
    var height: String? = null
        get() = if (field == null) "N/A" else field
    @SerializedName("weight")
    @Expose
    var weight: String? = null
        get() = if (field == null) "N/A" else field
    @SerializedName("injured")
    @Expose
    var injured: Any? = null
    @SerializedName("rating")
    @Expose
    var rating: Any? = null
        get() = if (field == null) "N/A" else field
    @SerializedName("team_id")
    @Expose
    var teamId: Int? = null
    @SerializedName("team_name")
    @Expose
    var teamName: String? = null
    @SerializedName("league")
    @Expose
    var league: String? = null
        get() = if (field == null) "N/A" else field
    @SerializedName("season")
    @Expose
    var season: String? = null
        get() = if (field == null) "N/A" else field
    @SerializedName("captain")
    @Expose
    var captain: Int? = null
    @SerializedName("shots")
    @Expose
    var shots: Shots? = null
    @SerializedName("goals")
    @Expose
    var goals: Goals? = null
    @SerializedName("passes")
    @Expose
    var passes: Passes? = null
    @SerializedName("tackles")
    @Expose
    var tackles: Tackles? = null
    @SerializedName("duels")
    @Expose
    var duels: Duels? = null
    @SerializedName("dribbles")
    @Expose
    var dribbles: Dribbles? = null
    @SerializedName("fouls")
    @Expose
    var fouls: Fouls? = null
    @SerializedName("cards")
    @Expose
    var cards: Cards? = null
    @SerializedName("penalty")
    @Expose
    var penalty: Penalty? = null
    @SerializedName("games")
    @Expose
    var games: Games? = null
    @SerializedName("substitutes")
    @Expose
    var substitutes: Substitutes? = null

    inner class Cards {
        @SerializedName("yellow")
        @Expose
        var yellow: Int? = null
        @SerializedName("yellowred")
        @Expose
        var yellowred: Int? = null
        @SerializedName("red")
        @Expose
        var red: Int? = null

    }

    inner class Dribbles {
        @SerializedName("attempts")
        @Expose
        var attempts: Int? = null
        @SerializedName("success")
        @Expose
        var success: Int? = null

    }

    inner class Duels {
        @SerializedName("total")
        @Expose
        var total: Int? = null
        @SerializedName("won")
        @Expose
        var won: Int? = null

    }

    inner class Fouls {
        @SerializedName("drawn")
        @Expose
        var drawn: Int? = null
        @SerializedName("committed")
        @Expose
        var committed: Int? = null

    }

    inner class Games {
        @SerializedName("appearences")
        @Expose
        var appearences: Int? = null
        @SerializedName("minutes_played")
        @Expose
        var minutesPlayed: Int? = null
        @SerializedName("lineups")
        @Expose
        var lineups: Int? = null

    }

    inner class Goals {
        @SerializedName("total")
        @Expose
        var total: Int? = null
        @SerializedName("conceded")
        @Expose
        var conceded: Int? = null
        @SerializedName("assists")
        @Expose
        var assists: Int? = null

    }

    inner class Passes {
        @SerializedName("total")
        @Expose
        var total: Int? = null
        @SerializedName("accuracy")
        @Expose
        var accuracy: Int? = null

    }

    inner class Penalty {
        @SerializedName("success")
        @Expose
        var success: Int? = null
        @SerializedName("missed")
        @Expose
        var missed: Int? = null
        @SerializedName("saved")
        @Expose
        var saved: Int? = null

    }

    inner class Shots {
        @SerializedName("total")
        @Expose
        var total: Int? = null
        @SerializedName("on")
        @Expose
        var on: Int? = null

    }

    inner class Substitutes {
        @SerializedName("in")
        @Expose
        var `in`: Int? = null
            set(`in`) {
                field = `in`
            }
        @SerializedName("out")
        @Expose
        var out: Int? = null
        @SerializedName("bench")
        @Expose
        var bench: Int? = null

    }

    inner class Tackles {
        @SerializedName("total")
        @Expose
        var total: Int? = null
        @SerializedName("blocks")
        @Expose
        var blocks: Int? = null
        @SerializedName("interceptions")
        @Expose
        var interceptions: Int? = null

    }
}
