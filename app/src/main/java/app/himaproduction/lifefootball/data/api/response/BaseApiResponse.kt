package app.himaproduction.lifefootball.data.api.response

open class BaseApiResponse<T> {
    var api: T? = null
        private set
}