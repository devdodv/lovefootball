package app.himaproduction.lifefootball.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Event(
    @SerializedName("elapsed")
    @Expose
    var elapsed: Int? = null,
    @SerializedName("team_id")
    @Expose
    var teamId: Int? = null,
    @SerializedName("teamName")
    @Expose
    var teamName: String? = null,
    @SerializedName("player_id")
    @Expose
    var playerId: Int? = null,
    @SerializedName("player")
    @Expose
    var player: String? = null,
    @SerializedName("type")
    @Expose
    var type: String? = null,
    @SerializedName("detail")
    @Expose
    var detail: String? = null
)

