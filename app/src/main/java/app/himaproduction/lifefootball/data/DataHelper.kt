package app.wooribankvietnam.wooribankdemo.data

import android.text.format.DateFormat
import app.himaproduction.lifefootball.data.entity.*
import app.himaproduction.lifefootball.data.repository.Repository
import org.koin.java.KoinJavaComponent.inject
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class DataHelper {
    private object HOLDER {
        val INSTANCE = DataHelper()
    }

    companion object {
        var selectedLeague: League? = null
        var leaguesByCountry: ArrayList<League>? = null
        var followingLeaguesMap: HashMap<Int, League>? = null
        var countries:List<Country>? = null
        var watchMediaObj:Media? = null
        var fixtureForDetail:Fixture? = null
        var selectedTeam:Team?=null
        var setting:Setting? = null
        var followingLeagues:ArrayList<League>?=null

        fun getFollowingLeagues(league: League) {
            if (followingLeaguesMap == null)
                followingLeaguesMap = HashMap()
            followingLeaguesMap?.put(league.league_id, league)
        }
    }

}