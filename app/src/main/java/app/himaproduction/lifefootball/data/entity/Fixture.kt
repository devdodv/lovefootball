package app.himaproduction.lifefootball.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Fixture {
	@SerializedName("fixture_id")
	@Expose
	var fixtureId: Int? = null
	@SerializedName("league_id")
	@Expose
	var leagueId: Int? = null
	@SerializedName("event_date")
	@Expose
	var eventDate: String? = null
	@SerializedName("event_timestamp")
	@Expose
	var eventTimestamp: Long? = null
	@SerializedName("firstHalfStart")
	@Expose
	var firstHalfStart: Int? = null
	@SerializedName("secondHalfStart")
	@Expose
	var secondHalfStart: Int? = null
	@SerializedName("round")
	@Expose
	var round: String? = null
	@SerializedName("status")
	@Expose
	var status: String? = null
	@SerializedName("statusShort")
	@Expose
	var statusShort: String? = null
	@SerializedName("elapsed")
	@Expose
	var elapsed: Int? = null
	@SerializedName("venue")
	@Expose
	var venue: String? = null
	@SerializedName("referee")
	@Expose
	var referee: Any? = null
	@SerializedName("homeTeam")
	@Expose
	var homeTeam: HomeTeam? = null
	@SerializedName("awayTeam")
	@Expose
	var awayTeam: AwayTeam? = null
	@SerializedName("goalsHomeTeam")
	@Expose
	var goalsHomeTeam: Int? = null
	@SerializedName("goalsAwayTeam")
	@Expose
	var goalsAwayTeam: Int? = null
	@SerializedName("score")
	@Expose
	var score: Score? = null
}

data class HomeTeam (
	val team_id : Int,
	val team_name : String,
	val logo : String
)
data class AwayTeam (
	val team_id : Int,
	val team_name : String,
	val logo : String
)
data class Score (
	val halftime : String,
	val fulltime : String,
	val extratime : String,
	val penalty : String
)