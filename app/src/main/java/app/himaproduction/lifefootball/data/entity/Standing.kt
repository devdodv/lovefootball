package app.himaproduction.lifefootball.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Standing {
    @SerializedName("rank")
    @Expose
    var rank: Int? = null
    @SerializedName("team_id")
    @Expose
    var teamId: Int? = null
        get() = if (field == null) -1 else field
    @SerializedName("teamName")
    @Expose
    var teamName: String? = null
    @SerializedName("logo")
    @Expose
    var logo: String? = null
        get() = if (field == null) "nologo" else field
    @SerializedName("group")
    @Expose
    var group: String? =null
        get() = if (field == null) "Group --- " else field
    @SerializedName("forme")
    @Expose
    var forme: String? = null
    @SerializedName("description")
    @Expose
    var description: String? = null
    @SerializedName("all")
    @Expose
    var all: All? = null
    @SerializedName("home")
    @Expose
    var home: Home? = null
    @SerializedName("away")
    @Expose
    var away: Away? = null
    @SerializedName("goalsDiff")
    @Expose
    var goalsDiff: Int? = null
    @SerializedName("points")
    @Expose
    var points: Int? = null
    @SerializedName("lastUpdate")
    @Expose
    var lastUpdate: String? = null

    inner class All : BaseStanding()
    inner class Away : BaseStanding()
    inner class Home : BaseStanding()
}