package app.himaproduction.lifefootball.data.api.response

import app.himaproduction.lifefootball.data.entity.Country

class CountriesResponse(var results: Int, var countries: ArrayList<Country>) :
    BaseApiResponse<CountriesResponse>()