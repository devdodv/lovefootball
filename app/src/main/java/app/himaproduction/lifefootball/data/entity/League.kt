package app.himaproduction.lifefootball.data.entity

import androidx.room.*
import app.himaproduction.lifefootball.Const
import com.google.gson.Gson
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


@Entity(tableName = Const.TABLE_NAME_FAVORITE_LEAGUES)
data class League(
    @PrimaryKey
    @SerializedName("league_id")
    val league_id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("type")
    val type: String?,
    @SerializedName("country")
    val country: String,
    @SerializedName("country_code")
    val country_code: String?,
    @SerializedName("season")
    val season: Int?,
    @SerializedName("season_start")
    val season_start: String?,
    @SerializedName("season_end")
    val season_end: String?,
    @SerializedName("logo")
    val logo: String?,
    @SerializedName("flag")
    val flag: String?,
    @SerializedName("standings")
    val standings: Int?,
    @SerializedName("is_current")
    val is_current: Int?,
    @Embedded(prefix = "coverage in league")
    @SerializedName("coverage")
    val coverage: Coverage?
)

data class Coverage(
    val standings: Boolean?,
    @Embedded(prefix = "fixtures in coverage of league")
    val fixtures: Fixtures?,
    val players: Boolean?,
    val topScorers: Boolean?,
    val predictions: Boolean?,
    val odds: Boolean?
)

data class Fixtures(
    val events: Boolean,
    val lineups: Boolean,
    val statistics: Boolean,
    val players_statistics: Boolean
)
