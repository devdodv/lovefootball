package app.himaproduction.lifefootball.data.api

import app.himaproduction.lifefootball.data.api.response.*
import app.himaproduction.lifefootball.data.entity.Team
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path


/**
 * Defines the abstract methods used for interacting with the API
 */
interface ApiService {
    @GET("countries")
    fun getCountriesApi(): Flowable<CountriesResponse>

    @GET("leagues/country/{country_name}/{season}")
    fun getLeaguesFromCountryApi(
        @Path("country_name") country: String,
        @Path("season") season: String
    ): Flowable<LeaguesResponse>

    @GET("fixtures/league/{league_id}/next/{number}")
    fun getSomeNextFixtureFromLeagueApi(
        @Path("league_id") leagueId: Int,
        @Path("number") number: Int
    ): Flowable<FixturesResponse>

    @GET("fixtures/league/{league_id}/last/{number}")
    fun getSomeLastFixtureFromLeagueApi(
        @Path("league_id") leagueId: Int,
        @Path("number") number: Int
    ): Flowable<FixturesResponse>

    @GET("fixtures/date/{date}")
    fun getFixturesByDateApi(@Path("date") date: String)

    @GET("topscorers/{league_id}")
    fun getTopScorerFromLeagueApi(@Path("league_id") leagueId: Int): Flowable<TopscorerResponse>

    @GET("leagueTable/{league_id}")
    fun getStandingFromLeagueApi(@Path("league_id") leagueId: Int): Flowable<StandingResponse>

    @GET("/events/{fixture_id}")
    fun getEventApi(@Path("fixture_id") fixtureId: Int): Flowable<EventsResponse?>

    @GET("statistics/fixture/{fixture_id}")
    fun getFixtureStatisticApi(@Path("fixture_id") fixtureId: Int): Flowable<FixtureStatisticResponse>

    @GET("teams/league/{leauge_id}")
    fun getTeamsFromLeagueApi(@Path("leauge_id") leagueId: Int): Flowable<TeamsResponse>

    @GET("players/squad/{team_id}/{season}")
    fun getPlayersFromTeam(@Path("team_id") teamId: Int,@Path("season") season: String): Flowable<PlayersResponse>

    @GET("statistics/{league_id}/{team_id}")
    fun getTeamStatistic(@Path("league_id") leagueId: Int,@Path("team_id") teamId: Int): Flowable<TeamStatisticResponse>

    @GET("predictions/{fixture_id}")
    fun getPredictionApi(@Path("fixture_id") fixtureId: Int): Flowable<PredictionResponse>

}