package app.himaproduction.lifefootball.data.repository

import app.himaproduction.lifefootball.data.api.response.MediaResponse
import app.himaproduction.lifefootball.data.entity.*
import app.himaproduction.lifefootball.data.source.ApiSource
import app.himaproduction.lifefootball.data.source.DBSource
import app.himaproduction.lifefootball.data.source.MemorySource
import com.google.firebase.firestore.DocumentSnapshot
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class RepositoryImpl(
    val apiSource: ApiSource,
    val dbSource: DBSource,
    val memorySource: MemorySource
) : Repository {
    override fun getMedias(lastSnapshot: DocumentSnapshot?): Flowable<MediaResponse> {
        return apiSource.getMediasApi(lastSnapshot)
    }

    /*
     * Get countries from memory first. If it has no,call to DB. If DB has no,call to Api
     * After call from Api,save countries to DB and memory imediately
     * After get countries from DB,save it to memory imediately
     */
    override fun getCountries(): Flowable<List<Country>> {
        return Flowable.concat(
            getCountriesFromMemory(),
            getCountriesFromDB(),
            getCountriesFromAPI()
        )
            .firstElement()
            .toFlowable()
    }

    private fun getCountriesFromMemory() = memorySource.getCountries()

    private fun getCountriesFromDB() = dbSource.getCountriesFromDB().doOnNext {
        memorySource.saveCountries(it)
    }

    private fun getCountriesFromAPI() = apiSource.getCountriesApi().doOnNext {
        dbSource.saveCountriesToDB(it)
        memorySource.saveCountries(it)
    }

    /*
     *  Following leagues
     *  Save following league must return Folowable<League> because follow league from Country,
     *  following leagues screen will be updated imediately
     */
    override fun saveFollowingLeague(league: League): Flowable<League> {
        return dbSource.saveFollowingLeague(league)
            .andThen(memorySource.saveFollowingLeague(league))
    }

    override fun getFollowingLeagues(): Flowable<List<League>> {
        return Flowable.concat(
            getFollowingLeaguesFromMemory(),
            getFollowingLeaguesFromDB()
        ).firstElement().toFlowable()
    }

    private fun getFollowingLeaguesFromMemory() = memorySource.getFollowingLeagues()
    private fun getFollowingLeaguesFromDB() =
        dbSource.getFollowingLeagueFromDB().doOnNext { memorySource.saveFollowingLeagues(it) }

    override fun getLeaguesFromCountry(
        countryName: String,
        season: String
    ): Flowable<List<League>> {
        return Flowable.concat(
            getLeaguesFromCountryMemory(countryName),
//            getLeaguesFromCountryDB(countryName),
            getLeaguesFromCountryApi(countryName, season)
        ).firstElement().toFlowable()
    }

    private fun getLeaguesFromCountryMemory(countryName: String) =
        memorySource.getLeaguesFromCountry(countryName)

//    private fun getLeaguesFromCountryDB(countryName: String)
//     = dbSource.getLeaguesFromCountry(countryName).doOnNext{memorySource.saveLeaguesFromCountry(it,countryName)}

    private fun getLeaguesFromCountryApi(countryName: String, season: String) =
        apiSource.getLeaguesFromCountryApi(countryName, season)
            .doOnNext { memorySource.saveLeaguesFromCountry(it, countryName) }


    override fun getSomeNextFixturesFromLeague(
        leagueId: Int,
        number: Int
    ): Flowable<List<Fixture>> {
        return apiSource.getSomeNextFixturesFromLeagueApi(leagueId, number)
    }

    override fun getStandingFromLeague(leagueId: Int): Flowable<ArrayList<Any>> {
        return apiSource.getStandingFromLeagueApi(leagueId)
    }

    override fun getTopScorerFromLeague(leagueId: Int): Flowable<List<Topscorer>> {
        return apiSource.getTopScorerFromLeagueApi(leagueId)
    }


    override fun unFollowLeague(league: League): Flowable<League> {
        return dbSource.deleteFollowingLeague(league)
            .andThen(memorySource.removeFollowingLeague(league))
    }

    override fun getEvent(fixtureId: Int): Flowable<List<Event>> {
        return apiSource.getEventFromFixtureApi(fixtureId)
    }

    override fun getStatisticFromFixture(fixtureId: Int): Flowable<BaseDataApi> {
        return apiSource.getStatisticFromFixtureApi(fixtureId)
    }

    override fun pushComment(msg: MediaComment, id: String?): Flowable<MediaComment> {
        return apiSource.pushComment(msg, id)
    }

    override fun fetchComments(id: String?): Flowable<List<MediaComment>> {
        return apiSource.getComments(id)
    }

    override fun uploadMedia(): Completable {
        return apiSource.uploadMedia()
    }

    override fun fetchTeams(leagueId: Int): Flowable<List<Team>> {
        return apiSource.fetchTeamsApi(leagueId)
    }

    override fun fetchPlayersFromTeam(teamId: Int): Flowable<List<Player>> {
        return apiSource.fetchPlayerFromTeamApi(teamId)
    }

    override fun getTeamStatistic(teamId: Int, leagueId: Int): Flowable<ArrayList<Any>> {
        return apiSource.getTeamStatisticApi(teamId, leagueId)
    }

    override fun isFollowedLeague(leagueId: Int): Flowable<Boolean> {
        return Flowable.concat(memorySource.isFollowed(leagueId), dbSource.isFollowed(leagueId))
            .firstElement().toFlowable()
    }

    override fun getSetting(): Flowable<Setting> {
        return apiSource.getSetting()
    }

    override fun fetchPrediction(fixtureId: Int): Flowable<BaseDataApi> {
        return apiSource.fetchPrediction(fixtureId)
    }

    override fun postFeedback(feedbackMsg: String, username: String?): Completable {
        return apiSource.postFeedback(feedbackMsg, username!!)
    }

    override fun getPredictionFixtures(url: String): Flowable<ArrayList<PredictionFixture>> {
        return apiSource.getPredictionFixtures(url)
    }
}
