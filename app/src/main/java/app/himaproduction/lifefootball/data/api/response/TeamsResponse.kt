package app.himaproduction.lifefootball.data.api.response

import app.himaproduction.lifefootball.data.entity.Team

class TeamsResponse(var results: Int, var teams: List<Team>) :
    BaseApiResponse<TeamsResponse>()