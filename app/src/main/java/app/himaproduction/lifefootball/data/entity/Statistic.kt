package app.himaproduction.lifefootball.data.entity

import android.text.TextUtils
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Statistic {
    @SerializedName("Shots on Goal")
    @Expose
    var shotsOnGoal: ShotsOnGoal? = null
    @SerializedName("Shots off Goal")
    @Expose
    var shotsOffGoal: ShotsOffGoal? = null
    @SerializedName("Total Shots")
    @Expose
    var totalShots: TotalShots? = null
    @SerializedName("Blocked Shots")
    @Expose
    var blockedShots: BlockedShots? = null
    @SerializedName("Shots insidebox")
    @Expose
    var shotsInsidebox: ShotsInsidebox? = null
    @SerializedName("Shots outsidebox")
    @Expose
    var shotsOutsidebox: ShotsOutsidebox? = null
    @SerializedName("Fouls")
    @Expose
    var fouls: Fouls? = null
    @SerializedName("Corner Kicks")
    @Expose
    var cornerKicks: CornerKicks? = null
    @SerializedName("Offsides")
    @Expose
    var offsides: Offsides? = null
    @SerializedName("Ball Possession")
    @Expose
    var ballPossession: BallPossession? = null
    @SerializedName("Yellow Cards")
    @Expose
    var yellowCards: YellowCards? = null
    @SerializedName("Red Cards")
    @Expose
    var redCards: RedCards? = null
    @SerializedName("Goalkeeper Saves")
    @Expose
    var goalkeeperSaves: GoalkeeperSaves? = null
    @SerializedName("Total passes")
    @Expose
    var totalPasses: TotalPasses? = null
    @SerializedName("Passes accurate")
    @Expose
    var passesAccurate: PassesAccurate? = null
    @SerializedName("Passes %")
    @Expose
    var passes: Passes? = null

    inner class ShotsOnGoal : BaseHomeAway("Shots on goal")
    inner class ShotsOffGoal : BaseHomeAway("Shots off goal")
    inner class TotalShots : BaseHomeAway("Total shots")
    inner class BlockedShots : BaseHomeAway("Blocked shots")
    inner class ShotsInsidebox : BaseHomeAway("Shots insidebox")
    inner class ShotsOutsidebox : BaseHomeAway("Shots outsidebox")
    inner class Fouls : BaseHomeAway("Fouls")
    inner class CornerKicks : BaseHomeAway("Corner kicks")
    inner class Offsides : BaseHomeAway("Offsides")
    inner class BallPossession : BaseHomeAway("Ball possession")
    inner class YellowCards : BaseHomeAway("Yellow cards")
    inner class RedCards : BaseHomeAway("Red cards")
    inner class GoalkeeperSaves : BaseHomeAway("Goalkeeper saves")
    inner class TotalPasses : BaseHomeAway("Total passes")
    inner class PassesAccurate : BaseHomeAway("Pass accurate")
    inner class Passes : BaseHomeAway("Pass %")
}

open class BaseHomeAway(val label:String) {
    @SerializedName("home")
    @Expose
    var home: String? = null
        get() = if (TextUtils.isEmpty(field)) "0" else field
    @SerializedName("away")
    @Expose
    var away: String? = null
        get() = if (TextUtils.isEmpty(field)) "0" else field

}