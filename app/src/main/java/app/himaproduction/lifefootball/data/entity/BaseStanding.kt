package app.himaproduction.lifefootball.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseStanding {
    @SerializedName("matchsPlayed")
    @Expose
    var matchsPlayed: Int? = null
    @SerializedName("win")
    @Expose
    var win: Int? = null
    @SerializedName("draw")
    @Expose
    var draw: Int? = null
    @SerializedName("lose")
    @Expose
    var lose: Int? = null
    @SerializedName("goalsFor")
    @Expose
    var goalsFor: Int? = null
    @SerializedName("goalsAgainst")
    @Expose
    var goalsAgainst: Int? = null

}