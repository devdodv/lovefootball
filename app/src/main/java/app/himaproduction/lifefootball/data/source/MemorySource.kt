package app.himaproduction.lifefootball.data.source

import app.himaproduction.lifefootball.data.entity.Country
import app.himaproduction.lifefootball.data.entity.League
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface MemorySource {
    fun getCountries():Flowable<List<Country>>
    fun saveCountries(countries:List<Country>)
    fun saveFollowingLeague(league: League): Flowable<League> // save after click follow button
    fun saveFollowingLeagues(leagues: List<League>) //save after get following from db
    fun removeFollowingLeague(league: League):Flowable<League>
    fun getFollowingLeagues(): Flowable<List<League>>
    fun isFollowed(leagueId: Int): Flowable<Boolean>
    fun isCachedCountries():Single<Boolean>

    fun saveLeaguesFromCountry(leagues: List<League>, countryId: String)
    fun getLeaguesFromCountry(countryId: String):Flowable<List<League>>
}