package app.himaproduction.lifefootball.data.api.response

import app.himaproduction.lifefootball.data.entity.Statistic
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FixtureStatisticResponse : BaseApiResponse<FixtureStatisticResponse?>() {
    @SerializedName("results")
    @Expose
    var results: Int? = null

    @SerializedName("statistics")
    @Expose
    var statistics: Statistic? = null

}