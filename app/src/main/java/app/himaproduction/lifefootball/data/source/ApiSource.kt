package app.himaproduction.lifefootball.data.source
import app.himaproduction.lifefootball.data.api.response.MediaResponse
import app.himaproduction.lifefootball.data.entity.*
import com.google.firebase.firestore.DocumentSnapshot
import io.reactivex.Completable
import io.reactivex.Flowable

interface ApiSource {
    fun getMediasApi(lastSnapshot: DocumentSnapshot?): Flowable<MediaResponse>
    fun getCountriesApi(): Flowable<List<Country>>
    fun getLeaguesFromCountryApi(country: String, season: String): Flowable<List<League>>
    fun getSomeLastFixturesFromLeagueApi(
        leagueId: Int,
        numberOfMatches: Int
    ): Flowable<List<Fixture>>

    fun getSomeNextFixturesFromLeagueApi(leagueId: Int, numberOfMatches: Int): Flowable<List<Fixture>>
    fun getStandingFromLeagueApi(leagueId: Int): Flowable<ArrayList<Any>>
    fun getTopScorerFromLeagueApi(leagueId: Int):Flowable<List<Topscorer>>
    fun getEventFromFixtureApi(fixtureId:Int):Flowable<List<Event>>
    fun getStatisticFromFixtureApi(fixtureId: Int): Flowable<BaseDataApi>
    fun pushComment(msg: MediaComment, id: String?): Flowable<MediaComment>
    fun getComments(id: String?): Flowable<List<MediaComment>>
    fun uploadMedia(): Completable
    fun fetchTeamsApi(leagueId: Int): Flowable<List<Team>>
    fun fetchPlayerFromTeamApi(teamId: Int): Flowable<List<Player>>
    fun getTeamStatisticApi(teamId: Int,leagueId: Int): Flowable<ArrayList<Any>>
    fun getSetting(): Flowable<Setting>
    fun fetchPrediction(fixtureId: Int): Flowable<BaseDataApi>
    fun postFeedback(msg:String,use:String):Completable
    fun getPredictionFixtures(url:String):Flowable<ArrayList<PredictionFixture>>
}