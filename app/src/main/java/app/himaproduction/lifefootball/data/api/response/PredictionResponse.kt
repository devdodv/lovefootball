package app.himaproduction.lifefootball.data.api.response

import app.himaproduction.lifefootball.data.entity.Prediction
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PredictionResponse(
    @SerializedName("results")
    @Expose
    var results: Int? = null,

    @SerializedName("predictions")
    @Expose
    var predictions: List<Prediction>? = null
) : BaseApiResponse<PredictionResponse>()