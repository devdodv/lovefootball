package app.himaproduction.lifefootball.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import app.himaproduction.lifefootball.Const

@Entity(tableName = Const.TABLE_NAME_COUNTRIES)
class Country(
    @PrimaryKey(autoGenerate = true)
    var id:Int,
    var code: String?,
    var country: String,
    var flag: String?)