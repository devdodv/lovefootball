package app.himaproduction.lifefootball.data.api.response

import app.himaproduction.lifefootball.data.entity.Topscorer
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TopscorerResponse : BaseApiResponse<TopscorerResponse?>() {
    @SerializedName("results")
    @Expose
    var results: Int? = null
    @SerializedName("topscorers")
    @Expose
    var topscorers: List<Topscorer>? = null

}