package app.himaproduction.lifefootball.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Topscorer {
    @SerializedName("player_id")
    @Expose
    var playerId: Int? = null
    @SerializedName("player_name")
    @Expose
    var playerName: String? = null
    @SerializedName("firstname")
    @Expose
    var firstname: String? = null
    @SerializedName("lastname")
    @Expose
    var lastname: String? = null
    @SerializedName("position")
    @Expose
    var position: String? = null
    @SerializedName("nationality")
    @Expose
    var nationality: String? = null
    @SerializedName("team_id")
    @Expose
    var teamId: Int? = null
    @SerializedName("team_name")
    @Expose
    var teamName: String? = null
    @SerializedName("games")
    @Expose
    var games: Games? = null
    @SerializedName("goals")
    @Expose
    var goals: Goals? = null
    @SerializedName("shots")
    @Expose
    var shots: Shots? = null
    @SerializedName("penalty")
    @Expose
    var penalty: Penalty? = null
    @SerializedName("cards")
    @Expose
    var cards: Cards? = null

    inner class Cards {
        @SerializedName("yellow")
        @Expose
        var yellow: Int? = null
        @SerializedName("second_yellow")
        @Expose
        var secondYellow: Int? = null
        @SerializedName("red")
        @Expose
        var red: Int? = null

    }

    inner class Games {
        @SerializedName("appearences")
        @Expose
        var appearences: Int? = null
        @SerializedName("minutes_played")
        @Expose
        var minutesPlayed: Int? = null

    }

    inner class Goals {
        @SerializedName("total")
        @Expose
        var total: Int? = null
        @SerializedName("assists")
        @Expose
        var assists: Int? = null
        @SerializedName("conceded")
        @Expose
        var conceded: Any? = null

    }

    inner class Penalty {
        @SerializedName("won")
        @Expose
        var won: Int? = null
        @SerializedName("commited")
        @Expose
        var commited: Any? = null
        @SerializedName("success")
        @Expose
        var success: Int? = null
        @SerializedName("missed")
        @Expose
        var missed: Int? = null
        @SerializedName("saved")
        @Expose
        var saved: Any? = null

    }

    inner class Shots {
        @SerializedName("total")
        @Expose
        var total: Int? = null
        @SerializedName("on")
        @Expose
        var on: Int? = null

    }
}