package app.himaproduction.lifefootball.data.entity

class MediaComment(
    val id: String = "",
    val comment: String = "",
    val username: String = "",
    val createTime: String = ""
)