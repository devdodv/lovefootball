package app.himaproduction.lifefootball.data.api.response

import app.himaproduction.lifefootball.data.entity.Standing
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class StandingResponse : BaseApiResponse<StandingResponse?>() {
    @SerializedName("results")
    @Expose
    var results: Int? = null
    @SerializedName("standings")
    @Expose
    var standings: List<List<Standing>>? = null

}