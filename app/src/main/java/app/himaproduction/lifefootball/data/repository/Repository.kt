package app.himaproduction.lifefootball.data.repository

import app.himaproduction.lifefootball.data.api.response.MediaResponse
import app.himaproduction.lifefootball.data.entity.*
import com.google.firebase.firestore.DocumentSnapshot
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

interface Repository {
    fun getMedias(lastSnapshot: DocumentSnapshot?): Flowable<MediaResponse>
    fun getCountries(): Flowable<List<Country>>
    fun getLeaguesFromCountry(country: String, season: String): Flowable<List<League>>
    fun getSomeNextFixturesFromLeague(leagueId: Int, number: Int): Flowable<List<Fixture>>
    fun getStandingFromLeague(leagueId: Int): Flowable<ArrayList<Any>>
    fun getTopScorerFromLeague(leagueId: Int): Flowable<List<Topscorer>>
    fun saveFollowingLeague(league: League): Flowable<League>
    fun getFollowingLeagues(): Flowable<List<League>>
    fun unFollowLeague(league: League): Flowable<League>
    fun getEvent(fixtureId:Int):Flowable<List<Event>>
    fun getStatisticFromFixture(fixtureId: Int): Flowable<BaseDataApi>
    fun pushComment(msg: MediaComment, id: String?): Flowable<MediaComment>
    fun fetchComments(id: String?): Flowable<List<MediaComment>>
    fun uploadMedia(): Completable
    fun fetchTeams(leagueId: Int): Flowable<List<Team>>
    fun fetchPlayersFromTeam(teamId:Int):Flowable<List<Player>>
    fun getTeamStatistic(teamId: Int,leagueId: Int): Flowable<ArrayList<Any>>
    fun isFollowedLeague(leagueId: Int): Flowable<Boolean>
    fun getSetting(): Flowable<Setting>
    fun fetchPrediction(fixtureId: Int):Flowable<BaseDataApi>
    fun postFeedback(feedbackMsg: String, username: String?): Completable
    fun getPredictionFixtures(url:String):Flowable<ArrayList<PredictionFixture>>
}