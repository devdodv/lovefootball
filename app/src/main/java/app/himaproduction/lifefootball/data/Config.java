package app.himaproduction.lifefootball.data;

/**
 * Created by vando on 11/15/18.
 */

public class Config {
    //Properties class of tips by league
    public static final String CLASS_FIXTURE_BY_LEAGUE_TABLE = ".pztable";
    public static final String CLASS_FIXTURE_BY_LEAGUE_TABLE_ROW = "pzcnth";
    public static final String CLASS_FIXTURE_BY_LEAGUE_TABLE_ROW_MATCH_CONTENT = "fixt";

    //Properties class of tips by date
    public static final String CLASS_FIXTURE_BY_DATE_TABLE = ".pttable";
    public static final String CLASS_FIXTURE_BY_DATE_TABLE_ROW = "pttr";
    public static final String CLASS_FIXTURE_BY_DATE_TABLE_ROW_MATCH_CONTENT = "ptgame";

    //Properties of tip detail
    public static final String ID_TIP_DETAIL_CONTENT = "#content";

    //Properties common
    public static final String TAG_A = "A";
    public static final String ATTRIBUTE_HREF = "href";
    public static final String ID_PREDICTION = "#prediction";
    public static final String CLASS_TIP_PREDICTION_BOX = "predbox";
    public static final String TAG_P = "p";
    public static final String CLASS_PREDICTION_BOX_SCORE = "ptxtscore";
    public static final String CLASS_PREDICTION_BOX_TEAM_WIN = "ptxtteam";
    public static final String CLASS_TIP_PREDICTION_INFO = "predfixt";
    public static final String CLASS_PREDICTION_INFO_LEAGUE = "predfeat";
    public static final String CLASS_PREDICTION_INFO_MATCH = "predfixtgsm";
    public static final String CLASS_PREDICTION_INFO_MATCH_TIME = "predfixtdt";
    public static final String CLASS_TIP_PREDICTION_ODD = "predodds";
    public static final String CLASS_TIP_PREDICTION_ODD_FEATURE_BET = "oddcirccont";
    public static final String CLASS_TIP_PREDICTION_ODD_FEATURE_BET_CIRCLE = "oddcirc";
    public static final String CLASS_TIP_PREDICTION_ODD_FEATURE_BET_CIRCLE_DESC = "odddesc";
    public static final String CLASS_PREDICTION_HEAD_TO_HEAD = "predocl";
    public static final String CLASS_PREDICTION_HEAD_TO_HEAD_ROW = "pzcnt1";
    public static final String TAG_TD = "td";
    public static final String CLASS_PREDICTION_MATCH_ODDS = "predoc";
    public static final String CLASS_PREDICTION_MATCH_ODDS_ROWS = "pzcnt";
    public static final String CLASS_PREDICTION_MATCH_OVER_UNDER_GOAL = "pzcnt1";
    public static final String TAG_PREDICTION_MATCH_OVER_UNDER_GOAL_ODDS = "odds";
    public static final String TAG_H1 = "h1";
    public static final String CLASS_PREDICTION_MATCH_BOTH_TEAM_ODDS_ROWS = "pzcnt1";
    public static final String TAG_H2 = "h2";
}

