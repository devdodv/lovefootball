package app.himaproduction.lifefootball.data.memory

import app.himaproduction.lifefootball.data.entity.Country
import app.himaproduction.lifefootball.data.entity.League
import app.himaproduction.lifefootball.data.source.MemorySource
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class CacheMemoryImpl : MemorySource {
    private var followingLeaguesMap: HashMap<Int, League>? = null
    private var countries: List<Country>? = null
    private var leaguesFromCountryMap: HashMap<String, List<League>>? = null

    // Countries
    override fun getCountries(): Flowable<List<Country>> {
        return Flowable.create({
            if (countries.isNullOrEmpty())
                it.onComplete()
            else
                it.onNext(countries!!)
        }, BackpressureStrategy.LATEST)
    }

    override fun saveCountries(countries: List<Country>) {
        this.countries = countries
    }

    // save following league after click follow button
    override fun saveFollowingLeague(league: League): Flowable<League> {
        return Completable.defer {
            if (followingLeaguesMap != null)
                followingLeaguesMap!!.put(league.league_id, league)
            Completable.complete()
        }.andThen(Flowable.just(league))
    }

    override fun getFollowingLeagues(): Flowable<List<League>> {
        return Flowable.create({
            if (followingLeaguesMap == null) it.onComplete() else {
                val leagues = arrayListOf<League>()
                followingLeaguesMap?.forEach {
                    leagues.add(it.value)
                }
                it.onNext(leagues)
            }
        }, BackpressureStrategy.LATEST)
    }

    // save after get following from DB
    override fun saveFollowingLeagues(leagues: List<League>) {
        for (league in leagues) {
            followingLeaguesMap?.put(league.league_id, league)
        }
    }

    override fun saveLeaguesFromCountry(leagues: List<League>, countryId: String) {
        if (leaguesFromCountryMap == null)
            leaguesFromCountryMap = HashMap()
        leaguesFromCountryMap!![countryId] = leagues
    }

    override fun getLeaguesFromCountry(countryId: String): Flowable<List<League>> {
        return Flowable.create({
            if (leaguesFromCountryMap.isNullOrEmpty() || !leaguesFromCountryMap?.containsKey(
                    countryId
                )!!
            ) {
                it.onComplete()
            } else {
                it.onNext(leaguesFromCountryMap?.getValue(countryId)!!)
            }
        }, BackpressureStrategy.LATEST)
    }

    override fun removeFollowingLeague(league: League): Flowable<League> {
        return Flowable.defer {
            followingLeaguesMap?.remove(league.league_id)
            Flowable.just(league)
        }
    }


    override fun isFollowed(leagueId: Int): Flowable<Boolean> {
        return Flowable.create ({
            if(followingLeaguesMap == null)
                it.onComplete()
            else{
                it.onNext(followingLeaguesMap!!.containsKey(leagueId))
            }
        },BackpressureStrategy.LATEST)
    }

    override fun isCachedCountries(): Single<Boolean> {
        TODO("Not yet implemented")
    }

}