package app.himaproduction.lifefootball.data.api.response

import app.himaproduction.lifefootball.data.entity.League

class LeaguesResponse(var results: Int, var leagues: List<League>) :
    BaseApiResponse<LeaguesResponse>()