package app.himaproduction.lifefootball.data.db

import android.util.Log
import app.himaproduction.lifefootball.data.entity.Country
import app.himaproduction.lifefootball.data.entity.League
import app.himaproduction.lifefootball.data.entity.Setting
import app.himaproduction.lifefootball.data.source.DBSource
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class DaoImpl(private val db: Dao) : DBSource {
    override fun saveFollowingLeague(league: League): Completable {
        return Completable.defer {
            db.insertFollowingLeague(league)
            Completable.complete()
        }
    }


    override fun isFollowed(leagueId: Int): Flowable<Boolean> {
        return Flowable.defer{
            Flowable.just(
                db.getLeagueFromFollowingDB(leagueId)!=null
            )
        }
    }

    override fun getFollowingLeagueFromDB(): Flowable<List<League>> {
        return Flowable.defer {
            Flowable.just(db.getFollowingLeague())
        }
    }

    override fun deleteFollowingLeague(league: League): Completable {
        return Completable.defer {
            db.deleteFollowingLeauge(league)
            Completable.complete()
        }
    }

    /*
     * Saving countries to DB just after getting countries from API
     */
    override fun saveCountriesToDB(countries: List<Country>) {
            countries.forEach {
                db.insertCountry(it)
            }
    }

    override fun getCountriesFromDB(): Flowable<List<Country>> {
        return Flowable.create({
            val countries = db.getCountriesFromDB()
            if (countries.isNullOrEmpty())
                it.onComplete()
            else
                it.onNext(countries)
        }, BackpressureStrategy.LATEST)
    }
}