package app.himaproduction.lifefootball.data.api.response

import app.himaproduction.lifefootball.data.entity.Player

class PlayersResponse(var results: Int, var players: List<Player>) :
    BaseApiResponse<PlayersResponse>()