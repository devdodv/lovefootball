package app.himaproduction.lifefootball.data.api.response

import app.himaproduction.lifefootball.data.entity.Fixture

class FixturesResponse(var results: Int, var fixtures: ArrayList<Fixture>) :
    BaseApiResponse<FixturesResponse>()