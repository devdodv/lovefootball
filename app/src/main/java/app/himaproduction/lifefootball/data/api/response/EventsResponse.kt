package app.himaproduction.lifefootball.data.api.response

import app.himaproduction.lifefootball.data.entity.Event
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EventsResponse : BaseApiResponse<EventsResponse?>() {
    @SerializedName("results")
    @Expose
    var results: Int? = null

    @SerializedName("events")
    @Expose
    var events: List<Event>? = null

}