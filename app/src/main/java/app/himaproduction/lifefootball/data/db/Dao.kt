package app.himaproduction.lifefootball.data.db

import androidx.room.*
import androidx.room.Dao
import app.himaproduction.lifefootball.Const
import app.himaproduction.lifefootball.data.entity.Country
import app.himaproduction.lifefootball.data.entity.League
import app.himaproduction.lifefootball.data.entity.Setting
import io.reactivex.Completable

@Dao
interface Dao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFollowingLeague(league: League)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCountry(country: Country)

    @Query(Const.SELECT_FOLLOWING_LEAGUE)
    fun getFollowingLeague(): List<League>

    @Query("SELECT * FROM ${Const.TABLE_NAME_FAVORITE_LEAGUES} WHERE league_id = :league_id")
    fun getLeagueFromFollowingDB(league_id:Int): League

    @Delete
    fun deleteFollowingLeauge(league:League)

    @Query(Const.SELECT_ALL_COUNTRIES)
    fun getCountriesFromDB(): List<Country>
}