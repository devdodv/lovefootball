package app.himaproduction.lifefootball.data.api

import android.util.Log
import app.himaproduction.lifefootball.data.api.parse.PredictionFixtureParser
import app.himaproduction.lifefootball.data.api.response.MediaResponse
import app.himaproduction.lifefootball.data.entity.*
import app.himaproduction.lifefootball.data.source.ApiSource
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.storage.FirebaseStorage
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable

class ApiImpl constructor(val apiService: ApiService) : ApiSource {
    private var firestore = FirebaseFirestore.getInstance()
    private var mStorageRef = FirebaseStorage.getInstance().getReference()

    override fun getMediasApi(lastSnapshot: DocumentSnapshot?): Flowable<MediaResponse> {
        val getMediaFS = if (lastSnapshot == null) firestore.collection("media")
            .whereEqualTo("active", true)
            .orderBy("createTime", Query.Direction.DESCENDING)
            .limit(5)
            .get()
        else firestore.collection("media")
            .whereEqualTo("active", true)
            .orderBy("createTime", Query.Direction.DESCENDING)
            .startAfter(lastSnapshot)
            .limit(5)
            .get()

        return Flowable.create({ emitter ->
            getMediaFS
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        var medias: ArrayList<Media> = arrayListOf()
                        if (task.result == null || task.result!!.size() == 0) {
                            emitter.onNext(MediaResponse(medias, null))
                            return@addOnCompleteListener
                        }
                        var lastItem = task.result!!.last()
                        for (document in task.result!!) {
                            var media = document.toObject(Media::class.java)
                            media.id = document.id
                            medias.add(media)
                        }
                        emitter.onNext(MediaResponse(medias, lastItem))
                    } else {
                        emitter.run { onError(Throwable(task.exception)) }
                    }
                }
        }, BackpressureStrategy.LATEST)
    }

    override fun getCountriesApi(): Flowable<List<Country>> {
        return apiService.getCountriesApi().map { it.api?.countries }
    }

    override fun getLeaguesFromCountryApi(country: String, season: String): Flowable<List<League>> {
        return apiService.getLeaguesFromCountryApi(country, season).map { it.api }
            .map { it.leagues }
    }

    override fun getSomeLastFixturesFromLeagueApi(
        leagueId: Int,
        numberOfMatches: Int
    ): Flowable<List<Fixture>> {
        return apiService.getSomeLastFixtureFromLeagueApi(leagueId, numberOfMatches)
            .map { it.api }
            .map { it.fixtures }
    }

    override fun getSomeNextFixturesFromLeagueApi(
        leagueId: Int,
        numberOfMatches: Int
    ): Flowable<List<Fixture>> {
        return apiService.getSomeLastFixtureFromLeagueApi(leagueId, numberOfMatches)
            .map { it.api }
            .map { it.fixtures }
    }

    override fun getStandingFromLeagueApi(leagueId: Int): Flowable<ArrayList<Any>> {
        return apiService.getStandingFromLeagueApi(leagueId)
            .map { it.api }
            .map { parseStanding(it.standings) }
    }

    private fun parseStanding(standings: List<List<Standing>>?): ArrayList<Any>? {
        val standingList: ArrayList<Any> = arrayListOf()
        if (standings != null) {
            if (standings.size == 1) {
                standingList.addAll(standings[0])
            } else {
                for (list in standings) {
                    standingList.add(list[0].group!!)
                    standingList.addAll(list)
                }
            }
        }
        return standingList
    }

    override fun getTopScorerFromLeagueApi(leagueId: Int): Flowable<List<Topscorer>> {
        return apiService.getTopScorerFromLeagueApi(leagueId).map { it.api }.map { it.topscorers }
    }

    override fun getEventFromFixtureApi(fixtureId: Int): Flowable<List<Event>> {
        return apiService.getEventApi(fixtureId).map { it.api?.events }
    }

    override fun getStatisticFromFixtureApi(fixtureId: Int): Flowable<BaseDataApi> {
        return apiService.getFixtureStatisticApi(fixtureId).map {
            if (it.api?.results == 0) {
                BaseDataApi(0, null)
            } else {
                val statistic: Statistic = it.api?.statistics!!
                val values: ArrayList<Any> = arrayListOf()
                values.add(statistic.ballPossession!!)
                values.add(statistic.totalShots!!)
                values.add(statistic.shotsOnGoal!!)
                values.add(statistic.shotsOffGoal!!)
                values.add(statistic.shotsInsidebox!!)
                values.add(statistic.shotsOutsidebox!!)
                values.add(statistic.blockedShots!!)
                values.add(statistic.totalPasses!!)
                values.add(statistic.passesAccurate!!)
                values.add(statistic.passes!!)
                values.add(statistic.cornerKicks!!)
                values.add(statistic.fouls!!)
                values.add(statistic.goalkeeperSaves!!)
                values.add(statistic.offsides!!)
                values.add(statistic.yellowCards!!)
                values.add(statistic.redCards!!)
                BaseDataApi(it.api?.results!!, values)
            }
        }
    }

    override fun pushComment(msg: MediaComment, id: String?): Flowable<MediaComment> {
        return Flowable.create({ emitter ->
            firestore.collection("media_comments")
                .document()
                .set(msg)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        emitter.onNext(msg)
                        return@addOnCompleteListener
                    }
                    if (it.isCanceled) {
                        emitter.onError(Throwable(it.exception))
                        return@addOnCompleteListener
                    }
                    emitter.onComplete()
                }

        }, BackpressureStrategy.LATEST)
    }

    override fun getComments(id: String?): Flowable<List<MediaComment>> {
        return Flowable.create({ emitter ->
            firestore.collection("media_comments")
                .whereEqualTo("id", id)
                .orderBy("createTime", Query.Direction.DESCENDING)
                .get()
                .addOnSuccessListener {
                    var comments: ArrayList<MediaComment> = arrayListOf()
                    if (it.documents == null) {
                        emitter.onNext(comments)
                        return@addOnSuccessListener
                    }
                    for (document in it.documents) {
                        var comment = document.toObject(MediaComment::class.java)
                        comments.add(comment!!)
                    }
                    emitter.onNext(comments)
                }
                .addOnFailureListener {
                    emitter.onError(Throwable(it))
                }
        }, BackpressureStrategy.LATEST)
    }

    override fun uploadMedia(): Completable {
        return Completable.complete()
    }

    override fun fetchTeamsApi(leagueId: Int): Flowable<List<Team>> {
        return apiService.getTeamsFromLeagueApi(leagueId).map { it.api?.teams }
    }

    override fun fetchPlayerFromTeamApi(teamId: Int): Flowable<List<Player>> {
        return apiService.getPlayersFromTeam(teamId, "2019-2020").map { it.api?.players }
    }

    override fun getTeamStatisticApi(teamId: Int, leagueId: Int): Flowable<ArrayList<Any>> {
        return apiService.getTeamStatistic(leagueId, teamId).map { it.api?.statistics }
            .map {
                val statistics: ArrayList<Any> = arrayListOf()
                it.matchs.matchsPlayed.let { it1 -> statistics.add(it1) }
                it.matchs.wins.let { it1 -> statistics.add(it1) }
                it.matchs.draws.let { it1 -> statistics.add(it1) }
                it.matchs.loses.let { it1 -> statistics.add(it1) }
                it.goals.goalsFor.let { it1 -> statistics.add(it1) }
                it.goals.goalsAgainst.let { it1 -> statistics.add(it1) }
                it.goalsAvg.goalsFor.let { it1 -> statistics.add(it1) }
                it.goalsAvg.goalsAgainst.let { it1 -> statistics.add(it1) }
                statistics
            }
    }

    override fun getSetting(): Flowable<Setting> {
        return Flowable.create({ emitter ->
            firestore.collection("setting")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        var setting: Setting? = null
                        if (task.result == null) {
                            emitter.onError(Throwable(app.himaproduction.lifefootball.Utils.serverErrorMsg))
                            return@addOnCompleteListener
                        }
                        for (document in task.result!!) {
                            setting = document.toObject(Setting::class.java)
                        }
                        emitter.onNext(setting!!)
                    } else {
                        emitter.run { onError(Throwable(task.exception)) }
                    }
                }
        }, BackpressureStrategy.LATEST)
    }

    override fun fetchPrediction(fixtureId: Int): Flowable<BaseDataApi> {
        return apiService.getPredictionApi(fixtureId).map {
//            var last5MatchesHome:HashMap<String,HashMap<Any,Any>> = HashMap()
//            var l5mh = it.api?.predictions[0]?.teams.home.last5Matches.
//            last5MatchesHome.put()
            BaseDataApi(it.api?.results!!, it.api?.predictions)
        }
    }

    override fun postFeedback(msg: String, user: String): Completable {
        return Completable.create { emitter ->
            run {
                var mapData: HashMap<String, String> = HashMap()
                mapData.put("feedback", msg)
                mapData.put("username", user)
                firestore.collection("Feedback")
                    .document()
                    .set(mapData)
                    .addOnCompleteListener {
                        emitter.onComplete()
                    }
            }
        }
    }

    override fun getPredictionFixtures(url: String): Flowable<ArrayList<PredictionFixture>> {
        return PredictionFixtureParser().getPredictionFixtures(url)
    }
}