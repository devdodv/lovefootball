package app.himaproduction.lifefootball.data.entity;

import java.util.List;

/**
 * Created by vando on 11/15/18.
 * <p>
 * /**
 * Created by vando on 11/3/18.
 */

public class PredictionFixture {
    private String matchLink;
    private String dateOfMatch;
    private String matchContent;
    private String matchTime;
    private String matchId;
    private String teamHome;
    private String teamAway;
    private String homeScore;
    private String awayScore;
    private String form;
    private List<String> lastFiveMatchesHome;
    private List<String> lastFiveMatchesAway;
//    private TipDetail tipDetail;

    public String getHomeScore() {
        return homeScore == null ? "" : homeScore.trim();
    }

    public void setHomeScore(String homeScore) {
        this.homeScore = homeScore;
    }

    public String getAwayScore() {
        return awayScore == null ? "" : awayScore.trim();
    }

    public void setAwayScore(String awayScore) {
        this.awayScore = awayScore;
    }

    public String getDateOfMatch() {
        return dateOfMatch;
    }

    public void setDateOfMatch(String dateOfMatch) {
        this.dateOfMatch = dateOfMatch;
    }

    public String getTeamHome() {
        return teamHome;
    }

    public List<String> getLastFiveMatchesHome() {
        return lastFiveMatchesHome;
    }

    public void setLastFiveMatchesHome(List<String> lastFiveMatchesHome) {
        this.lastFiveMatchesHome = lastFiveMatchesHome;
    }

    public List<String> getLastFiveMatchesAway() {
        return lastFiveMatchesAway;
    }

    public void setLastFiveMatchesAway(List<String> lastFiveMatchesAway) {
        this.lastFiveMatchesAway = lastFiveMatchesAway;
    }

    public void setTeamHome(String teamHome) {
        this.teamHome = teamHome;
    }

    public String getTeamAway() {

        return teamAway;
    }

    public void setTeamAway(String teamAway) {
        this.teamAway = teamAway;
    }

//    public TipDetail getTipDetail() {
//        return tipDetail;
//    }
//
//    public void setTipDetail(TipDetail tipDetail) {
//        this.tipDetail = tipDetail;
//    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getMatchLink() {
        return matchLink;
    }

    public void setMatchLink(String matchLink) {
        this.matchLink = matchLink;
    }

    public String getMatchContent() {
        return matchContent;
    }

    public void setMatchContent(String matchContent) {
        this.matchContent = matchFormart(matchContent);
    }

    private String matchFormart(String matchContent) {
        if (matchContent != null) {
            String[] slpitContent = matchContent.split(" v ");
            if (slpitContent.length == 2) {
                setTeamHome(slpitContent[0]);
                setTeamAway(slpitContent[1]);
            }
            return matchContent.replace(" v ", " - ");
        }
        return "";
    }

    public String getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(String matchTime) {
        this.matchTime = matchTime;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }
}


