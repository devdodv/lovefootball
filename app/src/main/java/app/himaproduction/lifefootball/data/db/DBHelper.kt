package app.himaproduction.lifefootball.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import app.himaproduction.lifefootball.data.entity.Country
import app.himaproduction.lifefootball.data.entity.League

@Database(entities = arrayOf(League::class,Country::class), version = 2)
abstract class DBHelper : RoomDatabase() {

    abstract fun getDAO(): Dao

    private var INSTANCE: DBHelper? = null

    private val sLock = Any()

    fun getInstance(context: Context): DBHelper {
        if (INSTANCE == null) {
            synchronized(sLock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            DBHelper::class.java, "lovefootball.db")
                            .build()
                }
                return INSTANCE!!
            }
        }
        return INSTANCE!!
    }

}