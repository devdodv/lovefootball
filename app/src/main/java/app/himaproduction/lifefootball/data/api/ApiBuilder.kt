package app.himaproduction.lifefootball.data.api

import app.himaproduction.lifefootball.BuildConfig
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Provide "make" methods to create instances of [BufferooService]
 * and its related dependencies, such as OkHttpClient, Gson, etc.
 */
object ApiBuilder {
    fun getWebService(): ApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .header("Accept", "application/json")
                .header("X-RapidAPI-Key",BuildConfig.API_KEY)
                .method(original.method(), original.body())
                .build()
            chain.proceed(request)
        }
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(logging)
        val retrofit = Retrofit.Builder().baseUrl(BuildConfig.API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient.build())
            .build()
        return retrofit.create(ApiService::class.java)
    }
//    fun createApiService(isDebug: Boolean): ApiService {
//        val okHttpClient = makeOkHttpClient(
//            makeLoggingInterceptor(isDebug)
//        )
//        return makeApiService(okHttpClient, makeGson())
//    }
//
//
//    private fun makeApiService(okHttpClient: OkHttpClient, gson: Gson): ApiService {
//        val retrofit = Retrofit.Builder()
//            .baseUrl("https://api-football-v1.p.rapidapi.com/v2/")
//            .client(okHttpClient)
//            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//            .addConverterFactory(GsonConverterFactory.create(gson))
//            .build()
//        return retrofit.create(ApiService::class.java)
//    }
//
//    private fun makeOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
//        val httpClient = OkHttpClient.Builder()
//        httpClient.addInterceptor { chain ->
//            val original = chain.request()
//            val request = original.newBuilder()
//                .header("X-RapidAPI-Key", BuildConfig.API_KEY)
//                .header("Accept", "application/json")
//                .method(original.method(), original.body())
//                .build()
//            chain.proceed(request)
//        }
//
//        return httpClient
//            .addInterceptor(httpLoggingInterceptor)
//            .connectTimeout(10, TimeUnit.SECONDS)
//            .readTimeout(10, TimeUnit.SECONDS)
//            .build()
//    }
//
//    private fun makeGson(): Gson {
//        return GsonBuilder()
//            .setLenient()
//            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
//            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
//            .create()
//    }
//
//    private fun makeLoggingInterceptor(isDebug: Boolean): HttpLoggingInterceptor {
//        val logging = HttpLoggingInterceptor()
//        logging.level = if (isDebug)
//            HttpLoggingInterceptor.Level.BODY
//        else
//            HttpLoggingInterceptor.Level.NONE
//        return logging
//    }

}