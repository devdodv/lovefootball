package app.himaproduction.lifefootball.data.api.parse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import app.himaproduction.lifefootball.data.Config;
import app.himaproduction.lifefootball.data.entity.PredictionFixture;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;

/**
 * Created by vando on 11/23/18.
 */

public class PredictionFixtureParser {
    private ArrayList<PredictionFixture> matches;
    org.jsoup.nodes.Document document = null;

    public interface FetchMatchesCallback {
        void onSuccess(ArrayList<PredictionFixture> matchList);

        void onFail(String error);
    }

    public Flowable<ArrayList<PredictionFixture>> getPredictionFixtures(String url) {
        return buildFlowable(url);
    }

    private Flowable<ArrayList<PredictionFixture>> buildFlowable(String url) {
        return Flowable.create(emitter -> {
            try {
                emitter.onNext(fetchMatches(getUrl(url)));
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }
        }, BackpressureStrategy.LATEST);
    }

    //get all matches by league
    private ArrayList<PredictionFixture> fetchMatches(URL leagueUrl) {
        try {
            document = Jsoup.connect(leagueUrl.toString()).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (document != null) {
            matches = new ArrayList<>();
            Element element;
            element = Jsoup.parse(document.toString()).select(Config.CLASS_FIXTURE_BY_LEAGUE_TABLE).first();
            if (element != null) {
                Elements sub = element.getElementsByClass(Config.CLASS_FIXTURE_BY_LEAGUE_TABLE_ROW);
                for (Element row : sub) {
                    String name = row.getElementsByClass(Config.CLASS_FIXTURE_BY_LEAGUE_TABLE_ROW_MATCH_CONTENT).text();
                    String link = row.getElementsByTag(Config.TAG_A).attr(Config.ATTRIBUTE_HREF);
                    Element last5Home = row.select(".last5box").first();
                    Element last5Away = row.select(".last5box").last();
                    Elements last5HomeElms = last5Home.getElementsByClass("neonboxsml2");
                    Elements last5AwayElms = last5Away.getElementsByClass("neonboxsml2");
                    List<String> last5HomeList = new ArrayList<>();
                    List<String> last5AwayList = new ArrayList<>();

                    for (Element elm : last5HomeElms) {
                        String result = elm.text();
                        last5HomeList.add(result);
                    }
                    for (Element elm : last5AwayElms) {
                        String result = elm.text();
                        last5AwayList.add(result);
                    }
                    PredictionFixture match = new PredictionFixture();
                    match.setMatchContent(name);
                    match.setLastFiveMatchesHome(last5HomeList);
                    match.setLastFiveMatchesAway(last5AwayList);
                    match.setMatchLink(link);
                    matches.add(match);
                }
            } else {
                element = Jsoup.parse(document.toString()).select(Config.CLASS_FIXTURE_BY_DATE_TABLE).first();
                Elements sub = element.getElementsByClass(Config.CLASS_FIXTURE_BY_DATE_TABLE_ROW);
                for (Element row : sub) {
                    List<String> last5HomeList = new ArrayList<>();
                    List<String> last5AwayList = new ArrayList<>();
                    String name = row.getElementsByClass(Config.CLASS_FIXTURE_BY_DATE_TABLE_ROW_MATCH_CONTENT).text();
                    String link = row.getElementsByTag(Config.TAG_A).first().attr(Config.ATTRIBUTE_HREF);
                    Element last5Home = row.select(".last5box").first();
                    Elements last5HomeElms = last5Home.getElementsByClass("neonboxsml2");
                    for (Element elm : last5HomeElms) {
                        String result = elm.text();
                        last5HomeList.add(result);
                    }
                    Element last5Away = row.select(".last5box").last();
                    Elements last5AwayElms = last5Away.getElementsByClass("neonboxsml2");
                    for (Element elm : last5AwayElms) {
                        String result = elm.text();
                        last5AwayList.add(result);
                    }

                    PredictionFixture match = new PredictionFixture();
                    match.setMatchContent(name);
                    match.setLastFiveMatchesHome(last5HomeList);
                    match.setLastFiveMatchesAway(last5AwayList);
                    match.setMatchLink(link);
                    matches.add(match);
                }
            }
        }
        return matches;
    }

    private URL getUrl(String url) {
        URL url1;
        try {
            url1 = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
        return url1;
    }
}
