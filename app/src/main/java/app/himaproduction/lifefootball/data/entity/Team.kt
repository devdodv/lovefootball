package app.himaproduction.lifefootball.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Team {
    @SerializedName("team_id")
    @Expose
    var teamId: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("code")
    @Expose
    var code: String? = null
    @SerializedName("logo")
    @Expose
    var logo: String? = null
    @SerializedName("country")
    @Expose
    var country: String? = null
    @SerializedName("founded")
    @Expose
    var founded: Int? = null
    @SerializedName("venue_name")
    @Expose
    var venueName: String? = null
    @SerializedName("venue_surface")
    @Expose
    var venueSurface: String? = null
    @SerializedName("venue_address")
    @Expose
    var venueAddress: String? = null
    @SerializedName("venue_city")
    @Expose
    var venueCity: String? = null
    @SerializedName("venue_capacity")
    @Expose
    var venueCapacity: Int? = null
}