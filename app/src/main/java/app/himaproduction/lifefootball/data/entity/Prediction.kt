package app.himaproduction.lifefootball.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class Prediction {
    @SerializedName("match_winner")
    @Expose
    var matchWinner: String? = null

    @SerializedName("under_over")
    @Expose
    var underOver: Any? = null

    @SerializedName("goals_home")
    @Expose
    var goalsHome: String? = null

    @SerializedName("goals_away")
    @Expose
    var goalsAway: String? = null

    @SerializedName("advice")
    @Expose
    var advice: String? = null

    @SerializedName("winning_percent")
    @Expose
    var winningPercent: WinningPercent? = null

    @SerializedName("teams")
    @Expose
    var teams: Teams? = null

    @SerializedName("h2h")
    @Expose
    var h2h: List<H2h>? = null

    @SerializedName("comparison")
    @Expose
    var comparison: Comparison? =
        null

    internal inner class AllLastMatches {
        @SerializedName("matchs")
        @Expose
        var matchs: Matchs? = null

        @SerializedName("goals")
        @Expose
        var goals: Goals? = null

        @SerializedName("goalsAvg")
        @Expose
        var goalsAvg: GoalsAvg? =
            null

    }

    inner class AllLastMatches_ {
        @SerializedName("matchs")
        @Expose
        var matchs: Matchs_? = null

        @SerializedName("goals")
        @Expose
        var goals: Goals_? = null

        @SerializedName("goalsAvg")
        @Expose
        var goalsAvg: GoalsAvg_? = null

    }

    inner class Att {
        @SerializedName("home")
        @Expose
        var home: String? = null

        @SerializedName("away")
        @Expose
        var away: String? = null

    }

    inner class Away {
        @SerializedName("team_id")
        @Expose
        var teamId: Int? = null

        @SerializedName("team_name")
        @Expose
        var teamName: String? = null

        @SerializedName("last_5_matches")
        @Expose
        var last5Matches: Last5Matches_? = null

        @SerializedName("all_last_matches")
        @Expose
        var allLastMatches: AllLastMatches_? = null

        @SerializedName("last_h2h")
        @Expose
        var lastH2h: LastH2h_? = null

    }

    inner class Comparison {
        @SerializedName("forme")
        @Expose
        private var forme: Forme? = null

        @SerializedName("att")
        @Expose
        var att: Att? = null

        @SerializedName("def")
        @Expose
        var def: Def? = null

        @SerializedName("fish_law")
        @Expose
        var fishLaw: FishLaw? = null

        @SerializedName("h2h")
        @Expose
        var h2h: H2h_? = null

        @SerializedName("goals_h2h")
        @Expose
        var goalsH2h: GoalsH2h? = null

    }

    inner class Def {
        @SerializedName("home")
        @Expose
        var home: String? = null

        @SerializedName("away")
        @Expose
        var away: String? = null

    }

    internal inner class Draws {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    internal inner class Draws_ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class Draws__ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class Draws___ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class FishLaw {
        @SerializedName("home")
        @Expose
        var home: String? = null

        @SerializedName("away")
        @Expose
        var away: String? = null

    }

    internal inner class Forme {
        @SerializedName("home")
        @Expose
        var home: String? = null

        @SerializedName("away")
        @Expose
        var away: String? = null

    }

    internal inner class Goals {
        @SerializedName("goalsFor")
        @Expose
        var goalsFor: GoalsFor? =
            null

        @SerializedName("goalsAgainst")
        @Expose
        var goalsAgainst: GoalsAgainst? =
            null

    }

    internal inner class GoalsAgainst {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    internal inner class GoalsAgainst_ {
        @SerializedName("home")
        @Expose
        var home: String? = null

        @SerializedName("away")
        @Expose
        var away: String? = null

        @SerializedName("total")
        @Expose
        var total: String? = null

    }

    inner class GoalsAgainst__ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class GoalsAgainst___ {
        @SerializedName("home")
        @Expose
        var home: String? = null

        @SerializedName("away")
        @Expose
        var away: String? = null

        @SerializedName("total")
        @Expose
        var total: String? = null

    }

    internal inner class GoalsAvg {
        @SerializedName("goalsFor")
        @Expose
        var goalsFor: GoalsFor_? = null

        @SerializedName("goalsAgainst")
        @Expose
        var goalsAgainst: GoalsAgainst_? = null

    }

    inner class GoalsAvg_ {
        @SerializedName("goalsFor")
        @Expose
        var goalsFor: GoalsFor___? = null

        @SerializedName("goalsAgainst")
        @Expose
        var goalsAgainst: GoalsAgainst___? = null

    }

    internal inner class GoalsFor {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    internal inner class GoalsFor_ {
        @SerializedName("home")
        @Expose
        var home: String? = null

        @SerializedName("away")
        @Expose
        var away: String? = null

        @SerializedName("total")
        @Expose
        var total: String? = null

    }

    inner class GoalsFor__ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class GoalsFor___ {
        @SerializedName("home")
        @Expose
        var home: String? = null

        @SerializedName("away")
        @Expose
        var away: String? = null

        @SerializedName("total")
        @Expose
        var total: String? = null

    }

    inner class GoalsH2h {
        @SerializedName("home")
        @Expose
        var home: String? = null

        @SerializedName("away")
        @Expose
        var away: String? = null

    }

    inner class Goals_ {
        @SerializedName("goalsFor")
        @Expose
        var goalsFor: GoalsFor__? = null

        @SerializedName("goalsAgainst")
        @Expose
        var goalsAgainst: GoalsAgainst__? = null

    }

    inner class H2h {
        @SerializedName("fixture_id")
        @Expose
        var fixtureId: Int? = null

        @SerializedName("league_id")
        @Expose
        var leagueId: Int? = null

        @SerializedName("league")
        @Expose
        var league: League? = null

        @SerializedName("event_date")
        @Expose
        var eventDate: String? = null

        @SerializedName("event_timestamp")
        @Expose
        var eventTimestamp: Int? = null

        @SerializedName("firstHalfStart")
        @Expose
        var firstHalfStart: Int? = null

        @SerializedName("secondHalfStart")
        @Expose
        var secondHalfStart: Int? = null

        @SerializedName("round")
        @Expose
        var round: String? = null

        @SerializedName("status")
        @Expose
        var status: String? = null

        @SerializedName("statusShort")
        @Expose
        var statusShort: String? = null

        @SerializedName("elapsed")
        @Expose
        var elapsed: Int? = null

        @SerializedName("venue")
        @Expose
        var venue: String? = null

        @SerializedName("referee")
        @Expose
        var referee: String? = null

        @SerializedName("homeTeam")
        @Expose
        var homeTeam: HomeTeam? = null

        @SerializedName("awayTeam")
        @Expose
        var awayTeam: AwayTeam? = null

        @SerializedName("goalsHomeTeam")
        @Expose
        var goalsHomeTeam: Int? = null

        @SerializedName("goalsAwayTeam")
        @Expose
        var goalsAwayTeam: Int? = null

        @SerializedName("score")
        @Expose
        var score: Score? = null

    }

    inner class H2h_ {
        @SerializedName("home")
        @Expose
        var home: String? = null

        @SerializedName("away")
        @Expose
        var away: String? = null

    }

    internal inner class Home {
        @SerializedName("team_id")
        @Expose
        var teamId: Int? = null

        @SerializedName("team_name")
        @Expose
        var teamName: String? = null

        @SerializedName("last_5_matches")
        @Expose
        var last5Matches: Last5Matches? = null

        @SerializedName("all_last_matches")
        @Expose
        var allLastMatches: AllLastMatches? = null

        @SerializedName("last_h2h")
        @Expose
        var lastH2h: LastH2h? = null

    }

    internal inner class Last5Matches {
        @SerializedName("forme")
        @Expose
        var forme: String? = null

        @SerializedName("att")
        @Expose
        var att: String? = null

        @SerializedName("def")
        @Expose
        var def: String? = null

        @SerializedName("goals")
        @Expose
        var goals: Int? = null

        @SerializedName("goals_avg")
        @Expose
        var goalsAvg: Double? = null

        @SerializedName("goals_against")
        @Expose
        var goalsAgainst: Int? = null

        @SerializedName("goals_against_avg")
        @Expose
        var goalsAgainstAvg: Double? = null

    }

    inner class Last5Matches_ {
        @SerializedName("forme")
        @Expose
        var forme: String? = null

        @SerializedName("att")
        @Expose
        var att: String? = null

        @SerializedName("def")
        @Expose
        var def: String? = null

        @SerializedName("goals")
        @Expose
        var goals: Int? = null

        @SerializedName("goals_avg")
        @Expose
        var goalsAvg: Double? = null

        @SerializedName("goals_against")
        @Expose
        var goalsAgainst: Int? = null

        @SerializedName("goals_against_avg")
        @Expose
        var goalsAgainstAvg: Double? = null

    }

    internal inner class LastH2h {
        @SerializedName("played")
        @Expose
        var played: Played? = null

        @SerializedName("wins")
        @Expose
        var wins: Wins_? = null

        @SerializedName("draws")
        @Expose
        var draws: Draws_? = null

        @SerializedName("loses")
        @Expose
        var loses: Loses_? = null

    }

    inner class LastH2h_ {
        @SerializedName("played")
        @Expose
        var played: Played_? = null

        @SerializedName("wins")
        @Expose
        var wins: Wins___? = null

        @SerializedName("draws")
        @Expose
        var draws: Draws___? = null

        @SerializedName("loses")
        @Expose
        var loses: Loses___? = null

    }

    internal inner class Loses {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    internal inner class Loses_ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class Loses__ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class Loses___ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    internal inner class Matchs {
        @SerializedName("matchsPlayed")
        @Expose
        var matchsPlayed: MatchsPlayed? =
            null

        @SerializedName("wins")
        @Expose
        var wins: Wins? = null

        @SerializedName("draws")
        @Expose
        var draws: Draws? = null

        @SerializedName("loses")
        @Expose
        var loses: Loses? = null

    }

    internal inner class MatchsPlayed {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class MatchsPlayed_ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class Matchs_ {
        @SerializedName("matchsPlayed")
        @Expose
        var matchsPlayed: MatchsPlayed_? = null

        @SerializedName("wins")
        @Expose
        var wins: Wins__? = null

        @SerializedName("draws")
        @Expose
        var draws: Draws__? = null

        @SerializedName("loses")
        @Expose
        var loses: Loses__? = null

    }

    internal inner class Played {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class Played_ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class Teams {
        @SerializedName("home")
        @Expose
        internal var home: Home? = null

        @SerializedName("away")
        @Expose
        var away: Away? = null

    }

    inner class WinningPercent {
        @SerializedName("home")
        @Expose
        var home: String? = null

        @SerializedName("draws")
        @Expose
        var draws: String? = null

        @SerializedName("away")
        @Expose
        var away: String? = null

    }

    internal inner class Wins {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    internal inner class Wins_ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class Wins__ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }

    inner class Wins___ {
        @SerializedName("home")
        @Expose
        var home: Int? = null

        @SerializedName("away")
        @Expose
        var away: Int? = null

        @SerializedName("total")
        @Expose
        var total: Int? = null

    }
}