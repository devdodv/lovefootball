package app.himaproduction.lifefootball.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TeamStatistic(
    val matchs: Matchs,
    val goals: Goals,
    val goalsAvg: GoalsAvg
) {

    data class MatchsPlayed(
        val home: Int,
        val away: Int,
        val total: Int
    )

    data class Matchs(
        val matchsPlayed: MatchsPlayed,
        val wins: Wins,
        val draws: Draws,
        val loses: Loses
    )

    data class Goals(
        val goalsFor: GoalsFor,
        val goalsAgainst: GoalsAgainst
    )

    data class GoalsAvg(
        val goalsFor: GoalsForAvg,
        val goalsAgainst: GoalsAgainstAvg
    )

    data class Wins(
        val home: Int,
        val away: Int,
        val total: Int
    )

    data class Draws(
        val home: Int,
        val away: Int,
        val total: Int
    )

    data class Loses(

        val home: Int,
        val away: Int,
        val total: Int
    )

    data class GoalsFor(

        val home: Int,
        val away: Int,
        val total: Int
    )

    data class GoalsAgainst(
        val home: Int,
        val away: Int,
        val total: Int
    )

    data class GoalsForAvg(

        val home: Double,
        val away: Double,
        val total: Double
    )

    data class GoalsAgainstAvg(
        val home: Double,
        val away: Double,
        val total: Double
    )
}