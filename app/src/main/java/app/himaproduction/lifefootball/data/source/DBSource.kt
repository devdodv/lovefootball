package app.himaproduction.lifefootball.data.source

import app.himaproduction.lifefootball.data.entity.Country
import app.himaproduction.lifefootball.data.entity.League
import app.himaproduction.lifefootball.data.entity.Setting
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface DBSource {
    fun saveFollowingLeague(league: League):Completable
    fun getFollowingLeagueFromDB():Flowable<List<League>>
    fun deleteFollowingLeague(league: League): Completable
    fun getCountriesFromDB(): Flowable<List<Country>>
    fun saveCountriesToDB(it: List<Country>)
    fun isFollowed(leagueId:Int): Flowable<Boolean>
}