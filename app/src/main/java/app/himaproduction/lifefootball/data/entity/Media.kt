package app.himaproduction.lifefootball.data.entity

import androidx.annotation.Keep
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.google.firebase.firestore.IgnoreExtraProperties

@Keep
@IgnoreExtraProperties
data class Media(
    var id:String = "",
    val lastComment:String?=null,
    val totalComment:Int = 0,
    val mediaUrl:String?=null,
    val mediaDescription:String?=null,
    val active:Boolean = false,
    val createTime:String = "2020/08/05 12:20",
    val username:String?=null,
    val videoThumbnail:String?=null
)