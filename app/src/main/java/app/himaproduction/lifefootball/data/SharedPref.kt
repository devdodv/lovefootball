package app.himaproduction.lifefootball.data

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor


class SharedPref(var _context: Context) {
    // Shared Preferences
    var pref: SharedPreferences
    var editor: Editor
    // Shared pref mode
    var PRIVATE_MODE = 0

    fun setUsername(username:String) {
        editor.putString(KEY_USER_NAME, username)
        // commit changes
        editor.commit()
    }

    val getUsername: String?
        get() = pref.getString(KEY_USER_NAME, "")

    companion object {
        // LogCat tag
        private val TAG = SharedPref::class.java.simpleName
        // Shared preferences file name
        private const val PREF_NAME = "fblover"
        private val KEY_USER_NAME = "username"
    }

    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }
}
