package app.himaproduction.lifefootball.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import app.himaproduction.lifefootball.Const

data class Setting(
    val current_season: String = "",
    val is_advertising: Boolean = true,
    val version_code: Int = 1,
    val update_content: String = "",
    val notify_message: String = "",
    val api_url: String = "",
    val api_key: String = ""
)