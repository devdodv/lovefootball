package app.himaproduction.lifefootball.data.api.response

import app.himaproduction.lifefootball.data.entity.TeamStatistic
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TeamStatisticResponse : BaseApiResponse<TeamStatisticResponse?>() {
    @SerializedName("results")
    @Expose
    var results: Int? = null
    @SerializedName("statistics")
    @Expose
    var statistics: TeamStatistic? = null

}