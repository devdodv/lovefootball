package app.himaproduction.lifefootball.di

import androidx.room.Room
import app.himaproduction.lifefootball.data.repository.RepositoryImpl
import app.himaproduction.lifefootball.data.repository.Repository
import app.himaproduction.lifefootball.data.db.DaoImpl
import app.himaproduction.lifefootball.data.db.DBHelper
import app.himaproduction.lifefootball.data.api.ApiBuilder
import app.himaproduction.lifefootball.data.api.ApiImpl
import app.himaproduction.lifefootball.data.memory.CacheMemoryImpl
import app.himaproduction.lifefootball.data.source.ApiSource
import app.himaproduction.lifefootball.data.source.DBSource
import app.himaproduction.lifefootball.data.source.MemorySource
import app.himaproduction.lifefootball.viewmodel.LeagueFollowViewModel
import app.himaproduction.lifefootball.viewmodel.MainViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module


val applicationModule = module(override = true) {
    single {
        Room.databaseBuilder(
                androidContext(),
                DBHelper::class.java, "lovefootball.db"
            )
            .build()
    }
    single { get<DBHelper>().getDAO() }
    single<ApiSource> { ApiImpl(get()) }
    single<DBSource> { DaoImpl(get()) }
    single<MemorySource> { CacheMemoryImpl() }
    single { ApiBuilder.getWebService() }
    single<Repository> {
        RepositoryImpl(get(),get(),get())
    }
    single { LeagueFollowViewModel(get()) }
    factory { MainViewModel(get()) }
}