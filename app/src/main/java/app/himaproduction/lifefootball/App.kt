package app.himaproduction.lifefootball

import android.app.Application
import android.os.Handler
import app.himaproduction.lifefootball.di.applicationModule
import com.google.android.gms.ads.*
import com.google.android.gms.ads.formats.UnifiedNativeAd
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class App : Application() {
    init {
        self = this
    }

    companion object {
        private var self: App? = null
        val mNativeAds: ArrayList<UnifiedNativeAd> = ArrayList()
        const val NUMBER_OF_ADS = 5
        var isReloadedNativeAd = false
        var bannerSmall: AdView? = null
        private var isBannerSmallReloaded = false
        // The AdLoader used to load ads.
        private var adLoader: AdLoader? = null
        val NATIVE_ADS_ID = "ca-app-pub-3940256099942544/8135179316"
        val banner_test = "ca-app-pub-3940256099942544/6300978111"
        fun self(): App {
            return self as App
        }
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(applicationModule)
        }
//        loadNativeAds()
//        loadBannerSmall()
    }

    fun loadNativeAds() {
        val builder = AdLoader.Builder(this, NATIVE_ADS_ID)
        adLoader = builder.forUnifiedNativeAd { unifiedNativeAd ->
            // A native ad loaded successfully, check if the ad loader has finished loading
            // and if so, insert the ads into the list.
            mNativeAds.add(unifiedNativeAd)
            if (!adLoader!!.isLoading) {

            }
        }.withAdListener(
            object : AdListener() {
                override fun onAdFailedToLoad(errorCode: Int) {
                    if (!adLoader!!.isLoading) {
//                        insertAdsInMenuItems()
                        if(!isReloadedNativeAd){
                            loadNativeAds()
                            isReloadedNativeAd = true
                        }
                    }
                }
            }).build()

        // Load the Native Express ad.
        // Load the Native Express ad.
        adLoader?.loadAds(AdRequest.Builder().build(), NUMBER_OF_ADS)
    }

    fun loadBannerSmall() {
        val adRequest =
            AdRequest.Builder()
        adRequest.addTestDevice("4CBF693D1317321A348148C41F3EB4C9")
        adRequest.addTestDevice("4CBF693D1317321A348148C41F3EB4C9")
        adRequest.addTestDevice("DE7AAC6B9D8AB862DFF1A18B13FDD54F")
        adRequest.addTestDevice("C183738541E12EE209152979E844A437")
        bannerSmall = AdView(this)
        bannerSmall!!.setAdUnitId(banner_test)
        bannerSmall!!.setAdSize(AdSize.SMART_BANNER)
        bannerSmall!!.setAdListener(object : AdListener() {
            override fun onAdFailedToLoad(i: Int) {
                super.onAdFailedToLoad(i)
                if (!isBannerSmallReloaded) {
                    isBannerSmallReloaded = true
                    loadBannerSmall()
                }
            }

            override fun onAdLoaded() {
                super.onAdLoaded()
            }
        })
        Handler().post(Runnable { bannerSmall!!.loadAd(adRequest.build()) })
    }

}
